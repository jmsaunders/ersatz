cbuffer VS_CONSTANT_BUFFER : register(b0) {
    matrix worldViewProjection;
    float4 color;
    float4 misc; // [view width, view height, 0, 0]
}

cbuffer VS_CONSTANT_BUFFER : register(b1) {
    float lineWidth;
}

struct VertexInputType {
    float4 position : POSITION;
    float2 uv : TEXCOORD;
};

struct GeometryInputType {
    float4 position : POSITION;
    float4 color : COLOR;
};

struct PixelInputType {
    float4 position : SV_POSITION;
    float4 color : COLOR;
};

float4 clipLineToNearPlane(float4 p, float4 q) {
    if (p.z < -p.w) {
        // I don't think this code is perfect for d3d11 clip planes (clip space is different), so
        // it will probably need to be fixed up.
        // solves p + (q - p) * A; for A when p.z / p.w = -1.0.
        const float denom = q.z - p.z + q.w - p.w;
        if (denom == 0.0) return p;

        const float A = (-p.z - p.w) / denom;
        p = p + (q - p) * A;
    }

    return p;
}

GeometryInputType vertex(VertexInputType input) {
    GeometryInputType ret;
    ret.color = color;
    ret.position = mul(float4(input.position.xyz, 1.f), worldViewProjection);
    return ret;
}

[maxvertexcount(6)]
void geometry(line GeometryInputType input[2], inout TriangleStream<PixelInputType> stream) {
    float4 fst = clipLineToNearPlane(input[0].position, input[1].position);
    float4 snd = clipLineToNearPlane(input[1].position, input[0].position);

    const float2 fstScreen = fst.xy / fst.w;
    const float2 sndScreen = snd.xy / snd.w;

    const float2 dirScreen = normalize(sndScreen - fstScreen);

    const float2 normal = float2(dirScreen.y, -dirScreen.x) / misc.xy;

    const float4 fstOffset = float4(normal * lineWidth, 0, 0) * fst.w;
    const float4 sndOffset = float4(normal * lineWidth, 0, 0) * snd.w;

    PixelInputType output;
    output.color = color;

    output.position = fst - fstOffset;
    stream.Append(output);
    output.position = fst + fstOffset;
    stream.Append(output);
    output.position = snd + sndOffset;
    stream.Append(output);

    output.position = snd - sndOffset;
    stream.Append(output);
    output.position = fst - fstOffset;
    stream.Append(output);
    output.position = fst + fstOffset;
    stream.Append(output);
}

float4 pixel(PixelInputType input) : SV_Target {
    return input.color;
}
