cbuffer VS_CONSTANT_BUFFER : register(b0) {
    matrix worldViewProjection;
    float4 colour;
    float4 misc;
}

struct VertexInputType {
    float4 position : POSITION;

#ifdef HAS_TEXCOORD0
    float2 texcoord : TEXCOORD0;
#endif
};

struct GeometryInputType {
    float4 position : SV_POSITION;
    float4 colour : COLOR;
    bool originalVertex : POSITION0;

#ifdef HAS_TEXCOORD0
    float2 texcoord : TEXCOORD0;
#endif
};

struct PixelInputType {
    float4 position : SV_POSITION;
    float4 colour : COLOR;
    //float3 bary : POSITION0;

#ifdef HAS_TEXCOORD0
    float2 texcoord : TEXCOORD0;
#endif
};

#ifdef HAS_TEXCOORD0
sampler sampler0;
Texture2D texture0;
#endif

PixelInputType vertex(VertexInputType input) {
    PixelInputType output;
    output.position = mul(float4(input.position.xyz, 1.f), worldViewProjection);
    output.colour = colour;

#ifdef HAS_TEXCOORD0
    output.texcoord = input.texcoord;
#endif

    return output;
}

/*
[maxvertexcount(3)]
void geometry(triangle GeometryInputType input[3], inout TriangleStream<PixelInputType> TriStream) {
    for (int i = 0; i < 3; ++i) {
        GeometryInputType vin = input[i];
        PixelInputType vout;
        vout.position = vin.position;

        float other = vin.originalVertex ? 0 : 100;

        if (i == 0) {
            vout.bary = float3(1, other, other);
        } else if (i == 1) {
            vout.bary = float3(other, 1, other);
        } else if (i == 2) {
            vout.bary = float3(other, other, 1);
        }

        vout.colour = vin.colour;
        vout.texcoord = vin.texcoord;
        TriStream.Append(vout);
    }
}

Old sampling code:

    float3 deltas = fwidth(input.bary);
    float3 bary = smoothstep(0.5*deltas, 1.5*deltas, input.bary);
    //float weight = min(bary.x, min(bary.y, bary.z));
    float weight = 1.f;

    float3 c = ((1-weight) * float3(1,1,1) + weight * s.xyz) * input.colour.xyz;
    return float4(c, 1);
*/

float4 pixel(PixelInputType input) : SV_Target {
#ifdef HAS_TEXCOORD0
    float4 s = texture0.Sample(sampler0, input.texcoord);
    return input.colour * float4(s.xyz, 1);
#else
    return colour;
#endif
}
