
void accumulateUniquePoints(const Mesh* mesh, Vec<float4>& out) {
    for (const auto& face : mesh->faces) {
        for (const auto& v : face.verts) {
            bool unique = true;
            for (const auto& curr : out) {
                if (length2(curr - v) < epsilon) {
                    unique = false;
                    break;
                }
            }

            if (unique) out.push_back(v);
        }
    }
}
