

#if 0
    struct Edge;
    struct Face;
    struct Vertex {
        Vertex(const float3& pos) : pos(pos) {}
        float3 pos;
    };

    // This is a half-edge data format:
    struct Edge {
        Vertex* v1 = nullptr;
        Vertex* v2 = nullptr;

        Face* face = nullptr;

        Edge* opposite = nullptr;
    };

    struct Face {
        Vec<Edge*> edges;
        Vec<float3> verts;
    };

    struct Mesh {
        Vec<Face*> faces;
    };

    Vertex* mkv(const float3& in) {
        return new Vertex(in);
    }
#endif

        // This maps from the two planes (creating the edge) into some data we need to reconstruct the edge
        // so we can create the half-edge format without doing a lot of extra work.
        //Map<Pair<size_t, size_t>, Vec<EdgeInfo>, pair_hash> edgeData;



                        if (brush.pointLiesOnBoundary(*v, epsilon*2)) {
                            // We have a real vertex on this line:
                            /*
                            auto aIdx = faceIt - begin(planes), bIdx = plane2It - begin(planes);
                            auto mindex = min(aIdx, bIdx), maxdex = max(aIdx, bIdx);
                            edgeData[std::make_pair(mindex, maxdex)].push_back({ size_t(plane3It - begin(planes)), *v });
                            */

                            verts.push_back(*v);
                        }



        // We don't care about try to use half edge stuff anymore:
#if 0
        Vec<Face*> faces;
        faces.reserve(planes.size());
        for (size_t i = 0; i < planes.size(); ++i) faces.push_back(new Face);

        // Create the edge list and populate the (unordered) vertex data on it for each face:
        for (const auto& [edgeDesc, edgeInfo] : edgeData) {
            auto [faceP, oppositeP] = edgeDesc;

            Vec<float3> uniquePoints;
            for (const auto& p : edgeInfo) {
                bool found = false;
                for (const auto& test : uniquePoints) {
                    if (length2(test - p.p) < epsilon) {
                        found = true;
                        break;
                    }
                }

                if (!found) uniquePoints.push_back(p.p);
            }

            // No edges are generated if we have only a single point. This can be created from this situation:
            //    \|/
            //  ---.---

            if (uniquePoints.size() == 1) continue;

            // We have a line and two unique positions on it. This creates a line on the two faces faceP and oppositeP.
            assert(uniquePoints.size() == 2);

            auto& face = faces[faceP];
            auto& oppositeFace = faces[oppositeP];

            auto e = new Edge;
            face->edges.push_back(e);
            e->face = face;

            auto e2 = new Edge;
            oppositeFace->edges.push_back(e2);
            e2->face = oppositeFace;

            auto p1 = mkv(uniquePoints[0]), p2 = mkv(uniquePoints[1]);

            e->opposite = e2;
            e2->opposite = e;

            e->v1 = e2->v1 = p1;
            e->v2 = e2->v2 = p2;
        }

        // Order the edges correctly:
        for (auto [i, face] : enumerate(faces)) {
            const auto& verts = ours[i];
            auto center = std::accumulate(begin(verts), end(verts), float3()) / verts.size();
            auto startv = normalize(center - verts[0]);

            for (auto& edge : face->edges) {
                auto v1v = normalize(center - edge->v1->pos), v2v = normalize(center - edge->v2->pos);
                if (dot(cross(v1v, v2v), planes[i].n) > 0)
                    std::swap(edge->v1, edge->v2);
            }

            std::sort(begin(face->edges), end(face->edges), [&](const Edge* edge1, const Edge* edge2) {
                auto lv = normalize(center - edge1->v1->pos), rv = normalize(center - edge2->v1->pos);
                return fullAngle(startv, lv, planes[i].n) < fullAngle(startv, rv, planes[i].n);
            });

            for (const auto& e : face->edges) {
                face->verts.push_back(e->v1->pos);
            }

            // Sanity check for debug:
#if DEBUG_CONNECTIVITY
            for (size_t i = 0; i < face->edges.size() - 1; ++i) {
                assert(length2(face->edges[i]->v2->pos - face->edges[i + 1]->v1->pos) < epsilon);
                if (i == 0)
                    assert(length2(face->edges[i]->v1->pos - face->edges.back()->v2->pos) < epsilon);
                else
                    assert(length2(face->edges[i]->v1->pos - face->edges[i-1]->v2->pos) < epsilon);
            }
#endif
        }

        return { faces };
#endif
