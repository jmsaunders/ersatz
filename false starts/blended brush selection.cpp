
    // This is the old version of selection that worked by blending. It might be better for performance
    // reasons, since we don't need to fully regenerate the vertex buffer every time this way.

    ComPtr<ID3D11BlendState> selectedBrushBlendState;
    ComPtr<ID3D11DepthStencilState> selectedBrushDepthState;
    ComPtr<ID3D11InputLayout> selectedInputLayout;
    Ref<Shader> selectedShader;

    void drawSelectedBrushes(const ComPtr<ID3D11DeviceContext>& dc, const float4x4& mvp) {
        Vec<float3> verts;

        for (auto brush : map->selectedBrushes) {
            for (const auto& face : map->meshes[brush].faces) {
                for (size_t i = 1; i < face.verts.size() - 1; ++i) {
                    verts.push_back(face.verts[0]);
                    verts.push_back(face.verts[i]);
                    verts.push_back(face.verts[i+1]);
                }
            }
        }

        if (verts.empty()) return;

        static bool initialized = false;
        if (!initialized) {
            initialized = true;

            // Only draw when we have the same depth value:
            D3D11_DEPTH_STENCIL_DESC dsDesc = {};
            dsDesc.DepthEnable = true;
            dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
            dsDesc.DepthFunc = D3D11_COMPARISON_EQUAL;

            dev->CreateDepthStencilState(&dsDesc, &selectedBrushDepthState);

            D3D11_BLEND_DESC blendState = {};
            blendState.RenderTarget[0].BlendEnable = true;
            blendState.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
            blendState.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
            blendState.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
            blendState.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

            blendState.RenderTarget[0].SrcBlendAlpha = blendState.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
            blendState.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_MAX;
            dev->CreateBlendState(&blendState, &selectedBrushBlendState);

            D3D11_INPUT_ELEMENT_DESC descs[] = {
                { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
            };

            selectedShader = resourceManager->requestShader("solidcolour.hlsl", {});
            if (FAILED(dev->CreateInputLayout(descs, _countof(descs), selectedShader->vsblob->GetBufferPointer(), selectedShader->vsblob->GetBufferSize(), &selectedInputLayout)))
                unreachable("failed to create input layout");
        }

        ComPtr<ID3D11Buffer> selectedBrushesVB;
        {
            D3D11_BUFFER_DESC desc = {};
            desc.ByteWidth = UINT(verts.size() * sizeof(verts[0]));
            desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

            D3D11_SUBRESOURCE_DATA initial = { (void*)&verts[0] };
            if (FAILED(dev->CreateBuffer(&desc, &initial, &selectedBrushesVB))) unreachable("could not create vertex buffer");
        }

        ComPtr<ID3D11DepthStencilState> prevDSState;
        dc->OMGetDepthStencilState(&prevDSState, nullptr);
        dc->OMSetDepthStencilState(selectedBrushDepthState.Get(), 0);

        ComPtr<ID3D11BlendState> oldBlendState;
        float oldBlendFactor[4];
        UINT oldMask;
        dc->OMGetBlendState(oldBlendState.GetAddressOf(), oldBlendFactor, &oldMask);
        dc->OMSetBlendState(selectedBrushBlendState.Get(), nullptr, 0xFFFFFFFF);

        {
            struct ConstantBuffer {
                float4x4 mvp;
                float4 colour;
            } constantBuffer = { mvp, float4(0.6f, 0.f, 0.f, 1.f) };

            D3D11_SUBRESOURCE_DATA initData = { &constantBuffer };

            D3D11_BUFFER_DESC cbDesc = {};
            cbDesc.ByteWidth = sizeof(ConstantBuffer);
            cbDesc.Usage = D3D11_USAGE_DYNAMIC;
            cbDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
            cbDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

            ComPtr<ID3D11Buffer> cb;
            if (FAILED(dev->CreateBuffer(&cbDesc, &initData, &cb))) unreachable("failed to create the shader constant buffer");
            dc->VSSetConstantBuffers(0, 1, cb.GetAddressOf());
        }

        selectedShader->bind(dc);
        UINT offset = 0, stride = sizeof(verts[0]);
        dc->IASetVertexBuffers(0, 1, selectedBrushesVB.GetAddressOf(), &stride, &offset);
        dc->IASetInputLayout(selectedInputLayout.Get());
        dc->Draw(UINT(verts.size()), 0);

        // Reset to previous values:
        dc->OMSetDepthStencilState(prevDSState.Get(), 0);
        dc->OMSetBlendState(oldBlendState.Get(), oldBlendFactor, oldMask);
    }
