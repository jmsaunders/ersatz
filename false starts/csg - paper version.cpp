#include "common.h"
#include "csg.h"

#include "tokenizer.h"
#include <numeric>

constexpr float epsilon = 0.001f;

struct pair_hash {
    template <class T1, class T2>
    std::size_t operator () (const std::pair<T1,T2> &p) const {
        auto h1 = std::hash<T1>{}(p.first);
        auto h2 = std::hash<T2>{}(p.second);
        return h1 ^ (h2 << 12);  
    }
};

namespace csg {
    bool Brush::containsPoint(const float3& pt) const {
        // For additive brushes, the normals face out so we have to be on the back side of all of them:
        auto badSide = type == SUBTRACTIVE ? Plane::BACK : Plane::FRONT;
        for (const auto& p : planes) {
            if (p.side(pt) == badSide)
                return false;
        }

        return true;
    }

    bool Brush::pointLiesOnBoundary(const float3& pt, float epsilon) const {
        bool hasBoundary = false;

        // For additive brushes, the normals face out so we have to be on the back side of all of them:
        auto badSide = type == SUBTRACTIVE ? Plane::BACK : Plane::FRONT;
        for (const auto& p : planes) {
            auto side = p.side(pt, epsilon);
            if (p.side(pt) == badSide)
                return false;
            hasBoundary |= side == Plane::ON;
        }

        return hasBoundary;
    }

    Vertex* mkv(const float3& in) {
        return new Vertex(in);
    }

    // Given a brush as input, produce a mesh:
    Mesh generateMeshFromBrush(const Brush& brush, QuakeMap* out = nullptr) {
        Vec<Vec<float3>> ours;
        Vec<float3> totalVerts;
        const auto& planes = brush.planes;

        struct EdgeInfo {
            size_t theFace;
            float3 p;
        };

        // This maps from the two planes (creating the edge) into some data we need to reconstruct the edge
        // so we can create the half-edge format without doing a lot of extra work.
        Map<Pair<size_t, size_t>, Vec<EdgeInfo>, pair_hash> edgeData;

        for (auto faceIt = begin(planes); faceIt != end(planes); ++faceIt) {
            const auto& facePlane = *faceIt;
            size_t faceIdx = faceIt - begin(planes);
            Vec<float3> verts;

            for (auto plane2It = begin(planes); plane2It != end(planes); ++plane2It) {
                // We find our line (assuming there is any intersection here):
                auto maybeLine = facePlane.intersect(*plane2It);
                if (!maybeLine) continue;
                auto line = *maybeLine;

                //for (auto plane3It = plane2It + 1; plane3It != end(planes); ++plane3It) {
                for (auto plane3It = begin(planes); plane3It != end(planes); ++plane3It) {
                    if (auto v = plane3It->intersect(line)) {
                        // debug code:
                        assert(plane3It->side(*v, epsilon) == Plane::ON);
                        assert(plane2It->side(*v, epsilon) == Plane::ON);
                        assert(faceIt->side(*v, epsilon) == Plane::ON);

                        if (brush.pointLiesOnBoundary(*v, epsilon)) {
                            // We have a real vertex on this line:
                            auto aIdx = faceIt - begin(planes), bIdx = plane2It - begin(planes);
                            auto mindex = min(aIdx, bIdx), maxdex = max(aIdx, bIdx);
                            edgeData[std::make_pair(mindex, maxdex)].push_back({ size_t(plane3It - begin(planes)), *v });

                            verts.push_back(*v);
                        }
                    }
                }
            }

            if (verts.size() < 3) {
                OutputDebugStringA("got a degenerate face!");
                continue;
            }

            auto center = std::accumulate(begin(verts), end(verts), float3()) / verts.size();
            auto startv = normalize(center - verts[0]);

            // We anchor the first vertex and then measure the angle between the vertex from the centre to this vertex from
            // the similar vector to all the other vertices.
            std::sort(begin(verts)+1, end(verts), [&](const float3& lhs, const float3& rhs) {
                auto lv = normalize(center - lhs), rv = normalize(center - rhs);
                return fullAngle(startv, lv, facePlane.n) < fullAngle(startv, rv, facePlane.n);
            });

            // Remove redundant vertices:
            for (size_t i = 0; i < verts.size() - 1;) {
                if (length2(verts[i] - verts[i + 1]) < epsilon) {
                    verts.erase(verts.begin() + i + 1);
                } else
                    ++i;
            }
            if (length2(verts.back() - verts.front()) < epsilon)
                verts.pop_back();

            if (verts.size() < 3) {
                OutputDebugStringA("removing duplicates resulted in a degenerate face!");
                continue;
            }

            totalVerts.insert(end(totalVerts), begin(verts), end(verts));

            ours.push_back(verts);
        }

        Vec<Face*> faces;
        faces.reserve(planes.size());
        for (size_t i = 0; i < planes.size(); ++i) faces.push_back(new Face);

        // Create the edge list and populate the (unordered) vertex data on it for each face:
        for (const auto& [edgeDesc, edgeInfo] : edgeData) {
            auto [faceP, oppositeP] = edgeDesc;

            Vec<float3> uniquePoints;
            for (const auto& p : edgeInfo) {
                bool found = false;
                for (const auto& test : uniquePoints) {
                    if (length2(test - p.p) < epsilon) {
                        found = true;
                        break;
                    }
                }

                if (!found) uniquePoints.push_back(p.p);
            }

            // No edges are generated if we have only a single point. This can be created from this situation:
            //    \|/
            //  ---.---

            if (uniquePoints.size() == 1) continue;

            // We have a line and two unique positions on it. This creates a line on the two faces faceP and oppositeP.
            assert(uniquePoints.size() == 2);

            auto& face = faces[faceP];
            auto& oppositeFace = faces[oppositeP];

            auto e = new Edge;
            face->edges.push_back(e);
            e->face = face;

            auto e2 = new Edge;
            oppositeFace->edges.push_back(e2);
            e2->face = oppositeFace;

            auto p1 = mkv(uniquePoints[0]), p2 = mkv(uniquePoints[1]);

            e->opposite = e2;
            e2->opposite = e;

            e->v1 = e2->v1 = p1;
            e->v2 = e2->v2 = p2;
        }

        // Order the edges correctly:
        for (auto [i, face] : enumerate(faces)) {
            const auto& verts = ours[i];
            auto center = std::accumulate(begin(verts), end(verts), float3()) / verts.size();
            auto startv = normalize(center - verts[0]);

            for (auto& edge : face->edges) {
                auto v1v = normalize(center - edge->v1->pos), v2v = normalize(center - edge->v2->pos);
                if (dot(cross(v1v, v2v), planes[i].n) > 0)
                    std::swap(edge->v1, edge->v2);
            }

            std::sort(begin(face->edges), end(face->edges), [&](const Edge* edge1, const Edge* edge2) {
                auto lv = normalize(center - edge1->v1->pos), rv = normalize(center - edge2->v1->pos);
                return fullAngle(startv, lv, planes[i].n) < fullAngle(startv, rv, planes[i].n);
            });

            for (const auto& e : face->edges) {
                face->verts.push_back(e->v1->pos);
            }

            // Sanity check for debug:
#if DEBUG_CONNECTIVITY
            for (size_t i = 0; i < face->edges.size() - 1; ++i) {
                assert(length2(face->edges[i]->v2->pos - face->edges[i + 1]->v1->pos) < epsilon);
                if (i == 0)
                    assert(length2(face->edges[i]->v1->pos - face->edges.back()->v2->pos) < epsilon);
                else
                    assert(length2(face->edges[i]->v1->pos - face->edges[i-1]->v2->pos) < epsilon);
            }
#endif
        }

        // I think this is unsound but I'm not sure. TODO!!
        float maxDist = 0.f;
        float3 best1, best2;
        for (const auto& v1 : totalVerts) {
            for (const auto& v2 : totalVerts) {
                auto len = length2(v1 - v2);
                if (len > maxDist) {
                    maxDist = len;
                    best1 = v1;
                    best2 = v2;
                }
            }
        }
        float3 center = (best1 + best2) / 2;

        if (out) {
            for (const auto& o : ours)
                out->f.push_back({ o });
        }

        return { faces, center, sq(sqrt(maxDist)/2) };
    }

    __forceinline float fastlen(const float3& v) {
        return fastdot(v, v);
    }

    __forceinline float dist2(const float3& v1, const float3& v2) {
        float3 diff(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
        return fastlen(diff);
    }

    void accumulateUniquePoints(const Mesh* mesh, Vec<float3>& out) {
        for (const auto& face : mesh->faces) {
            for (const auto& v : face->verts) {
                bool unique = true;
                for (const auto& curr : out) {
                    if (length2(curr - v) < epsilon) {
                        unique = false;
                        break;
                    }
                }

                if (unique) out.push_back(v);
            }
        }
    }

    // Are all vertices of this mesh on the same side of the given plane? Vertices on the plane mean intersection mean
    // they are not all on the same side.
    bool allOnFrontSide(const Mesh& mesh, const Plane& p) {
        int side = -1;
        for (const auto& face : mesh.faces) {
            for (const auto& v : face->verts) {
                if (p.side(v) != Plane::FRONT) return false;
            }
        }

        return true;
    }

    bool brushesTouch(VecA<Mesh> meshes, VecA<Brush> brushes, size_t a, size_t b) {
        // Brushes are separated if one of the planes for the brush has every vertex of the mesh on the same side of it:
        for (const auto& p : brushes[a].planes) {
            if (allOnFrontSide(meshes[b], p))
                return false;
        }

        for (const auto& p : brushes[b].planes) {
            if (allOnFrontSide(meshes[a], p))
                return false;
        }

        // We were unable to find a plane that does not have points of the other side spanning it, we intersect:
        return true;
    }

}

using namespace csg;
struct CSG {
    VecA<Brush> brushes;
    VecA<Mesh> meshes;

    Map<size_t, Vec<size_t>> connectivity;

    bool testFirstOrder(size_t originBrush, const float3& v, bool over) {
        auto c = connectivity[originBrush];
        for (auto it = rbegin(c); it != rend(c); ++it) {
            const auto brushIdx = *it;
            const auto& brush = brushes[brushIdx];

            bool in = false;
            if (brushIdx == originBrush) {
                in = over;
            } else {
                in = brush.containsPoint(v);
            }

            if (in) {
                return brush.type == Brush::ADDITIVE;
            }
        }

        return false;
    }

    bool testSecondOrder(size_t brushA, size_t brushB, const float3& v, bool overrideA, bool overrideB, VecA<size_t> c) {
        for (auto it = rbegin(c); it != rend(c); ++it) {
            const auto brushIdx = *it;
            const auto& brush = brushes[brushIdx];

            bool in = false;
            if (brushIdx == brushA) {
                in = overrideA;
            } else if (brushIdx == brushB) {
                in = overrideB;
            } else {
                in = brush.containsPoint(v);
            }

            if (in) {
                return brush.type == Brush::ADDITIVE;
            }
        }

        return false;
    }

    CSG(VecA<Brush> brushes, VecA<Mesh> meshes) : brushes(brushes), meshes(meshes) {
        // Find brush connectivity information:
        for (size_t i = 0; i < brushes.size(); ++i) {
            for (size_t n = i + 1; n < brushes.size(); ++n) {
                if (brushesTouch(meshes, brushes, i, n)) {
                    connectivity[i].push_back(n);
                    connectivity[n].push_back(i);
                }
            }

            // For convenience later on but a bit ugly:
            connectivity[i].push_back(i);
        }

        for (auto& [key, val] : connectivity)
            std::sort(begin(val), end(val));

        struct Looplet {
            float3 v;
            Face* comingFrom;
            Face* ourFace;
            Face* goingTo;
        };

        Map<Face*, Vec<Looplet>> faceLoops;

        for (const auto& [brushId, mesh] : enumerate(meshes)) {
            // STEP 1: Find a list of unique vertices in our mesh and figure out which of thes are
            // contributing to the final mesh:
            for (const auto& face : mesh.faces) {
                for (const auto& [edgeNo, edge] : enumerate(face->edges)) {
                    const auto& v = edge->v2->pos;

                    bool b0 = testFirstOrder(brushId, v, false);
                    bool b1 = testFirstOrder(brushId, v, true);

                    if (b0 != b1) {
                        auto nextEdge = edgeNo == face->edges.size() - 1 ? face->edges[0] : face->edges[edgeNo + 1];
                        faceLoops[face].push_back({ v, edge->opposite->face, face, nextEdge->opposite->face });
                    }
                }
            }

            for (const auto& face : mesh.faces) {
                // STEP 2: Find the second order vertices (the vertices creating by intersecting our edges
                // with other brushes), and then figure out which of those are in the final mesh:

                for (const auto& edge : face->edges) {
                    Line line = { edge->v1->pos, edge->v2->pos - edge->v1->pos };
                    auto face3 = edge->opposite->face;

                    for (const auto& connectedId : connectivity[brushId]) {
                        if (connectedId == brushId) continue;
                        const auto& otherBrush = brushes[connectedId];

                        for (size_t planeNum = 0; planeNum < otherBrush.planes.size(); ++planeNum) {
                            const auto& p = otherBrush.planes[planeNum];
                            const auto& face2 = meshes[connectedId].faces[planeNum];

                            if (auto dist = p.intersectTime(line)) {
                                if (*dist > 0 && *dist < 1) {
                                    Vec<size_t> c;
                                    const auto& ca = connectivity[brushId];
                                    const auto& cb = connectivity[connectedId];
                                    mergeIntersection(begin(ca), end(ca), begin(cb), end(cb), std::back_inserter(c));

                                    auto v = line.atTime(*dist);

                                    bool b00 = testSecondOrder(brushId, connectedId, v, false, false, c);
                                    bool b10 = testSecondOrder(brushId, connectedId, v, true, false, c);
                                    bool b01 = testSecondOrder(brushId, connectedId, v, false, true, c);
                                    bool b11 = testSecondOrder(brushId, connectedId, v, true, true, c);

                                    bool result = (b00 != b10 || b01 != b11) && (b00 != b01 || b10 != b11);
                                    if (result) {
                                        if (!b00 && b10) {
                                            faceLoops[face].push_back({ v, face3, face, face2 });
                                            faceLoops[face3].push_back({ v, face2, face3, face });
                                        }
                                        if (!b01 && b11) {
                                            faceLoops[face].push_back({ v, face2, face, face3 });
                                            faceLoops[face3].push_back({ v, face, face3, face2 });
                                        }

                                        if (b01 && !b00)
                                            faceLoops[face2].push_back({ v, face, face2, face3 });
                                        if (b11 && !b10)
                                            faceLoops[face2].push_back({ v, face3, face2, face });

                                        // we shouldn't ever get these cases:
                                        assert(!(b00 && !b10));
                                        assert(!(b01 && !b11));
                                        assert(!(!b01 && b00));
                                        assert(!(!b10 && b11));
                                    }
                                }
                            }
                        }
                    }
                }
            }

            // STEP 3: Find the third order vertices - those created from our face, another face, and a third
            // face on a third brush.

        }

        Vec<Vec<float3>> finalFaces;

        // Try to reconstruct the faces into something meaningful from the l��ps
        for (auto& [face, loops] : faceLoops) {
            while (loops.size() > 0) {
                auto anchor = loops.back();
                loops.pop_back();

                auto curr = anchor;
                Vec<float3> verts = { curr.v };
                bool err = false;
                while (true) {
                    Maybe<Looplet> next;
                    size_t nextIndex = -1;

                    for (size_t i = 0; i < loops.size(); ++i) {
                        const auto& loop = loops[i];
                        if (curr.goingTo == loop.comingFrom) {
                            if (!next || dist2(loop.v, curr.v) < dist2(next->v, curr.v)) {
                                next = loop;
                                nextIndex = i;
                            }
                        }
                    }

                    // The loop back into the first vert may win over the 'next' we get here (maybe initialize
                    // to the anchor if the things are right instead?)
                    if (next) {
                        curr = *next;
                        verts.push_back(curr.v);
                        loops.erase(begin(loops) + nextIndex);
                        continue;
                    }

                    if (curr.goingTo != anchor.comingFrom) {
                        // We have no next vertex found and we're not headed back to the start, something is awry here:
                        err = true;
                        if (curr.comingFrom != anchor.goingTo)
                            debugBreak();
                    }
                    break;
                }

                if (!err)
                    finalFaces.push_back(verts);
            }
        }
    }
};


// TODO this code is atrocious:
static csg::Brush parseQuakeBrush(Tokenizer& tok) {
    using namespace csg;

    Brush ret;
    ret.type = Brush::ADDITIVE;

    std::string token;
    while ("(" == (token = tok.next())) {
        float a = std::stof(tok.next().c_str()) / 2;
        float b = std::stof(tok.next().c_str()) / 2;
        float c = std::stof(tok.next().c_str()) / 2;
        ignore(tok.next()); // )

        float3 p1(-a, c, b);

        ignore(tok.next()); // (
        a = std::stof(tok.next().c_str()) / 2;
        b = std::stof(tok.next().c_str()) / 2;
        c = std::stof(tok.next().c_str()) / 2;
        ignore(tok.next()); // )

        float3 p2(-a, c, b);

        ignore(tok.next()); // (
        a = std::stof(tok.next().c_str()) / 2;
        b = std::stof(tok.next().c_str()) / 2;
        c = std::stof(tok.next().c_str()) / 2;
        ignore(tok.next()); // )

        float3 p3(-a, c, b);

        ignore(tok.next()); // tex
        ignore(tok.next()); ignore(tok.next()); ignore(tok.next()); ignore(tok.next()); ignore(tok.next()); // boring texinfo.

        float3 normal = normalize(cross(p3 - p1, p2 - p1));
        float distance = -dot(normal, p1);

        ret.planes.push_back({ normal, distance });
    }
    tok.rewind();

    return ret;
}

QuakeMap openQuake(const std::string& path) {
    using namespace csg;

    std::ifstream input(path.c_str());
    if (!input.good()) abort();

    Tokenizer tok(input);
    int level = 0;

    Vec<Brush> brushes;

    while (tok.more()) {
        std::string token = tok.next();
        if (token == "{") {
            if (++level == 2) {
                auto brush = parseQuakeBrush(tok);
                brushes.push_back(brush);
            }
        } else if (token == "}") {
            if (--level == 0) break;
        }
    }

    QuakeMap out;

    Vec<csg::Mesh> meshes;
    for (const auto& b : brushes)
        meshes.push_back(generateMeshFromBrush(b, &out));

    CSG csg(brushes, meshes);

    return out;
}
