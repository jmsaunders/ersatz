
// A spool is a spindle of threads:
class Spool {
    Vec<std::thread> _threads;

    std::queue<std::function<void()>> _work;
    std::mutex _mutex;
    std::condition_variable _populatedNotifier;
    bool _shutdown = false;

    // We acquire a read lock on this while doing work so that we can use the write lock
    // to know that we have no outstanding workers. When combined with an empty queue, this
    // is how we notice we've drained our queue:
    std::shared_mutex _workLock;

    void worker() {
        while (true) {
            std::function<void()> work;

            {
                std::unique_lock lock(_mutex);
                if (_shutdown) return;

                if (_work.empty()) {
                    using namespace std::chrono_literals;
                    while (_work.empty()) {
                        if (_shutdown) return;
                        _populatedNotifier.wait_until(lock, std::chrono::system_clock::now() + 100ms);
                    }
                }

                _workLock.lock_shared();
                work = _work.front();
                _work.pop();
            }

            work();
            _workLock.unlock_shared();
        }
    }

public:
    Spool(size_t workers) {
        for (size_t i = 0; i < workers; ++i) {
            _threads.push_back(std::thread([this]() { this->worker(); }));
        }
    }

    ~Spool() {
        _shutdown = true;
        _populatedNotifier.notify_all();

        for (auto& t : _threads)
            t.join();
    }

    void submit(std::function<void()> job) {
        std::unique_lock lock(_mutex);
        _work.push(job);
        _populatedNotifier.notify_one();
    }

    void drain() {
        while (true) {
            std::unique_lock lock(_mutex);
            std::unique_lock workLock(_workLock);

            if (_work.empty()) return;
        }
    }
};
