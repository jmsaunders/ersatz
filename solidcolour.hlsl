cbuffer VS_CONSTANT_BUFFER : register(b0)
{
    matrix worldViewProjection;
}

struct VertexInputType
{
    float3 position : POSITION;
    float4 colour : COLOUR;
};

struct PixelInputType
{
    float4 position : SV_POSITION;
    float4 colour : COLOUR;
};

PixelInputType vertex(VertexInputType input)
{
    PixelInputType output;
    output.position = mul(float4(input.position, 1.f), worldViewProjection);
    output.colour = input.colour;
    return output;
}

float4 pixel(PixelInputType input) : SV_Target
{
    return input.colour;
}
