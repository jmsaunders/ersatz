#pragma once

#include <d3d11.h>
#include <external/miniz.h>
#include "math_common.h"

struct Shader {
    Shader(const ComPtr<ID3D11VertexShader>& vs, const ComPtr<ID3DBlob>& vsblob,
           const ComPtr<ID3D11PixelShader>& ps, const ComPtr<ID3DBlob>& psblob,
           const ComPtr<ID3D11GeometryShader>& gs, const ComPtr<ID3DBlob>& gsblob)
        : vs(vs), ps(ps), vsblob(vsblob), psblob(psblob), gs(gs), gsblob(gsblob) {
    }

    ComPtr<ID3D11VertexShader> vs;
    ComPtr<ID3D11GeometryShader> gs;
    ComPtr<ID3D11PixelShader> ps;

    ComPtr<ID3DBlob> vsblob, gsblob, psblob;

    void bind(const ComPtr<ID3D11DeviceContext>& dc);
};
Ref<Shader> compileShader(StrA path, ComPtr<ID3D11Device> dev, bool geo = false,  const Set<Str>& defines = {});

struct DrawBatch {
    ComPtr<ID3D11Buffer> indexBuffer;
    DXGI_FORMAT indexFormat;
    UINT indexBufferOffset, indexCount;

    ComPtr<ID3D11InputLayout> inputLayout;
    Ref<Shader> shader;

    ComPtr<ID3D11ShaderResourceView> texture;

    // |vbs| = |strides| = |offsets|
    Vec<ComPtr<ID3D11Buffer>> vbs;
    Vec<UINT> strides, offsets;
};

struct StaticMesh {
    Vec<DrawBatch> batches;

    void issueDraws(ID3D11DeviceContext* ctx);
};

Maybe<StaticMesh> loadMesh(StrA path, ID3D11Device* dev, ID3D11DeviceContext* context);

struct TextureInfo {
    Str name;
    ComPtr<ID3D11ShaderResourceView> srv;
    int width = -1, height = -1;
};

struct ResourceManager {
    Map<Str, CompMap<Set<Str>, Ref<Shader>>> shaders;

    Map<Str, Map<Str, Maybe<TextureInfo>>> _archiveTextures;
    Map<Str, Maybe<TextureInfo>> _textures;

    Map<Str, mz_zip_archive> _archives;

    ComPtr<ID3D11Device> m_device;
    ComPtr<ID3D11DeviceContext> m_context;

    ResourceManager(const ComPtr<ID3D11Device>& device, const ComPtr<ID3D11DeviceContext>& dc) : m_device(device), m_context(dc) {}
    Ref<Shader> requestShader(StrA path, const Set<Str>& defines, bool geo = false);

    TextureInfo defaultTexture();

    Maybe<TextureInfo> requestTexture(StrA path);
    Maybe<TextureInfo> requestTextureFromArchive(StrA archive, StrA path);
};
extern Ref<ResourceManager> resourceManager;
