#ifndef _TOKENIZER_H_
#define _TOKENIZER_H_

#include <list>
#include <string>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <assert.h>

class Tokenizer {
    Vec<unsigned char> m_data;
    StrView m_view;
    size_t m_offset = 0;

    static bool isnum(char c) {
        return c >= '0' && c <= '9';
    }

    static constexpr char m_splitTokens[] = { ' ', '\r', '\t', '\n' };
    size_t findNextBreak(StrView from) {
        if (from[0] == '"') {
            for (size_t i = 1; i < from.size(); ++i) {
                if (from[i] == '"')
                    return i + 1;
            }
            unreachable("unterminated string in parse");
        }

        // Recognize one-character things:
        if (from[0] == '(' || from[0] == ')' || from[0] == '[' || from[0] == ']') return 1;

        if (isnum(from[0]) || from[0] == '-') {
            for (size_t i = 1; i < from.size(); ++i) {
                // Just slam a shitty regex at it that'll fix it
                if (!isnum(from[i]) && from[i] != '.' && from[i] != 'e' && from[i] != '-')
                    return i;
            }
        }

        for (size_t i = 0; i < from.size(); ++i) {
            for (auto st : m_splitTokens) {
                if (from[i] == st)
                    return i;
            }
        }
        return from.size();
    }

public:

    Tokenizer(StrA filename) {
        auto file = readEntireFile(filename.c_str());
        assert(file);
        m_data = std::move(*file);
        m_view = StrView((char*)m_data.data(), m_data.size());
    }

    Tokenizer(StrView data) : m_view(data) {
    }

    bool more() {
        return m_offset < m_data.size();
    }

    void expect(StrA expected) {
        auto tok = next();
        if (tok != expected) {
            unreachable("implement error handling");
        }
    }

    StrView peek() {
        assert(m_offset < m_view.size());
        auto nextBreak = findNextBreak(StrView((char*)m_view.data() + m_offset, m_view.size() - m_offset));
        return StrView((char*)m_view.data() + m_offset, nextBreak);
    }

    StrView next() {
        auto tok = peek();
        m_offset += tok.size();

        bool more = true;
        while (more && m_offset < m_view.size()) {
            more = false;
            for (auto splitToken : m_splitTokens) {
                if (splitToken == m_view[m_offset]) {
                    ++m_offset;
                    more = true;
                    break;
                }
            }
        }

        return tok;
    }
};

#endif // _TOKENIZER_H_

