#pragma once

#include "buffer_allocator.h"

struct ID3D11Buffer;
struct ID3D11DeviceContext;

//
// There are two layers of index buffer allocations:
//  Top layer: This allocators 'sub index buffers'
//  Sub layer: Contiguous ranges where we can do a single draw call over the entire sub buffer
//
// The sub layer can expand which can cause the top layer to reconfigure where the sub layer is located within it.
//

class SubIndexAllocator;
class IndexBufferSectionAllocator {
    BufferAllocator<int> m_allocator;

public:
    IndexBufferSectionAllocator(size_t size) : m_allocator(size) {
    }

    SubIndexAllocator* alloc(size_t size);
    bool realloc(SubIndexAllocator* block, size_t newSize);
    void free(SubIndexAllocator* block);

    size_t highwater() const { return m_allocator.highwater(); }
};

struct SubIndexAllocationBlock {
    class SubIndexAllocator* parentAllocator;
    BufferAllocator<int>::Block* block;
};

struct SubIndexAllocation {
    SubIndexAllocationBlock* block = nullptr;

    SubIndexAllocation() = delete;
    SubIndexAllocation(const SubIndexAllocation&) = delete;
    SubIndexAllocation(SubIndexAllocation&& other) {
        std::swap(block, other.block);
    }
    SubIndexAllocation(SubIndexAllocationBlock* block) : block(block) {
    }

    void operator= (const SubIndexAllocation&) = delete;
    void operator= (SubIndexAllocation&& other) {
        std::swap(block, other.block);
    }

    ~SubIndexAllocation();
};

// In the SubIndexAllocator, we maintain indices we expect to draw with one call. For this reason, any spaces
// between 'online' sections require us to zero the intermediate bits otherwise we will render garbage.
class SubIndexAllocator {
    // We mirror the data inside of the index buffer because we can be moved around and have to re-upload:
    Vec<int> m_buffer;

    // This is the allocator for information inside m_buffer and our subsection of the index buffer
    BufferAllocator<int> m_allocator;

    // The parent allocator that we were created from, and our block pointer inside of it:
    IndexBufferSectionAllocator* m_parentAllocator;
    BufferAllocator<int>::Block* m_block;

    friend IndexBufferSectionAllocator;

    bool m_dirty = false;

public:
    SubIndexAllocator(size_t size, IndexBufferSectionAllocator* parentAllocator, BufferAllocator<int>::Block* block);

    SubIndexAllocationBlock* alloc(VecA<int> indices);
    void upload(ComPtr<ID3D11DeviceContext> context, ComPtr<ID3D11Buffer> indexBuffer);
    void free(SubIndexAllocationBlock* s);

    size_t offset() const { return m_block->offset; }
    size_t highwater() const { return m_allocator.highwater(); }
    bool empty() const { return m_allocator.empty(); }
};
