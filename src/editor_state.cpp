#include "common.h"
#include "editor_state.h"
#include "tokenizer.h"
#include "d3d.h"
#include "spool.h"
#include "wadfile.h"

#include <d3d11.h>

static void processBrushes(EditorWorld* world);

float4 parseTextureAxis(Tokenizer& tok) {
    tok.expect("[");
    float x = parseFloat(tok.next());
    float y = parseFloat(tok.next());
    float z = parseFloat(tok.next());
    float w = parseFloat(tok.next());
    tok.expect("]");

    return float4(-x, z, y, w);
}

static const float3 textureAxes[] = {
    float3(1, 0, 0), float3(0, 0, 1), float3(0, 1, 0),
    float3(0, 1, 0), float3(1, 0, 0), float3(0, 0, -1),
    float3(0, 0, 1), float3(-1, 0, 0), float3(0, 1, 0),
    float3(-1, 0, 0), float3(0, 0, 1), float3(0, 1, 0),
    float3(0, -1, 0), float3(1, 0, 0), float3(0, 0, -1),
    float3(0, 0, -1), float3(-1, 0, 0), float3(0, 1, 0),
};

float4x2 generateTextureAxes(float3 normal, float rot, float2 scale, float2 offs, float2 texdim) {
    int bestplane = -1;
    float bestdot = 0;

    for (unsigned int p = 0; p < 6; ++p) {
        const float ourdot = dot(normal, textureAxes[p * 3]);
        if (ourdot > bestdot) {
            bestdot = ourdot;
            bestplane = p;
        }
    }
    assert(bestplane != -1);

    float3 uaxis = textureAxes[bestplane * 3 + 1];
    float3 vaxis = textureAxes[bestplane * 3 + 2];

    if (!isZero(rot)) {
        if (bestplane >= 3) {
            rot = -rot;
            bestplane -= 3;
        }

        float4x4 r;
        if (bestplane == 0) {
            r = xRotation(toRad(rot));
        } else if (bestplane == 1) {
            r = yRotation(toRad(rot));
        } else {
            assert(bestplane == 2);
            r = zRotation(toRad(rot));
        }

        uaxis = mul(r, float4(uaxis, 0.f)).xyz();
        vaxis = mul(r, float4(vaxis, 0.f)).xyz();
    }

    return {
        float4(uaxis / scale.x, offs.x) / texdim.x,
        float4(vaxis / -scale.y, offs.y) / texdim.y,
    };
}

Plane parsePlane(Tokenizer& tok) {
    tok.expect("(");
    float a = parseFloat(tok.next());
    float b = parseFloat(tok.next());
    float c = parseFloat(tok.next());
    tok.expect(")");

    const float3 p1(-a, c, b);

    tok.expect("(");
    a = parseFloat(tok.next());
    b = parseFloat(tok.next());
    c = parseFloat(tok.next());
    tok.expect(")");

    const float3 p2(-a, c, b);

    tok.expect("(");
    a = parseFloat(tok.next());
    b = parseFloat(tok.next());
    c = parseFloat(tok.next());
    tok.expect(")");

    const float3 p3(-a, c, b);

    const float3 normal = normalize(cross(p3 - p1, p2 - p1));
    const float distance = -dot(normal, p1);

    return Plane(normal, distance);
}

static Ref<Brush> parseQuakeBrush(Tokenizer& tok, Wad& wad, bool valve = true) {
    auto ret = mkref<Brush>();
    ret->type = BrushType::ADDITIVE;

    bool clipBrush = false;

    while (tok.peek() == "(") {
        auto plane = parsePlane(tok);

        const auto textureName = tok.next();
        const auto tex = wad.request(toUpper(Str(textureName)));

        float4x2 uvspace;

        // When we want to make the TNB matrices for doing normal mapping later on:
        //tangent = axes[bestplane * 3 + 2].cross(pl.normal);
        //binormal = pl.normal.cross(axes[bestplane * 3 + 1]);

        if (tok.peek() == "[") {
            auto u = parseTextureAxis(tok);
            auto v = parseTextureAxis(tok);

            auto rot = tok.next();
            ignore(rot); // we have an axis why do we need a rotation..

            float uscale = parseFloat(tok.next());
            float vscale = parseFloat(tok.next());

            u.w *= uscale;
            v.w *= vscale;

            u /= (tex.width * uscale);
            v /= (tex.height * vscale);

            uvspace[0] = u;
            uvspace[1] = v;
        } else {
            //
            // https://github.com/id-Software/Quake-Tools/blob/c0d1b91c74eb654365ac7755bc837e497caaca73/qutils/QBSP/MAP.C#L362
            // http://www.gamers.org/dEngine/quake/spec/quake-spec34/qkspec_4.htm
            // https://github.com/defunkt/quake/blob/master/QW/client/gl_rsurf.c#L1466
            // https://github.com/stefanha/map-files/blob/master/MAPFiles.pdf
            //

            const float xoff = parseFloat(tok.next());
            const float yoff = parseFloat(tok.next());
            float rot = parseFloat(tok.next());
            const float xscale = parseFloat(tok.next());
            const float yscale = parseFloat(tok.next());

            uvspace = generateTextureAxes(plane.n, rot, float2(xscale, yscale), float2(xoff, yoff), float2(float(tex.width), float(tex.height)));
        }

        ret->textureInfo.push_back({ tex, uvspace });
        ret->planes.push_back(plane);
    }

    return ret;
}

void EditorWorld::loadMapFile(StrA path) {
    Tokenizer tok(path);
    int level = 0;

    Wad mainWad;
    mainWad.loadWad("res/quake101.wad");
    mainWad.loadWad("res/start.wad");
    mainWad.loadWad("res/makkon1.wad");
    mainWad.loadWad("res/makkon2.wad");
    mainWad.loadBsp("res/honey/maps/saint.bsp");

    //mainWad.loadBsp("res/sm211_naitelveni.bsp");

    brushes.clear();
    entities.clear();

    Ref<Entity> currentEntity;

    while (tok.more()) {
        auto token = tok.next();

        // Key value parsing:
        if (token == "\"classname\"") {
            auto tyquoted = tok.next();
            auto ty = tyquoted.substr(1, tyquoted.size() - 2);

            currentEntity = nullptr;
            if (ty == "light")
                currentEntity = mkref<LightEntity>();
            else if (ty == "func_detail")
                currentEntity = mkref<DetailEntity>();
            else if (ty.substr(0, 7) == "trigger")
                currentEntity = mkref<TriggerEntity>();
            else if (ty == "func_door")
                currentEntity = mkref<DoorEntity>();
            else if (ty.substr(0, 7) == "monster" || ty.substr(0, 4) == "item" || ty.substr(0, 6) == "weapon") {
                // Don't care about these for now:
            }
            //else
            //    OutputDebugStringA(format("Unrecognized entity: %s\n", Str(ty).c_str()).c_str());

            if (currentEntity)
                entities.push_back(currentEntity);
        }

        if (token == "{") {
            ++level;
            if (level == 1)
                currentEntity = nullptr;

            if (level == 2) {
                if (auto brush = parseQuakeBrush(tok, mainWad)) {
                    if (currentEntity) {
                        if (auto be = dyn_cast<BrushEntity>(currentEntity)) {
                            brush->index = be->brushes.size();
                            brush->parent = be.get();
                            be->brushes.push_back(brush);
                        }
                    } else {
                        brush->index = brushes.size();
                        brushes.push_back(brush);
                    }
                }
            }
        } else if (token == "}") {
            --level;
            // We're sealing an entity:
        }
    }

    TaskBatch batch;
    submit<EditorWorld>(processBrushes, this, TaskPriority::LOW, &batch);
    wait(&batch);
}

void EditorWorld::loadVmfLevel(StrA path) {
    Tokenizer tok(path);

    if (tok.peek() == "world") {
        tok.expect("world");
        tok.expect("{");

        // Skip all the boring key/val stuff:
        while (tok.peek()[0] == '"') tok.next();

        while (tok.peek() != "}") {
            auto brush = mkref<Brush>();

            tok.expect("solid");
            tok.expect("{");

            while (tok.peek()[0] == '"') tok.next();

            bool skip = false;
            while (true) {
                if (tok.peek() == "}") break;
                tok.expect("side");
                tok.expect("{");

                Plane plane({}, 0.f);
                Brush::FaceTextureInfo texInfo;
                texInfo.tex = resourceManager->defaultTexture();


                while (true) {
                    auto key = tok.next();
                    if (key == "}") break;

                    if (key[0] == '"') {
                        const auto keyName = key.substr(1, key.size() - 2);
                        const auto val = tok.next();
                        auto valtok = Tokenizer(val.substr(1, val.size() - 2));

                        if (keyName == "plane") plane = parsePlane(valtok);
                        if (keyName == "uaxis") texInfo.uvspace.x = parseTextureAxis(valtok);
                        if (keyName == "vaxis") texInfo.uvspace.y = parseTextureAxis(valtok);
                        if (keyName == "material") {
                            if (val.find("toolsclip") != StrView::npos ||
                                val.find("toolsskip") != StrView::npos) {
                                skip = true;
                            }
                        }
                    }

                    if (key == "{") {
                        int level = 1;

                        while (true) {
                            auto t = tok.next();
                            if (t == "{") ++level;
                            if (t == "}") --level;
                            if (level == 0) break;
                        }
                    }
                }

                texInfo.uvspace /= 32.f;

                brush->planes.push_back(plane);
                brush->textureInfo.push_back(texInfo);
            }

            if (!skip) {
                brush->index = brushes.size();
                brushes.push_back(brush);
            }

            tok.expect("}");
        }
        tok.expect("}");
    }

    TaskBatch batch;
    submit<EditorWorld>(processBrushes, this, TaskPriority::LOW, &batch);
    wait(&batch);
}

static void processBrushes(EditorWorld* world) {
    auto& brushes = world->brushes;
    auto& entities = world->entities;

    // Generate the base mesh and BVH tree:
    {
        Timer timer;
        TaskBatch batch;

        for (auto& b : brushes) {
            submit<Brush>([](Brush* b) {
                b->regenerateBase();
            }, b.get(), TaskPriority::HIGH, &batch);
        }

        for (auto& ent : entities) {
            if (auto be = dyn_cast<BrushEntity>(ent)) {
                for (auto b : be->brushes) {
                    submit<Brush>([](Brush* b) {
                        b->regenerateBase();
                    }, b.get(), TaskPriority::HIGH, &batch);
                }
            }
        }

        wait(&batch);
        OutputDebugStringA(format("Base meshes: %f ms\n", timer.elapsedMs()).c_str());
    }

    {
        Timer timer;
        TaskBatch batch;

        struct Packet {
            Vec<Ref<Brush>> brushes;
            BvhTree<Ref<Brush>>& bvhTree;

            Packet(VecA<Ref<Brush>> brushes, BvhTree<Ref<Brush>>& bvhTree) : brushes(brushes), bvhTree(bvhTree) {}
        };

        Vec<Packet> packets;
        packets.reserve(1 + entities.size());

        packets.push_back(Packet(brushes, world->bvhTree));
        submit<Packet>([](Packet* p) {
            for (const auto& b : p->brushes)
                p->bvhTree.insert(b, b->bounds);
        }, &packets[0], TaskPriority::HIGH, &batch);

        for (auto ent : entities) {
            if (auto be = dyn_cast<BrushEntity>(ent)) {
                packets.push_back(Packet(be->brushes, be->bvhTree));
                submit<Packet>([](Packet* p) {
                    for (const auto& b : p->brushes)
                        p->bvhTree.insert(b, b->bounds);
                }, &packets.back(), TaskPriority::HIGH, &batch);
            }
        }

        wait(&batch);
        OutputDebugStringA(format("BVH gen: %f ms\n", timer.elapsedMs()).c_str());
    }

    Timer timer;
    TaskBatch batch;

    struct Packet {
        Ref<Brush> brush;
        VecA<Ref<Brush>> brushes;
        BvhTree<Ref<Brush>>& bvhTree;
    };

    size_t required = brushes.size();
    for (auto ent : entities) {
        if (auto be = dyn_cast<BrushEntity>(ent)) {
            required += be->brushes.size();
        }
    }

    Vec<Packet> packets;
    packets.reserve(required);

    for (auto brush : brushes) {
        packets.push_back({ brush, brushes, world->bvhTree });
        submit<Packet>([](Packet* packet) {
            generateFinalFaces(packet->brush, packet->brushes, packet->bvhTree);
        }, &packets.back(), TaskPriority::HIGH, &batch);
    }

    for (auto ent : entities) {
        if (auto be = dyn_cast<BrushEntity>(ent)) {
            for (auto brush : be->brushes) {
                packets.push_back({ brush, be->brushes, be->bvhTree });

                submit<Packet>([](Packet* packet) {
                    generateFinalFaces(packet->brush, packet->brushes, packet->bvhTree);
                }, &packets.back(), TaskPriority::HIGH, &batch);
            }
        }
    }

    wait(&batch);
    OutputDebugStringA(format("CSG took: %f ms\n", timer.elapsedMs()).c_str());
}

// Only works for convex. Check that the point is on the same side of every induced plane:
bool pointInPoly(const float3& pt, VecA<CsgVertex> poly) {
    const auto normal = normalize(cross((poly[2].pos - poly[0].pos).xyz(), (poly[1].pos - poly[0].pos).xyz()));
    for (size_t i = 0; i < poly.size(); ++i) {
        const auto vert = poly[i].pos.xyz();
        const auto next = poly[(i + 1) % poly.size()].pos.xyz();

        auto edgeNormal = normalize(cross(normal, normalize((next - vert))));
        Plane edgePlane(edgeNormal, -dot(edgeNormal, vert));

        if (edgePlane.distanceToPoint(pt) > 0.1f) return false;
    }

    return true;
}

Maybe<Pair<Ref<Brush>, size_t>> EditorWorld::pickFace(const float3& pt, const float3& dir) {
    float nearest = FLT_MAX;
    Ref<Brush> nearestBrush;
    size_t nearestFaceIndex;

    Timer timer;

    // We'll get brushes we intersect here, but from there 
    const Line ray = { pt, dir };

    auto brushFinder = [&](const Ref<Brush>& brush, float& nearest) {
        // Ignore trigger brushes for now:
        if (brush->parent && dynamic_cast<TriggerEntity*>(brush->parent)) return;

        for (const auto& face : brush->finalFaces) {
            const Plane& fplane = brush->planes[face.planeNo];

            if (fastdot(ray.dir, fplane.n) > 0) continue;

            if (const auto t = fplane.intersectTime(ray)) {
                if (*t < 0) continue;

                const auto intersection = ray.atTime(*t);
                if (*t < nearest && pointInPoly(intersection, face.verts)) {
                    nearest = *t;
                    nearestBrush = brush;
                    nearestFaceIndex = face.planeNo;
                }
            }
        }
    };

    bvhTree.raycast(ray, nearest, brushFinder);

    for (const auto& ent : entities) {
        if (auto brushEnt = dyn_cast<BrushEntity>(ent)) {
            brushEnt->bvhTree.raycast(ray, nearest, brushFinder);
        }
    }

    OutputDebugStringA(format("Found in %f ms\n", timer.elapsedMs()).c_str());

    if (nearestBrush)
        return std::make_pair(nearestBrush, nearestFaceIndex);
    return none;
}

Ref<Brush> EditorWorld::pickBrush(const float3& pt, const float3& dir) {
    if (auto face = pickFace(pt, dir))
        return face->first;
    return nullptr;
}

void EditorWorld::findBrushesInteract(Set<Ref<Brush>>& out, const Set<Ref<Brush>>& in) {
    for (const auto& brush : in) {
        auto& tree = brush->parent ? brush->parent->bvhTree : bvhTree;
        visitBvhNode<Ref<Brush>>(tree.root, brush->bounds, [&](RefA<Brush> brush) {
            out.insert(brush);
        });
    }
}

