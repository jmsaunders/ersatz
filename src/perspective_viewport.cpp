﻿#include "common.h"
#include "perspective_viewport.h"
#include "csg.h"
#include "buffer_allocator.h"
#include "map_render.h"

#include <numeric>

struct MoveBrushTool : TransformTool {
    Ref<Brush> brush;
    float3 start;
    Vec<Plane> startingPlanes;

    MoveBrushTool(RefA<Brush> brush, tinygizmo::rigid_transform transform, PerspectiveViewport& viewport, EditorWorld& world)
        : TransformTool(transform, viewport, world), brush(brush), start(transform.position), startingPlanes(brush->planes) {
    }

    void cancel() override {
        assert(_world.selectedBrushes.size() == 1);
        auto brush = *_world.selectedBrushes.begin();

        viewport.processBrushAction(brush, [&](RefA<Brush> brush) {
            brush->planes = startingPlanes;
            brush->regenerateBase();
        });
    }

    void update() override {
        const float3 delta = transform.position - start;
        auto brush = *_world.selectedBrushes.begin();

        viewport.processBrushAction(brush, [&](RefA<Brush> brush) {
            brush->planes = transformPlanes(delta, startingPlanes);
            brush->regenerateBase();
        });

    }

    void commit() override {
        auto b = *_world.selectedBrushes.begin();
        auto cmd = mkref<TransformBrushCommand>(b, startingPlanes);
        _world.undoManager.addCommand(cmd);
    }
};

struct MoveFaceTool : TransformTool {
    Ref<Brush> brush;
    size_t face;
    float3 start;
    Vec<Plane> startingPlanes;

    MoveFaceTool(RefA<Brush> brush, size_t face, tinygizmo::rigid_transform transform, PerspectiveViewport& viewport, EditorWorld& world)
        : TransformTool(transform, viewport, world), brush(brush), face(face), start(transform.position), startingPlanes(brush->planes) {
    }

    void cancel() override {
        viewport.processBrushAction(brush, [&](RefA<Brush> brush) {
            brush->planes = startingPlanes;
            brush->regenerateBase();
        });
    }

    void update() override {
        const float3 baseDelta = transform.position - start;
        const float delta = dot(baseDelta, brush->planes[face].n);

        viewport.processBrushAction(brush, [&](RefA<Brush> brush) {
            auto currentPlanes = brush->planes;
            brush->planes[face].d = startingPlanes[face].d - delta;
            if (!brush->regenerateBase(false)) {
                brush->planes = currentPlanes;
            }
        });
    }

    void commit() override {
        auto cmd = mkref<TransformBrushCommand>(brush, startingPlanes);
        _world.undoManager.addCommand(cmd);
    }
};

Plane planeFromPointsPreservingOrientation(float3 p1, float3 p2, float3 p3, Plane prev) {
    float3 normal = normalize(cross(p3 - p1, p2 - p1));
    if (dot(normal, prev.n) < 0) normal = -normal;
    const float distance = -dot(normal, p1);

    return Plane(normal, distance);
}

struct MoveEdgeTool : TransformTool {
    EdgeSelection edge;
    Vec<Plane> startingPlanes;
    float3 start;

    MoveEdgeTool(const EdgeSelection& edge, tinygizmo::rigid_transform transform, PerspectiveViewport& viewport, EditorWorld& world)
        : TransformTool(transform, viewport, world), edge(edge), startingPlanes(edge.brush->planes), start(transform.position) {
    }

    void cancel() override {
        viewport.processBrushAction(edge.brush, [&](RefA<Brush> brush) {
            brush->planes = startingPlanes;
            brush->regenerateBase();
        });
    }

    void update() override {
        const float3 baseDelta = transform.position - start;

        viewport.processBrushAction(edge.brush, [&](RefA<Brush> brush) {
            const float3 edgeVert1 = edge.edgeVert1 + baseDelta;
            const float3 edgeVert2 = edge.edgeVert2 + baseDelta;

            auto currentPlanes = brush->planes;

            auto& plane1 = edge.brush->planes[edge.plane1];
            plane1 = planeFromPointsPreservingOrientation(edgeVert1, edgeVert2, edge.anchorVert1, plane1);
            auto& plane2 = edge.brush->planes[edge.plane2];
            plane2 = planeFromPointsPreservingOrientation(edgeVert1, edgeVert2, edge.anchorVert2, plane2);

            if (!brush->regenerateBase(false)) {
                brush->planes = currentPlanes;
            }
        });
    }

    void commit() override {
        auto cmd = mkref<TransformBrushCommand>(edge.brush, startingPlanes);
        _world.undoManager.addCommand(cmd);
    }
};

const float movementDamp = 0.8f;
const float rotationDamp = 0.004f;

struct GizmoVertex {
    float3 pos;
    float4 colour;
};

constexpr D3D11_INPUT_ELEMENT_DESC k_gizmoVertexLayout[] = {
    { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(GizmoVertex, pos), D3D11_INPUT_PER_VERTEX_DATA, 0 },
    { "COLOUR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, offsetof(GizmoVertex, colour), D3D11_INPUT_PER_VERTEX_DATA, 0 },
};

PerspectiveViewport::PerspectiveViewport(EditorWorld* world, ComPtrA<ID3D11Device> device, ComPtrA<ID3D11DeviceContext> context)
        : EditorViewport(device, context), _world(world), _renderer(device, context, world) {

    // Hm these should probably be done kind of differently maybe?
    {
        D3D11_BUFFER_DESC desc = {};
        desc.ByteWidth = sizeof(GizmoVertex) * 1024 * 512;
        desc.Usage = D3D11_USAGE_DYNAMIC;
        desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
        desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
        if (FAILED(device->CreateBuffer(&desc, nullptr, &_vertexBuffer))) unreachable("failed to create vertex buffer");
    }

    {
        D3D11_BUFFER_DESC desc = {};
        desc.ByteWidth = sizeof(unsigned int) * 1024 * 512;
        desc.Usage = D3D11_USAGE_DYNAMIC;
        desc.BindFlags = D3D11_BIND_INDEX_BUFFER;
        desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
        if (FAILED(device->CreateBuffer(&desc, nullptr, &_indexBuffer))) unreachable("failed to create vertex buffer");
    }

    _gizmo.render = [this](const tinygizmo::geometry_mesh& mesh) {
        Vec<GizmoVertex> verts;

        for (const auto& v : mesh.vertices)
            verts.push_back({ v.position, float4(v.color.xyz(), 1.f) });

        {
            D3D11_MAPPED_SUBRESOURCE res;
            _context->Map(_vertexBuffer.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &res);
            memcpy(res.pData, verts.data(), verts.size() * sizeof(verts[0]));
            _context->Unmap(_vertexBuffer.Get(), 0);
        }

        const auto& indices = mesh.triangles;
        {
            D3D11_MAPPED_SUBRESOURCE res;
            _context->Map(_indexBuffer.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &res);
            memcpy(res.pData, indices.data(), indices.size() * sizeof(indices[0]));
            _context->Unmap(_indexBuffer.Get(), 0);
            _gizmoElements = UINT(indices.size() * 3);
        }
    };

    if (auto maybeShader = resourceManager->requestShader("solidcolour.hlsl", {})) {
        _gizmoShader = maybeShader;
    } else {
        unreachable("no shader");
    }

    if (FAILED(device->CreateInputLayout(k_gizmoVertexLayout, _countof(k_gizmoVertexLayout),
            _gizmoShader->vsblob->GetBufferPointer(), _gizmoShader->vsblob->GetBufferSize(), &_gizmoInputLayout))) {
        unreachable("couldn't create gizmo vertex layout");
    }
}

const float worldscale = 1.f / 4;
static const auto fov = toRad(95.f);
static constexpr float nearclip = 0.5f, farclip = 1024.f;

float3 PerspectiveViewport::directionOfScreenPos(float2 pos) const {
    const auto viewportSize = size();
    float4 screenProj(2 * (pos.x / viewportSize.x) - 1, -2 * (pos.y / viewportSize.y) + 1, 0, 1);

    const float aspect = float(viewportSize.x) / viewportSize.y;

    const auto modelViewProjection = mul(_perspectiveProjection, _rotation);
    const auto modelViewProjectionInv = inverse(modelViewProjection);
    return mul(modelViewProjectionInv, screenProj).xyz();
}

void doThing(bool& pressed, unsigned int color, const char* buttonName) {
    if (pressed)
        ImGui::PushStyleColor(ImGuiCol_Button, color);
    else
        ImGui::PushStyleColor(ImGuiCol_Button, ImVec4{ 0.2f, 0.3f, 0.4f, 1.0f });

    if (ImGui::Button(buttonName))
        pressed = !pressed;

    ImGui::PopStyleColor(1);
}

void CreateBrushTool::updateBrush() {
    bool first = brush == nullptr;
    if (!brush) {
        brush = mkref<Brush>();
        brush->type = BrushType::ADDITIVE;
        brush->index = world->brushes.size();
        world->brushes.push_back(brush);
    }

    auto& planes = brush->planes;
    planes.clear();

    planes.push_back(Plane(float3(0, 1, 0), -height));
    planes.push_back(Plane(float3(0, -1, 0), 0));

    for (int i = 0; i < sides; ++i) {
        const float angle = 2 * pi * float(i) / sides;
        const float c = cos(angle), s = sin(angle);
        const float3 n(c, 0, s);
        planes.push_back(Plane(n, -radius + dot(n, offs)));
    }

    planes = transformPlanes(position, planes);

    const auto tex = resourceManager->defaultTexture();
    brush->textureInfo.clear();
    for (auto p : planes)
        brush->textureInfo.push_back({ tex, generateTextureAxes(p.n, 0, float2(.25f, .25f), {}, float2(float(tex.width), float(tex.height))) });

    brush->regenerateBase();
    if (first) 
        world->bvhTree.insert(brush, brush->bounds);
    else
        world->bvhTree.update(brush, brush->bounds);
}

bool CreateBrushTool::layout() {
    bool live = true;

    bool dirty = false;
    if (ImGui::Begin("Create brush", &live)) {
        dirty |= ImGui::SliderInt("Sides", &sides, 3, 32);
        dirty |= ImGui::SliderFloat("Radius", &radius, 10, 1000);
        dirty |= ImGui::SliderFloat("Height", &height, 10, 1000);
        dirty |= ImGui::SliderFloat("Xoff", &offs.x, -1000, 1000);
        dirty |= ImGui::SliderFloat("zoff", &offs.z, -1000, 1000);

        if (ImGui::Button("Cancel")) {
            live = false;
        }
    }
    ImGui::End();

    if (dirty) {
        updateBrush();
    }

    if (!live) done = true;

    return dirty;
}

template<typename Fn>
void PerspectiveViewport::processBrushAction(const Ref<Brush>& brush, const Fn& fn) {
    auto& bvhTree = brush->parent ? brush->parent->bvhTree : _world->bvhTree;

    Set<Ref<Brush>> vicinity;
    _world->findBrushesInteract(vicinity, { brush });

    fn(brush);

    bvhTree.update(brush, brush->bounds);

    _world->findBrushesInteract(vicinity, { brush });

    generateCsgGeometry(_world->brushes, bvhTree, vicinity);
    _renderer.updateVertexBuffer(vicinity);
}

void PerspectiveViewport::layout() {
    using namespace ImGui;

    SameLine(); Checkbox("lines", &_drawLines);
    SameLine(); Checkbox("world", &_drawWorld);

    SameLine();
    if (Button(k_selectionModeNames[int(_world->selectionMode)]))
        OpenPopup("selection_mode_popup");

    if (BeginPopup("selection_mode_popup")) {
        const auto selectionModes = {
            SelectionMode::Object,
            SelectionMode::Face,
            SelectionMode::Edge,
            SelectionMode::Vertex,
        };
        for (auto i : selectionModes) {
            if (MenuItem(k_selectionModeNames[int(i)])) {
                _world->setSelectionMode(i);
            }
        }
        EndPopup();
    }

    SameLine();
    if (Button("Ngon")) {
        float3 dir = _rotation.row(2).xyz();
        _currentTool = mkref<CreateBrushTool>(_world, -_translation, -dir);

        Set<Ref<Brush>> vicinity;
        _world->findBrushesInteract(vicinity, { _currentTool->brush });
        generateCsgGeometry(_world->brushes, _world->bvhTree, vicinity);
        _renderer.updateVertexBuffer(vicinity);
    }

    SameLine();
    if (Button("Invert")) {
        Set<Ref<Brush>> vicinity;

        for (auto brush : _world->selectedBrushes) {
            brush->flip();
            brush->regenerateBase();

            _world->findBrushesInteract(vicinity, { brush });
        }

        auto cmd = mkref<InvertBrushCommand>(_world->selectedBrushes);
        _world->undoManager.addCommand(cmd);

        if (vicinity.size()) {
            generateCsgGeometry(_world->brushes, _world->bvhTree, vicinity);
            _renderer.updateVertexBuffer(vicinity);
        }
    }

    SameLine();
    if (Button(format("Grid (%.0f)", _translationSnap).c_str()))
        OpenPopup("grid_size_popup");

    if (BeginPopup("grid_size_popup")) {
        for (size_t i = 1; i < 128; i <<= 1) {
            if (MenuItem(format("%d", i).c_str())) {
                _translationSnap = float(i);
            }
        }
        EndPopup();
    }

    SameLine();
    if (Button(format("Bring to..", _translationSnap).c_str()))
        OpenPopup("bring_to_popup");

    if (BeginPopup("bring_to_popup")) {
        if (MenuItem("front")) {
        }
        if (MenuItem("back")) {
        }
        EndPopup();
    }

    SameLine(); Text("pos: %f,%f,%f", _translation.x, _translation.y, _translation.z);
    auto selected = _world->brushesInSelection();
    SameLine(); Text("selected parent: 0x%x", selected.size() > 0 ? (*selected.begin())->parent : 0);

    if (_currentTool) {
        Set<Ref<Brush>> vicinity;
        _world->findBrushesInteract(vicinity, { _currentTool->brush });

        if (_currentTool->layout()) {
            _world->bvhTree.update(_currentTool->brush, _currentTool->brush->bounds);
            _world->findBrushesInteract(vicinity, { _currentTool->brush });

            generateCsgGeometry(_world->brushes, _world->bvhTree, vicinity);
            _renderer.updateVertexBuffer(vicinity);
        }

        if (_currentTool->done) {
            auto cmd = mkref<AppendBrushCommand>(_currentTool->brush);
            _world->undoManager.addCommand(cmd);
            _currentTool = nullptr;
        }
    }

    // Keyboard shortucts:
    // Better placed somewhere further up definitely...

    auto& io = ImGui::GetIO();
    if (g_keyDownInLastFrame[VK_DELETE]) {
        if (io.KeyShift) {
            Set<Ref<Brush>> toDelete;
            for (const auto& b : _world->brushes) {
                if (_world->selectedBrushes.find(b) == end(_world->selectedBrushes))
                    toDelete.insert(b);
            }

            Set<Ref<Brush>> vicinity;
            _world->findBrushesInteract(vicinity, toDelete);

            auto cmd = mkref<DeleteBrushesCommand>(toDelete);
            _world->undoManager.addCommand(cmd);
            cmd->redo(*_world);

            _world->selectedBrushes.clear();

            generateCsgGeometry(_world->brushes, _world->bvhTree, vicinity);
            _renderer.updateVertexBuffer(vicinity);
        } else if (!_world->selectedBrushes.empty()) {
            Set<Ref<Brush>> vicinity;
            _world->findBrushesInteract(vicinity, _world->selectedBrushes);

            auto cmd = mkref<DeleteBrushesCommand>(_world->selectedBrushes);
            _world->undoManager.addCommand(cmd);
            cmd->redo(*_world);

            _world->selectedBrushes.clear();

            generateCsgGeometry(_world->brushes, _world->bvhTree, vicinity);
            _renderer.updateVertexBuffer(vicinity);
        }
    }

    // @TODO incomplete
    if (io.KeyCtrl) {
        if (g_keyDownInLastFrame['A']) {
            Set<Ref<Brush>> vicinity;
            for (auto s : _world->selectedBrushes) {
                auto prev = s->parent;
                s->parent = nullptr;
                _world->findBrushesInteract(vicinity, { s });
                if (prev) {
                    s->index = _world->brushes.size();
                    _world->brushes.push_back(s);
                    _world->bvhTree.insert(s, s->bounds);
                    _world->findBrushesInteract(vicinity, { s });
                }
            }

            generateCsgGeometry(_world->brushes, _world->bvhTree, vicinity);
            _renderer.updateVertexBuffer(vicinity);
        }
    }


    if (g_keyDownInLastFrame[VK_ESCAPE]) {
        if (_currentTool) {
            _currentTool = nullptr;
        } else if (_transformTool) {
            _transformTool->cancel();
            _transformTool = nullptr;
        } else {
            auto selected = _world->brushesInSelection();

            _world->clearSelection();
            processDirtyBrushes(selected);
        }
    }
}

tinygizmo::gizmo_application_state PerspectiveViewport::makeGizmoState() const {
    ImGuiIO& io = ImGui::GetIO();

    const auto cursorPos = ImGui::GetMousePos();
    const auto widgetPos = ImGui::GetItemRectMin();

    bool othersDown = false;
    for (int i = 1; i < 5; ++i) {
        if (io.MouseDown[i]) {
            othersDown = true;
            break;
        }
    }

    const float localX = cursorPos.x - widgetPos.x, localY = cursorPos.y - widgetPos.y;
    const auto viewportSize = size();

    tinygizmo::gizmo_application_state state;
    state.hotkey_translate = true;
    state.mouse_left = io.MouseDown[0] && !othersDown;
    //state.worldspace_scale = 75.f;
    state.screenspace_scale = 12.f;
    state.cam.near_clip = nearclip;
    state.cam.far_clip = farclip;
    state.cam.position = -_translation;
    state.cam.yfov = fov;
    state.ray_direction = directionOfScreenPos(float2(localX, localY));
    state.viewport_size = { viewportSize.x, viewportSize.y };
    state.ray_origin = -_translation;
    const float3x3 rot(_rotation.row(0).xyz(), _rotation.row(1).xyz(), _rotation.row(2).xyz());
    state.cam.orientation = linalg::rotation_quat(rot);

    state.snap_translation = _translationSnap;

    return state;
}

static const char* k_transformGizmoId = "transform gizmo";

void PerspectiveViewport::drawInternal() {
    float bg[] = { 0.2f, 0.2f, 0.2f, 1.0f };
    _context->ClearRenderTargetView(_renderTargetView.Get(), bg);
    _context->ClearDepthStencilView(_depthStencilView.Get(), D3D11_CLEAR_DEPTH, 1.f, 0);

    if (_world->dirtyBrushes.size() > 0) {
        processDirtyBrushes({});
    }

    const auto viewportSize = size();

    ImGuiIO& io = ImGui::GetIO();
    if (io.MouseReleased[0]) {
        if (_transformTool) {
            _transformTool->commit();
            _transformTool = nullptr;
        }
    }

    {
        bool drawGizmo = true;
        if (_transformTool) {
            _gizmo.update(makeGizmoState());
            tinygizmo::transform_gizmo(k_transformGizmoId, _gizmo, _transformTool->transform);
        } else if (_world->selectedBrushes.size() == 1) {
            auto brush = *_world->selectedBrushes.begin();

            tinygizmo::rigid_transform transform;
            transform.position = brush->bounds.center();

            _gizmo.update(makeGizmoState());
            if (tinygizmo::transform_gizmo(k_transformGizmoId, _gizmo, transform) && io.MouseClicked[0]) {
                _transformTool = mkref<MoveBrushTool>(brush, transform, *this, *_world);
            }
        } else if (_world->selectedFaces.size() == 1) {
            auto [brush, faceIndex] = *_world->selectedFaces.begin();
            const auto& face = brush->baseMesh.faces[faceIndex];
            const auto center = std::accumulate(begin(face.verts), end(face.verts), float4()) / face.verts.size();

            tinygizmo::rigid_transform transform;
            transform.position = center.xyz();

            // @TODO
            // This I think supplies us with our deltas in this orientation frame which is annoying to work with.
            const auto normal = brush->planes[faceIndex].n;
            const auto binormal = normalize(brush->textureInfo[faceIndex].uvspace.x.xyz());
            const auto tangent = cross(normal, binormal);
            const float3x3 orientation = { normal, binormal, tangent, };

            //transform.orientation = rotation_quat(orientation);

            _gizmo.update(makeGizmoState());
            if (tinygizmo::transform_gizmo(k_transformGizmoId, _gizmo, transform) && io.MouseClicked[0]) {
                _transformTool = mkref<MoveFaceTool>(brush, faceIndex, transform, *this, *_world);
            }
        } else if (_world->selectedEdges) {
            const auto& edge = *_world->selectedEdges;
            tinygizmo::rigid_transform transform;

            transform.position = (edge.edgeVert1 + edge.edgeVert2) / 2;

            _gizmo.update(makeGizmoState());
            if (tinygizmo::transform_gizmo(k_transformGizmoId, _gizmo, transform) && io.MouseClicked[0]) {
                _transformTool = mkref<MoveEdgeTool>(edge, transform, *this, *_world);
            }
        } else {
            _gizmoElements = 0;
            drawGizmo = false;
        }

        if (drawGizmo) _gizmo.draw();
    }

    // Figure out where we're looking at:
    float4x4 modelView, projection;
    {
        const auto s = scaling_matrix<float>({ worldscale, worldscale, worldscale });
        const auto t = translation_matrix<float>(_translation);

        modelView = transpose(mul(mul(s, _rotation), t));

        const float aspect = float(viewportSize.x) / viewportSize.y;
        _perspectiveProjection = perspective_matrix(fov, aspect, 0.5f, 4096.f, linalg::neg_z, linalg::zero_to_one);

        projection = transpose(_perspectiveProjection);
    }

    _renderer.draw(_context, mul(modelView, projection), float2(viewportSize.x, viewportSize.y), _drawLines, _drawWorld);

    if (_gizmoElements > 0) {
        _context->ClearDepthStencilView(_depthStencilView.Get(), D3D11_CLEAR_DEPTH, 1.f, 0);
        _context->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

        _gizmoShader->bind(_context);
        _context->IASetInputLayout(_gizmoInputLayout.Get());
        UINT strides[] = { sizeof(GizmoVertex) }, offsets[] = { 0 };
        _context->IASetVertexBuffers(0, 1, _vertexBuffer.GetAddressOf(), strides, offsets);
        _context->IASetIndexBuffer(_indexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);
        _context->DrawIndexed(_gizmoElements, 0, 0);
    }
}

void PerspectiveViewport::mouseDown(int button, int x, int y) {
    ImGuiIO& io = ImGui::GetIO();

    // If this is the start of the mouse action, then keep a handle of where it is:
    bool startingGesture = true;
    for (int i = 0; i < 5; ++i) {
        if (io.MouseDown[i] && button != i) {
            startingGesture = false;
            break;
        }
    }

    if (startingGesture)
        _buttonDownPosition = float2(float(x), float(y));
}

static float3 findAnchorVertex(VecA<float4> pts, float3 a, float3 b) {
    for (const auto& v4 : pts) {
        auto v = v4.xyz();
        if (distance2(v, a) > defaultEpsilon && distance2(v, b) > defaultEpsilon)
            return v;
    }
    unreachable("couldn't find other vertex");
}

static Maybe<EdgeSelection> findBestEdge(EditorWorld& world, float3 pos, float3 dir) {
    if (auto f = world.pickFace(pos, dir)) {
        const auto brush = f->first;
        const auto faceIndex = f->second;

        const auto face = brush->baseMesh.faces[faceIndex];

        // TODO find the best line here:
#if 0
        for (size_t i = 0; i < face.verts.size(); ++i) {
            auto fst = face.verts[i], snd = face.verts[(i + 1) % face.verts.size()];
        }
#endif
        const float3 a = face.verts[0].xyz(), b = face.verts[1].xyz();
        const Line bestLine = { a, b-a };

        // Find the other plane involved:
        size_t otherPlane = -1;
        for (size_t i = 0; i < brush->planes.size(); ++i) {
            if (i == faceIndex) continue;
            if (brush->planes[i].side(a) == Side::ON && brush->planes[i].side(b) == Side::ON) {
                otherPlane = i;
                break;
            }
        }
        assert(otherPlane != -1);

        EdgeSelection ret = {
            brush,
            faceIndex, otherPlane,
            findAnchorVertex(brush->baseMesh.faces[faceIndex].verts, a, b), findAnchorVertex(brush->baseMesh.faces[otherPlane].verts, a, b),
            a, b
        };
        return ret;
    }
    return none;
}

void PerspectiveViewport::mouseUp(int button, int x, int y) {
    ImGuiIO& io = ImGui::GetIO();
    if (distance(float2(float(x), float(y)), _buttonDownPosition) > 4) return;

    const auto viewportSize = size();

    if (button == 0 && !_transformTool) {
        const auto projectedDir = directionOfScreenPos(float2(float(x), float(y)));

        switch (_world->selectionMode) {
            case SelectionMode::Object: {
                if (auto b = _world->pickBrush(-_translation, projectedDir)) {
                    auto it = _world->selectedBrushes.find(b);
                    if (it == _world->selectedBrushes.end())
                        _world->selectedBrushes.insert(b);
                    else
                        _world->selectedBrushes.erase(it);

                    processDirtyBrushes({ b });
                }

                break;
            }

            case SelectionMode::Face: {
                if (auto f = _world->pickFace(-_translation, projectedDir)) {
                    auto it = _world->selectedFaces.find(*f);
                    if (it == _world->selectedFaces.end())
                        _world->selectedFaces.insert(*f);
                    else
                        _world->selectedFaces.erase(it);

                    processDirtyBrushes({ f->first });
                }
                break;
            }

            case SelectionMode::Edge: {
                _world->selectedEdges = findBestEdge(*_world, -_translation, projectedDir);
                break;
            }
        }
    }
}

void PerspectiveViewport::processDirtyBrushes(const Set<Ref<Brush>>& brushes) {
    for (const auto& b : brushes) _world->dirtyBrushes.insert(b);
    _renderer.updateVertexBuffer(_world->dirtyBrushes);
    _world->dirtyBrushes.clear();
}

void PerspectiveViewport::mouseDrag(int dx, int dy) {
    auto& io = ImGui::GetIO();
    const float shiftDamp = io.KeysDown[VK_SHIFT] ? 0.1f : 1.f;

    if (_transformTool) {
        _transformTool->update();
        return;
    }

    if (io.MouseDown[0] && io.MouseDown[1]) {
        auto localX = _rotation.row(0).xyz();
        _translation -= localX * dx * movementDamp * shiftDamp;
        _translation += float3(0, 1, 0) * dy * movementDamp * shiftDamp;
    } else if (io.MouseDown[0]) {
        _rotation = mul(_rotation, yRotation(-dx * rotationDamp));

        auto localZ = _rotation.row(2).xyz();
        auto forward = localZ;
        forward.y = 0.f;
        forward = normalize(forward);
        _translation -= forward * dy * movementDamp * shiftDamp;
    } else if (io.MouseDown[1]) {
        _rotation = mul(xRotation(-dy * rotationDamp), _rotation);
        _rotation = mul(_rotation, yRotation(-dx * rotationDamp));
    }

    const float defaultMinimumAngle = 0.01f;

    // Restrict the angle the camera is looking at:
    {
        float3 globalUp(0, 1, 0);
        float3 localUp = _rotation.row(1).xyz();
        float3 localFwd = _rotation.row(2).xyz();

        // When the dot product is negative, that means the two vectors are more 
        // than 90 degrees apart, which in turn means that we've rotated too far
        // and are now looking at an impossible angle (bad).

        float localDotGlobal = dot(localUp, globalUp);
        if (localDotGlobal < 0.01) {
            float angle = acos(localDotGlobal) - pi / 2 + defaultMinimumAngle;
            _rotation = mul(xRotation(localFwd[1] < 0 ? -angle : angle), _rotation);
        }
    }
}
