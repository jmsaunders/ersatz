#pragma once

#include "d3d.h"

// Quake-style WAD file loader:
// https://www.gamers.org/dEngine/quake/spec/quake-spec34/qkspec_7.htm
// https://github.com/joshuaskelly/quake-cli-tools/blob/master/qcli/bsp2wad/cli.py
class Wad {
    Vec<MappedFile> _files;
    Map<Str, TextureInfo> _loaded;

    struct PicData {
        uint32_t w, h;
        const unsigned char* loc;
    };
    Map<Str, PicData> _pics;

public:

    void loadWad(StrA path);
    void loadBsp(StrA path);

    TextureInfo request(StrA name);
};
