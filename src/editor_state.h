#pragma once

#include "csg.h"
#include "bvh_tree.h"
#include "commands.h"

//
// Entities:
//

struct Entity {
    virtual ~Entity() {}
};

struct LightEntity : Entity {
    float3 pos;
    float radius;
};

// An entity which has brushes:
struct BrushEntity : Entity {
    Vec<Ref<Brush>> brushes;
    BvhTree<Ref<Brush>> bvhTree;
};

struct DetailEntity : BrushEntity {
};

struct TriggerEntity : BrushEntity {
};

struct DoorEntity : BrushEntity {
};

enum class SelectionMode {
    Object,
    Face,
    Edge,
    Vertex
};

static constexpr const char* k_selectionModeNames[] = {
    "Object", "Face", "Edge", "Vertex",
};

struct EdgeSelection {
    Ref<Brush> brush;
    size_t plane1, plane2;
    float3 anchorVert1, anchorVert2;
    float3 edgeVert1, edgeVert2;
};

struct EditorWorld {
    EditorWorld() : undoManager(*this) {}

    Vec<Ref<Brush>> brushes;
    BvhTree<Ref<Brush>> bvhTree;
    Vec<Ref<Entity>> entities;

    void setSelectionMode(SelectionMode newSelectionMode) {
        selectionMode = newSelectionMode;

        dirtyBrushes = brushesInSelection();
        clearSelection();
    }

    void clearSelection() {
        selectedBrushes.clear();
        selectedFaces.clear();
        selectedEdges = none;
    }

    Set<Ref<Brush>> brushesInSelection() const {
        switch (selectionMode) {
            case SelectionMode::Object:
                return selectedBrushes;

            case SelectionMode::Face: {
                Set<Ref<Brush>> ret;
                for (auto& f : selectedFaces) ret.insert(f.first);
                return ret;
            }
        }

        return {};
    }

    SelectionMode selectionMode = SelectionMode::Face;
    Set<Ref<Brush>> selectedBrushes;
    Set<Pair<Ref<Brush>, size_t>> selectedFaces;
    Maybe<EdgeSelection> selectedEdges;

    UndoManager undoManager;

    // Brushes which require an update:
    Set<Ref<Brush>> dirtyBrushes;

    // Picking:
    Ref<Brush> pickBrush(const float3& pt, const float3& dir);
    Maybe<Pair<Ref<Brush>, size_t>> pickFace(const float3& pt, const float3& dir);

    void findBrushesInteract(Set<Ref<Brush>>& out, const Set<Ref<Brush>>& in);

    // Quake-style maps
    void loadMapFile(StrA path);

    // Valve-style maps
    void loadVmfLevel(StrA path);
};

struct CreateBrushTool {
    CreateBrushTool(EditorWorld* world, float3 pos, float3 fwd) : world(world) {
        position = pos + fwd * 150;
        updateBrush();
    }

    EditorWorld* world;
    Ref<Brush> brush;

    float3 position;
    int axis = 0;
    float radius = 50.f, height = 100.f;
    int sides = 4;

    float3 offs = { 0.f, 0.f, 0.f };

    bool done = false;

    bool layout();
    void updateBrush();
};
