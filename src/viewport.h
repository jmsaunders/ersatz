#pragma once

#include <d3d11.h>
#include "imgui.h"

struct EditorViewport {
    EditorViewport(ComPtrA<ID3D11Device> device, ComPtrA<ID3D11DeviceContext> context) : _device(device), _context(context) {
    }

    void update();

protected:
    virtual Str name() = 0;
    virtual void drawInternal() = 0;

    virtual void mouseDown(int button, int x, int y) {}
    virtual void mouseUp(int button, int x, int y) {}
    virtual void mouseDrag(int dx, int dy) {}
    virtual void mouseWheel(bool up, int x, int y) {}

    virtual void layout() {}

    bool _stealingInput = false;

    ComPtr<ID3D11Device> _device;
    ComPtr<ID3D11DeviceContext> _context;
    ComPtr<ID3D11RenderTargetView> _renderTargetView;
    ComPtr<ID3D11ShaderResourceView> _shaderResourceView;
    ComPtr<ID3D11Texture2D> _texture, _resolvedTexture;
    ComPtr<ID3D11DepthStencilView> _depthStencilView;

    ImVec2 size() const;

private:
    void resetViewport(size_t width, size_t height);
    bool viewportNeedsReset(size_t width, size_t height);
};
