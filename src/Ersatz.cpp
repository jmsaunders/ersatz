#include "common.h"
#include "d3d.h"
#include "math_common.h"
#include "csg.h"

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#include "imgui.h"
#include "examples/imgui_impl_dx11.h"

#include "perspective_viewport.h"
#include "orthographic_viewport.h"
#include "spool.h"

int width = 2560, height = 1600;

namespace {
    const auto className = L"Ersatz";
    float dpiScale = 2.f;
}

bool g_keyDownInLastFrame[256];

IMGUI_IMPL_API LRESULT ImGui_ImplWin32_WndProcHandler(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam) {
    if (ImGui::GetCurrentContext() == NULL)
        return 0;

    ImGuiIO& io = ImGui::GetIO();
    switch (msg) {
    case WM_LBUTTONDOWN: case WM_LBUTTONDBLCLK:
    case WM_RBUTTONDOWN: case WM_RBUTTONDBLCLK:
    case WM_MBUTTONDOWN: case WM_MBUTTONDBLCLK:
    case WM_XBUTTONDOWN: case WM_XBUTTONDBLCLK: {
        int button = 0;
        if (msg == WM_LBUTTONDOWN || msg == WM_LBUTTONDBLCLK) { button = 0; }
        if (msg == WM_RBUTTONDOWN || msg == WM_RBUTTONDBLCLK) { button = 1; }
        if (msg == WM_MBUTTONDOWN || msg == WM_MBUTTONDBLCLK) { button = 2; }
        if (msg == WM_XBUTTONDOWN || msg == WM_XBUTTONDBLCLK) { button = (GET_XBUTTON_WPARAM(wParam) == XBUTTON1) ? 3 : 4; }
        if (!ImGui::IsAnyMouseDown() && ::GetCapture() == NULL)
            ::SetCapture(hwnd);
        io.MouseDown[button] = true;
        return 0;
    }

    case WM_KILLFOCUS:
        for (auto& key : io.KeysDown)
            key = false;
        for (auto& key : io.MouseDown)
            key = false;
        break;

    case WM_LBUTTONUP: case WM_RBUTTONUP: case WM_MBUTTONUP: case WM_XBUTTONUP: {
        int button = 0;
        if (msg == WM_LBUTTONUP) { button = 0; }
        if (msg == WM_RBUTTONUP) { button = 1; }
        if (msg == WM_MBUTTONUP) { button = 2; }
        if (msg == WM_XBUTTONUP) { button = (GET_XBUTTON_WPARAM(wParam) == XBUTTON1) ? 3 : 4; }
        io.MouseDown[button] = false;
        if (!ImGui::IsAnyMouseDown() && ::GetCapture() == hwnd)
            ::ReleaseCapture();
        return 0;
    }

    case WM_MOUSEWHEEL:
        io.MouseWheel += (float)GET_WHEEL_DELTA_WPARAM(wParam) / (float)WHEEL_DELTA;
        return 0;
    case WM_MOUSEHWHEEL:
        io.MouseWheelH += (float)GET_WHEEL_DELTA_WPARAM(wParam) / (float)WHEEL_DELTA;
        return 0;

    case WM_KEYDOWN:
    case WM_SYSKEYDOWN:
        if (wParam == VK_CONTROL)
            io.KeyCtrl = true;
        if (wParam == VK_SHIFT)
            io.KeyShift = true;

        if (wParam < 256) {
            io.KeysDown[wParam] = true;
            g_keyDownInLastFrame[wParam] = true;
        }
        return 0;

    case WM_KEYUP:
    case WM_SYSKEYUP:
        if (wParam == VK_CONTROL)
            io.KeyCtrl = false;
        if (wParam == VK_SHIFT)
            io.KeyShift = false;
        if (wParam < 256)
            io.KeysDown[wParam] = false;
        return 0;

    case WM_CHAR:
        // You can also use ToAscii()+GetKeyboardState() to retrieve characters.
        io.AddInputCharacter((unsigned int)wParam);
        return 0;
    }
    return 0;
}

struct WindowStuff {
    HWND hwnd;
    ComPtr<ID3D11Device> device;
    ComPtr<ID3D11DeviceContext> context;
    ComPtr<IDXGISwapChain> swapChain;
    ComPtr<ID3D11RenderTargetView> renderTargetView;
    ComPtr<ID3D11DepthStencilView> depthStencilView = nullptr;

    bool beingResized = false;

    Pair<int, int> clientRect() {
        RECT rect;
        GetClientRect(hwnd, &rect);
        return { rect.right - rect.left, rect.bottom - rect.top };
    }

    void createDepthStencil() {
        depthStencilView = nullptr;

        // Set up the depth-stencil state:
        ComPtr<ID3D11Texture2D> pDepthStencil;
        D3D11_TEXTURE2D_DESC descDepth;
        descDepth.Width = width;
        descDepth.Height = height;
        descDepth.MipLevels = 1;
        descDepth.ArraySize = 1;
        descDepth.Format = DXGI_FORMAT_D32_FLOAT;
        descDepth.SampleDesc.Count = 1;
        descDepth.SampleDesc.Quality = 0;
        descDepth.Usage = D3D11_USAGE_DEFAULT;
        descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;
        descDepth.CPUAccessFlags = 0;
        descDepth.MiscFlags = 0;
        if (FAILED(device->CreateTexture2D(&descDepth, NULL, &pDepthStencil))) unreachable("couldn't create depth stencil");

        if (FAILED(device->CreateDepthStencilView(pDepthStencil.Get(), nullptr, &depthStencilView))) unreachable("couldn't create depth stencil view");

        bindRtv();
    }

    void bindRtv() {
        context->OMSetRenderTargets(1, renderTargetView.GetAddressOf(), depthStencilView.Get());
    }

    void handleResize() {
        if (!IsIconic(hwnd)) {
            context->OMSetRenderTargets(0, nullptr, nullptr);
            renderTargetView = nullptr;

            std::tie(width, height) = clientRect();
            ImGui::GetIO().DisplaySize = ImVec2(float(width), float(height));

            if (FAILED(swapChain->ResizeBuffers(0, 0, 0, DXGI_FORMAT_UNKNOWN, 0))) unreachable("could not resize window");

            ComPtr<ID3D11Texture2D> buffer;
            if (FAILED(swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&buffer)))
                unreachable("could not get swapchain buffer");

            if (FAILED(device->CreateRenderTargetView(buffer.Get(), NULL, &renderTargetView)))
                unreachable("could not create rtv");

            D3D11_VIEWPORT viewport = { 0, 0, float(width), float(height), 0.f, 1.f };
            context->RSSetViewports(1, &viewport);

            createDepthStencil();
        }
    }
};

LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) {
    ImGui_ImplWin32_WndProcHandler(hwnd, message, wParam, lParam);
    auto ws = (WindowStuff*)GetWindowLongPtr(hwnd, GWLP_USERDATA);

    switch (message) {
    case WM_DESTROY:
        PostQuitMessage(0);
        break;

    case WM_ENTERSIZEMOVE:
        ws->beingResized = true;
        break;

    case WM_EXITSIZEMOVE:
        ws->beingResized = false;
        ws->handleResize();
        break;

    case WM_SIZE:
        if (ws && !ws->beingResized)
            ws->handleResize();

        break;
    }

    return DefWindowProc(hwnd, message, wParam, lParam);
}

WindowStuff createWindow(HINSTANCE hInstance) {
    const auto style = WS_OVERLAPPEDWINDOW;

    WNDCLASSEXW wcex = {};
    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc = WndProc;
    wcex.hInstance = hInstance;
    wcex.hbrBackground = (HBRUSH) GetStockObject(BLACK_BRUSH);
    wcex.lpszClassName = className;
    wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);

    RegisterClassExW(&wcex);

    RECT r = { 0, 0, width, height };
    AdjustWindowRect(&r, style, false);
    int windowWidth = r.right - r.left;
    int windowHeight = r.bottom - r.top;

    auto hwnd = CreateWindowW(className, className, style, CW_USEDEFAULT, CW_USEDEFAULT,
        windowWidth, windowHeight, nullptr, nullptr, hInstance, nullptr);

    ShowWindow(hwnd, SW_SHOW);
    UpdateWindow(hwnd);

    ComPtr<ID3D11Device> device;
    ComPtr<ID3D11DeviceContext> context;
    ComPtr<IDXGISwapChain> swapChain;
    D3D_FEATURE_LEVEL featureLevel;

    UINT flags = 0;
#ifndef NDEBUG
    //flags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

    DXGI_SWAP_CHAIN_DESC swapChainDesc = {};
    swapChainDesc.BufferCount = 2;
    swapChainDesc.BufferDesc.Width = width;
    swapChainDesc.BufferDesc.Height = height;
    swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    swapChainDesc.OutputWindow = hwnd;
    swapChainDesc.SampleDesc.Count = 1;
    swapChainDesc.SampleDesc.Quality = 0;
    swapChainDesc.Windowed = TRUE;
    swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;

    if (FAILED(D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, flags,
        nullptr, 0, D3D11_SDK_VERSION, &swapChainDesc, &swapChain, &device, &featureLevel, &context)))
        unreachable("could not create swap chain");

    ComPtr<ID3D11Texture2D> pBackBuffer;
    if (FAILED(swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer)))
        unreachable("could not create back buffer");

    ComPtr<ID3D11RenderTargetView> pRenderTargetView;
    if (FAILED(device->CreateRenderTargetView(pBackBuffer.Get(), NULL, &pRenderTargetView)))
        unreachable("could not create RTV");

    WindowStuff ret = { hwnd, device, context, swapChain, pRenderTargetView };
    return ret;
}

static void ImGui_ImplWin32_UpdateMousePos(const WindowStuff& ws) {
    ImGuiIO& io = ImGui::GetIO();

    if (io.WantSetMousePos) unreachable("implemented me");

    // Set mouse position
    io.MousePos = ImVec2(-FLT_MAX, -FLT_MAX);
    POINT pos;
    if (HWND active_window = ::GetForegroundWindow())
        if (active_window == ws.hwnd || ::IsChild(active_window, ws.hwnd))
            if (::GetCursorPos(&pos) && ::ScreenToClient(ws.hwnd, &pos))
                io.MousePos = ImVec2(float(pos.x), float(pos.y));
}

void issueImguiCalls() {
    ImGui::Render();
    ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());
}

Maybe<TextureInfo> textureWindow() {
    Maybe<TextureInfo> ret;
    ImGuiIO& io = ImGui::GetIO();

    constexpr float textureScale = 2;

    ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, { 4, 4 });
    static ImGuiTextFilter filter{};
    if (ImGui::Begin("Materials", nullptr, ImGuiWindowFlags_AlwaysVerticalScrollbar)) {
        float windowWidth = ImGui::GetWindowContentRegionWidth();

        float x = 20.f;
        for (auto [name, tex] : resourceManager->_textures) {
            float width = tex->width * textureScale, height = tex->height * textureScale;

            if (width < 128 || height < 128) {
                const float lesser = min(width, height);
                const float rescale = 128 / lesser;
                width *= rescale;
                height *= rescale;
            }
            if (width > 256 || height > 256) {
                const float greater = max(width, height);
                const float rescale = 256 / greater;
                width *= rescale;
                height *= rescale;
            }

            if (x + width + 50 < windowWidth) {
                ImGui::SameLine();
            } else {
                ImGui::SetCursorPosX(20);
                x = 20.f;
            }

            auto c1 = ImGui::GetCursorPosX();
            if (ImGui::BeginChild(name.c_str(), ImVec2(width + 50, height + 50))) {
                if (ImGui::ImageButton(tex->srv.Get(), ImVec2(width, height))) {
                    ret = tex;
                }
                ImGui::Text(name.c_str());
            }
            ImGui::EndChild();
            auto c2 = ImGui::GetCursorPosX();
            x += width + 50;
        }
    }
    ImGui::End();
    ImGui::PopStyleVar();

    return ret;
}

// This is a separate function so all its stuff gets destructed before we do the ReportLiveObjects dance
void doMain() {
    auto windowStuff = createWindow(nullptr);
    windowStuff.createDepthStencil();
    SetWindowLongPtr(windowStuff.hwnd, GWLP_USERDATA, (LONG_PTR)&windowStuff);
    auto dev = windowStuff.device;
    auto ctx = windowStuff.context;

    startJobs();

    resourceManager = mkref<ResourceManager>(dev, ctx);

    auto ictx = ImGui::CreateContext();

    ImGuiIO& io = ImGui::GetIO();
    io.KeyMap[ImGuiKey_Delete] = VK_DELETE;
    io.KeyMap[ImGuiKey_Backspace] = VK_BACK;
    io.DisplaySize = ImVec2(float(width), float(height));
    io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable | ImGuiConfigFlags_DockingEnable;

    // Handle setting up the DPI stuff we need to:
    ImGui::GetStyle().ScaleAllSizes(dpiScale);
    io.FontDefault = io.Fonts->AddFontFromFileTTF("Roboto-regular.ttf", 13.f * dpiScale);

    // Imgui init stuff:
    ImGui_ImplDX11_Init(dev.Get(), ctx.Get());
    ImGui_ImplDX11_CreateDeviceObjects();

    // Defaults to stencil off:
    D3D11_DEPTH_STENCIL_DESC dsDesc = {};
    dsDesc.DepthEnable = true;
    dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
    dsDesc.DepthFunc = D3D11_COMPARISON_LESS;

    ComPtr<ID3D11DepthStencilState> dsState;
    dev->CreateDepthStencilState(&dsDesc, &dsState);
    ctx->OMSetDepthStencilState(dsState.Get(), 0);

    // Set up sane defaults for the rasterizer state:
    D3D11_RASTERIZER_DESC rasterizerDesc = {};
    rasterizerDesc.CullMode = D3D11_CULL_BACK;
    rasterizerDesc.FillMode = D3D11_FILL_SOLID;
    rasterizerDesc.FrontCounterClockwise = false;
    rasterizerDesc.DepthBias = false;
    rasterizerDesc.DepthBiasClamp = 0;
    rasterizerDesc.SlopeScaledDepthBias = 0;
    rasterizerDesc.MultisampleEnable = true;

    ComPtr<ID3D11RasterizerState> rasterizerState;
    dev->CreateRasterizerState(&rasterizerDesc, &rasterizerState);
    ctx->RSSetState(rasterizerState.Get());

    // To-do handle the times right..
    io.DeltaTime = 0.01f;

    Timer frameTimer;

    EditorWorld world;

    //world.loadMapFile("res/xmasjam2019/_src/xj19_qalten.map");
    //world.loadMapFile("res/quake maps/e3m1.map");
    //world.loadMapFile("res/quake maps/start.map");
    //world.loadMapFile("res/quake maps/e3m3.map");
    ///world.loadMapFile("res/quake maps/E2M7.map");
    //world.loadLevel("res/quake maps/end.map");

    //world.loadMapFile("res/quake maps/dm2.map");

    //world.loadMapFile("res/pun5.map");
    //world.loadMapFile("res/sm211_naitelveni.map");

    //world.loadMapFile("res/halloween jam/src/hwjam3_makkon.map");
    world.loadMapFile("res/xj2020/_src/xj2020_naitelveni.map");
    //world.loadMapFile("res/arcanedimensions/ad_sepulcher.map");
    //world.loadLevel("res/xj2020/_src/xj2020_qmaster2.map");

    //world.loadVmfLevel("res/de_nuke_d.vmf");

    //world.loadMapFile("res/honey/maps_src/saint.map");

    PerspectiveViewport perspective(&world, dev, ctx);

    bool done = false;
    while (!done) {
        memset(g_keyDownInLastFrame, 0, sizeof(g_keyDownInLastFrame));

        MSG msg;
        while (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE)) {
            TranslateMessage(&msg);
            DispatchMessage(&msg);

            if (msg.message == WM_QUIT)
                done = true;
        }
        HRESULT hr = GetLastError();
        if (FAILED(hr)) debugBreak();

        ImGui_ImplWin32_UpdateMousePos(windowStuff);
        ImGui::NewFrame();

        // Ugly main window prep code:
        ImGuiWindowFlags window_flags = ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoDocking;
        ImGuiViewport* viewport = ImGui::GetMainViewport();
        ImGui::SetNextWindowPos(viewport->Pos);
        ImGui::SetNextWindowSize(viewport->Size);
        ImGui::SetNextWindowViewport(viewport->ID);
        ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
        ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
        window_flags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove;
        window_flags |= ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;

        ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
        ImGui::Begin("DockSpace Demo", 0, window_flags);
        ImGui::PopStyleVar(3);

        ImGuiID dockspace_id = ImGui::GetID("MyDockSpace");
        ImGui::DockSpace(dockspace_id, ImVec2(0.0f, 0.0f), 0);

        if (ImGui::BeginMenuBar()) {
            if (ImGui::BeginMenu("File")) {
                if (ImGui::MenuItem("Exit")) done = true;
                ImGui::EndMenu();
            }
        }
        ImGui::EndMenuBar();

        if (auto tex = textureWindow()) {
            Vec<FaceTextureChange> changes;
            for (auto b : world.selectedBrushes) {
                for (size_t i = 0; i < b->textureInfo.size(); ++i)
                    changes.push_back({ b, i, b->textureInfo[i].tex, *tex });
            }

            for (auto& [brush, face] : world.selectedFaces)
                changes.push_back({ brush, face, brush->textureInfo[face].tex, *tex });

            auto cmd = mkref<ChangeTextureCommand>(changes);
            world.undoManager.addCommand(cmd);

            cmd->redo(world);

            auto dirtyBrushes = cmd->brushes();
            world.dirtyBrushes.insert(begin(dirtyBrushes), end(dirtyBrushes));
            generateCsgGeometry(world.brushes, world.bvhTree, world.dirtyBrushes);
        }
        perspective.update();
        world.dirtyBrushes.clear();

        ImGui::End(); // window end

        frameTimer.reset();

        if (io.KeyCtrl) {
            Set<Ref<Brush>> dirty;
            if (g_keyDownInLastFrame['Z'])
                dirty = world.undoManager.undo();
            else if (g_keyDownInLastFrame['R'])
                dirty = world.undoManager.redo();

            if (dirty.size() > 0) {
                world.dirtyBrushes.insert(begin(dirty), end(dirty));
                generateCsgGeometry(world.brushes, world.bvhTree, dirty);
            }
        }

        // Do all the main rendering:
        {
            windowStuff.bindRtv();
            D3D11_VIEWPORT viewport = { 0, 0, float(width), float(height), 0.f, 1.f };
            ctx->RSSetViewports(1, &viewport);

            float clearColor[] = { 0.2f, 0.2f, 0.2f, 1.f };
            ctx->ClearRenderTargetView(windowStuff.renderTargetView.Get(), clearColor);
            ctx->ClearDepthStencilView(windowStuff.depthStencilView.Get(), D3D11_CLEAR_DEPTH, 1.f, 0);
            issueImguiCalls();
        }

        windowStuff.swapChain->Present(1, 0);
    }

    shutdownJobs();

    ImGui_ImplDX11_Shutdown();
    ImGui::DestroyContext();

    resourceManager = nullptr;
    //return dev;
}

#include <mimalloc-new-delete.h>

int WINAPI wWinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE /*hPrevInstance*/, _In_ LPWSTR /*lpCmdLine*/, _In_ int /*nCmdShow*/) {
    // Being DPI aware makes it so the CreateWindow call is in actual screen pixels rather than sized to DPI:
    SetProcessDPIAware();

    // Force mimalloc to be here
    //mi_version();

    if (false) {
#ifdef _DEBUG
        AllocConsole();
        freopen("CONOUT$", "w", stdout);
        freopen("CONOUT$", "w", stderr);
#endif
    }

    doMain();

    //_exit(0);

    /*
    ComPtr<ID3D11Debug> dbg;
    dev.As(&dbg);
    if (dbg) dbg->ReportLiveDeviceObjects(D3D11_RLDO_DETAIL);
    */

    return 0;
}

#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "D3dcompiler.lib")
