#include "common.h"
#include "viewport.h"
#include "imgui_internal.h"

void EditorViewport::update() {
    ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, { 4, 4 });
    if (ImGui::Begin(name().c_str(), nullptr, ImGuiWindowFlags_NoDecoration)) {
        layout();

        auto avail = ImGui::GetContentRegionAvail();
        int width = std::max(int(avail.x), 50), height = std::max(int(avail.y), 50);

        if (viewportNeedsReset(width, height)) resetViewport(width, height);
        ImGui::ImageButton((ImTextureID)_shaderResourceView.Get(), avail, { 0, 0 }, { 1, 1 }, 0);

        // If we're clicked on for the first time we're now stealing input which means we become the active id:
        if (ImGui::IsItemHovered()) {
            if (ImGui::IsMouseClicked(1) && !ImGui::IsMouseDown(0))
                _stealingInput = true;
            if (ImGui::IsMouseClicked(0) && !ImGui::IsMouseDown(1))
                _stealingInput = true;

            auto cursorPos = ImGui::GetMousePos();
            auto widgetPos = ImGui::GetItemRectMin();

            int localX = int(cursorPos.x - widgetPos.x), localY = int(cursorPos.y - widgetPos.y);

            ImGuiIO& io = ImGui::GetIO();
            if (io.MouseWheel != 0)
                mouseWheel(io.MouseWheel > 0, localX, localY);

            for (int i = 0; i < 5; ++i)
                if (io.MouseClicked[i])
                    mouseDown(i, localX, localY);

            if (io.MouseReleased[0])
                mouseUp(0, localX, localY);
        }

        // We stop stealing when both buttons are up:
        if (!ImGui::IsMouseDown(0) && !ImGui::IsMouseDown(1))
            _stealingInput = false;

        if (_stealingInput) {
            ImGui::SetWindowFocus(name().c_str());
            ImGui::SetActiveID(ImGui::GetItemID(), ImGui::GetCurrentWindow());
        }

        // this is broken because we keep reset the drag which resets the cursor clicked pos irritatingly
        if (ImGui::IsItemActive()) {
            // We start dragging on this if we pressed the right mouse button this frame:
            if (ImGui::IsMouseDragging(0, 0)) {
                auto delta = ImGui::GetMouseDragDelta(0, 0);
                mouseDrag(int(delta.x), int(delta.y));
            } else if (ImGui::IsMouseDragging(1, 0)) {
                auto delta = ImGui::GetMouseDragDelta(1, 0);
                mouseDrag(int(delta.x), int(delta.y));
            }

            ImGui::ResetMouseDragDelta(0);
            ImGui::ResetMouseDragDelta(1);
        }

        D3D11_VIEWPORT viewport = { 0, 0, float(width), float(height), 0.f, 1.f };
        _context->RSSetViewports(1, &viewport);
        _context->OMSetRenderTargets(1, _renderTargetView.GetAddressOf(), _depthStencilView.Get());

        drawInternal();

        // Resolve from the multisampled render target:
        _context->ResolveSubresource(_resolvedTexture.Get(), 0, _texture.Get(), 0, DXGI_FORMAT_R8G8B8A8_UNORM);
    }
    ImGui::End();
    ImGui::PopStyleVar();
}

ImVec2 EditorViewport::size() const {
    D3D11_TEXTURE2D_DESC desc;
    _texture->GetDesc(&desc);
    return { float(desc.Width), float(desc.Height) };
}

void EditorViewport::resetViewport(size_t width, size_t height) {
    const unsigned sampleCount = 8;

    {
        D3D11_TEXTURE2D_DESC desc = {};
        desc.ArraySize = 1;
        desc.Width = UINT(width);
        desc.Height = UINT(height);
        desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
        desc.MipLevels = desc.ArraySize = 1;
        desc.SampleDesc.Count = sampleCount;
        desc.Usage = D3D11_USAGE_DEFAULT;
        desc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;

        if (FAILED(_device->CreateTexture2D(&desc, nullptr, &_texture)))
            unreachable("couldn't create rtv");

        desc.SampleDesc.Count = 1;
        if (FAILED(_device->CreateTexture2D(&desc, nullptr, &_resolvedTexture)))
            unreachable("couldn't create rtv");
    }

    if (FAILED(_device->CreateRenderTargetView(_texture.Get(), nullptr, &_renderTargetView)))
        unreachable("couldn't create RTV");

    if (FAILED(_device->CreateShaderResourceView(_resolvedTexture.Get(), nullptr, &_shaderResourceView)))
        unreachable("couldn't create offscreen srv");

    {
        // Set up the depth-stencil state:
        ComPtr<ID3D11Texture2D> pDepthStencil;
        D3D11_TEXTURE2D_DESC descDepth;
        descDepth.Width = UINT(width);
        descDepth.Height = UINT(height);
        descDepth.MipLevels = 1;
        descDepth.ArraySize = 1;
        descDepth.Format = DXGI_FORMAT_D32_FLOAT;
        descDepth.SampleDesc.Count = sampleCount;
        descDepth.SampleDesc.Quality = 0;
        descDepth.Usage = D3D11_USAGE_DEFAULT;
        descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;
        descDepth.CPUAccessFlags = 0;
        descDepth.MiscFlags = 0;
        if (FAILED(_device->CreateTexture2D(&descDepth, NULL, &pDepthStencil))) unreachable("couldn't create depth stencil");

        D3D11_DEPTH_STENCIL_VIEW_DESC dsdesc = {};
        dsdesc.Format = DXGI_FORMAT_D32_FLOAT;
        dsdesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;
        if (FAILED(_device->CreateDepthStencilView(pDepthStencil.Get(), &dsdesc, &_depthStencilView))) unreachable("couldn't create depth stencil view");
    }
}

bool EditorViewport::viewportNeedsReset(size_t width, size_t height) {
    if (!_renderTargetView || !_texture) return true;

    // Set up the viewport for drawing:
    D3D11_TEXTURE2D_DESC desc;
    _texture->GetDesc(&desc);

    return desc.Width != width || desc.Height != height;
}
