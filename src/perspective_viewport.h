#pragma once

#include "viewport.h"
#include "immediate_mode.h"
#include "editor_state.h"
#include "map_render.h"
#include "external/tinygizmo/tiny-gizmo.hpp"

struct TransformTool;

// An immediate-mode geometry manager for debug overlays and stuff like that:
struct PerspectiveViewport : EditorViewport {
    PerspectiveViewport(EditorWorld* world, ComPtrA<ID3D11Device> device, ComPtrA<ID3D11DeviceContext> context);

    template<typename Fn>
    void processBrushAction(const Ref<Brush>& brush, const Fn& fn);

protected:
    void drawInternal() override;

    Str name() {
        return "Perspective viewport";
    }

    void mouseDown(int button, int x, int y) override;
    void mouseUp(int button, int x, int y) override;
    void mouseDrag(int dx, int dy) override;
    void layout() override;

private:
    float3 directionOfScreenPos(float2 pos) const;

    void processDirtyBrushes(const Set<Ref<Brush>>& brushes);

    tinygizmo::gizmo_application_state makeGizmoState() const;

    bool _drawLines = true, _drawWorld = true;

    float _translationSnap = 8.f;

    EditorWorld* _world;
    MapRenderer _renderer;

    float4x4 _perspectiveProjection;
    float4x4 _rotation = identity;
    float3 _translation;

    Ref<TransformTool> _transformTool;

    float2 _buttonDownPosition;

    // Gizmo stuff:
    UINT _gizmoElements = 0;
    ComPtr<ID3D11InputLayout> _gizmoInputLayout;
    Ref<Shader> _gizmoShader;
    ComPtr<ID3D11Buffer> _vertexBuffer, _indexBuffer;

    Ref<CreateBrushTool> _currentTool;
    tinygizmo::gizmo_context _gizmo;
};

struct TransformTool {
    tinygizmo::rigid_transform transform;
    PerspectiveViewport& viewport;
    EditorWorld& _world;

    TransformTool(tinygizmo::rigid_transform transform, PerspectiveViewport& viewport, EditorWorld& world)
        : transform(transform), viewport(viewport), _world(world) {
    }

    virtual void cancel() = 0;
    virtual void update() = 0;
    virtual void commit() = 0;
};
