#pragma once

#include "math_common.h"
#include <queue>
#include <functional>

template<typename T>
struct BvhTree {
    struct InnerNode;
    struct LeafNode;

    struct Node {
        Aabb bounds;
        InnerNode* parent = nullptr;

        virtual ~Node() {}

        // Find the ancestor with all relevant nodes to this bound:
        virtual Node* findRelevanceAncestor(const Aabb& bounds) = 0;

        virtual void validate() = 0;

        virtual InnerNode* innerNode() { return nullptr; }
        virtual LeafNode* leafNode() { return nullptr; }

        virtual size_t maxDepth() const = 0;

    protected:
        Node(const Aabb& bounds) : bounds(bounds) {}
    };

    struct InnerNode : Node {
        Node* left;
        Node* right;

        InnerNode(Node* left, Node* right) : Node(left->bounds), left(left), right(right) {
            this->bounds.expand(right->bounds);
            left->parent = right->parent = this;
        }

        ~InnerNode() {
            delete left;
            delete right;
        }

        size_t maxDepth() const override {
            return std::max(left->maxDepth(), right->maxDepth()) + 1;
        }

        Node* findRelevanceAncestor(const Aabb& bounds) override {
            const bool intersect0 = left->bounds.intersects(bounds);
            const bool intersect1 = right->bounds.intersects(bounds);

            if (intersect0 && intersect1) return this;
            if (intersect0) return left->findRelevanceAncestor(bounds);
            if (intersect1) return right->findRelevanceAncestor(bounds);

            // not relevant to anything:
            return nullptr;
        }

        // New children mean we need to update our bounds:
        void updateBounds() {
            Aabb newBounds = left->bounds;
            newBounds.expand(right->bounds);

            if (!newBounds.equals(this->bounds)) {
                this->bounds = newBounds;
                if (this->parent) this->parent->updateBounds();
            }
        }

        void validate() override {
            assert(left->parent == this);
            assert(right->parent == this);

            left->validate();
            right->validate();

            Aabb merged(left->bounds);
            merged.expand(right->bounds);

            assert(merged.equals(this->bounds));
        }

        Node*& other(Node* child) {
            if (left == child) return right;
            assert(right == child);
            return left;
        }
        Node*& childRef(Node* child) {
            if (left == child) return left;
            assert(right == child);
            return right;
        }

        virtual InnerNode* innerNode() { return this; }
    };

    struct LeafNode : Node {
        T data;

        LeafNode(T data, const Aabb& bounds) : Node(bounds), data(data) {
        }

        size_t maxDepth() const override {
            return 1;
        }

        Node* findRelevanceAncestor(const Aabb& bounds) override {
            if (this->bounds.intersects(bounds)) return this;
            return nullptr;
        }

        void validate() override {}

        virtual LeafNode* leafNode() { return this; }
    };

    BvhTree() {}
    ~BvhTree() {
        delete root;
    }

    template<typename Fn>
    void raycast(const Line& line, float& nearest, Fn f) {
        if (!root || !root->bounds.intersection(line)) return;

        std::queue<Node*> nodes;
        nodes.push(root);

        while (!nodes.empty()) {
            auto node = nodes.front();
            nodes.pop();

            if (auto inner = node->innerNode()) {
                if (auto t = inner->left->bounds.intersection(line); t && *t < nearest)
                    nodes.push(inner->left);
                if (auto t = inner->right->bounds.intersection(line); t && *t < nearest)
                    nodes.push(inner->right);
            } else if (auto leaf = node->leafNode()) {
                if (auto t = leaf->bounds.intersection(line); t && *t < nearest)
                    f(leaf->data, nearest);
            }
        }
    }

    void insert(T elem, const Aabb& bounds) {
        auto newLeaf = addLeaf(elem, bounds);

        if (root) {
            auto sibling = findOptimalSibling(root, bounds);
            m_leaves[elem] = newLeaf;

            auto originalParent = sibling->parent ? sibling->parent->innerNode() : nullptr;
            Node** originalSiblingAddr = nullptr;
            if (originalParent) originalSiblingAddr = &originalParent->childRef(sibling);

            auto newParent = new InnerNode(sibling, newLeaf);
            newParent->parent = originalParent;

            // If we had a parent, we need to re-write the child to our new inner node, otherwise, the new parent
            // is the root:
            if (originalSiblingAddr) {
                *originalSiblingAddr = newParent;
                originalParent->updateBounds();

                // We can try doing rotations here:
                rotate(newParent);
            } else {
                root = newParent;
            }
        } else {
            root = newLeaf;
        }
    }

    void remove(T elem) {
        auto it = m_leaves.find(elem);
        assert(it != m_leaves.end());
        auto node = it->second;
        m_leaves.erase(it);

        if (auto formerParent = node->parent) {
            // Our sibling that will remain after deleting the leaf node `elem` and `formerParent`
            auto remaining = formerParent->other(node);
            auto newParent = formerParent->parent;
            remaining->parent = newParent;

            if (newParent) {
                newParent->childRef(node->parent) = remaining;
                newParent->updateBounds();
                rotate(newParent);
            } else {
                root = remaining;
            }

            // Depend on the destructor to deal with the other node deletion, but don't delete the remaining one:
            formerParent->childRef(remaining) = nullptr;
            delete formerParent;
        } else {
            delete node;
            root = nullptr;
        }
    }

    void update(T elem, const Aabb& bounds) {
        remove(elem);
        insert(elem, bounds);
    }

    Node* root = nullptr;

private:
    LeafNode* addLeaf(T elem, const Aabb& bounds) {
        auto ret = new LeafNode(elem, bounds);
        auto& bucket = m_leaves[elem];
        assert(bucket == nullptr);
        return bucket = ret;
    }

    // Surface area we lose by rotating the lower and upper nodes:
    static float savingsFromRotation(Node* upper, Node* lower) {
        // The only savings from this rotation is to the parent of the lower item.
        const auto lp = lower->parent;
        assert(lp);

        const auto replacedSurfaceArea = Aabb(lp->other(lower)->bounds, upper->bounds).surfaceArea();
        return lp->bounds.surfaceArea() - replacedSurfaceArea;
    }

    // Try to lower the surface area recursively upwards by rotating nodes under us:
    static void rotate(InnerNode* parent) {
        float bestSavings = 0.f;
        Node* bestUpper = nullptr;
        Node* bestLower = nullptr;

        auto evalConfiguration = [&](Node* testUpper, Node* testLower) {
            const float savings = savingsFromRotation(testUpper, testLower);
            if (savings > bestSavings) {
                bestSavings = savings;
                bestUpper = testUpper;
                bestLower = testLower;
            }
        };

        if (auto left = parent->left->innerNode()) {
            evalConfiguration(parent->right, left->left);
            evalConfiguration(parent->right, left->right);
        }
        if (auto right = parent->right->innerNode()) {
            evalConfiguration(parent->left, right->left);
            evalConfiguration(parent->left, right->right);
        }

        if (bestUpper) {
            auto pivot = bestLower->parent;
            pivot->childRef(bestLower) = bestUpper;
            bestUpper->parent = pivot;
            parent->childRef(bestUpper) = bestLower;
            bestLower->parent = parent;

            pivot->updateBounds();
        }

        if (parent->parent) rotate(parent->parent);
    }

   std::unordered_map<T, LeafNode*> m_leaves;

    // Calculate the new additional surface area when adding the bounds to the node in question:
    static float surfaceAreaDelta(Node* node, const Aabb& bounds) {
        if (!node) return 0.f;

        Aabb newBounds(node->bounds, bounds);
        const float proximalIncrease = newBounds.surfaceArea() - node->bounds.surfaceArea();

        float parentIncrease = 0.f;
        if (node->parent)
            parentIncrease = surfaceAreaDelta(node->parent, bounds);

        return proximalIncrease + parentIncrease;
    }

    // How much it would cost to create a new node anchored here with node and bounds as children:
    static float costToBeNeighbour(Node* node, const Aabb& bounds) {
        const float ourCost = Aabb(node->bounds, bounds).surfaceArea();
        return ourCost + surfaceAreaDelta(node->parent, bounds);
    }

    // Find the optimal node to become our sibling:
    static Node* findOptimalSibling(Node* base, const Aabb& bounds) {
        struct QueueBucket {
            Node* node;

            // The increase in surface are for making `node` our neighbour
            float cost;
            // The difference in surface area for all parent nodes including the new node, up to the root.
            float parentCostIncrease;

            bool operator < (const QueueBucket& rhs) const {
                return cost < rhs.cost;
            }
        };

        std::priority_queue<QueueBucket> queue;
        queue.push({ base, costToBeNeighbour(base, bounds), 0 });

        const float boundsSurfaceArea = bounds.surfaceArea();

        float bestCost = -1.f;
        Node* bestNode = nullptr;

        while (!queue.empty()) {
            auto bucket = queue.top();
            queue.pop();

            if (bestCost == -1.f || bucket.cost < bestCost) {
                bestCost = bucket.cost;
                bestNode = bucket.node;
            }

            if (auto in = bucket.node->innerNode()) {
                const auto mergedBounds = Aabb(bucket.node->bounds, bounds);

                const float ourCostIncrease = mergedBounds.surfaceArea() - bucket.node->bounds.surfaceArea();
                const float minChildCost = boundsSurfaceArea + bucket.parentCostIncrease + ourCostIncrease;

                if (minChildCost < bestCost) {
                    const float newParentCost = ourCostIncrease + bucket.parentCostIncrease;

                    const float leftCost = newParentCost + Aabb(in->left->bounds, bounds).surfaceArea();
                    const float rightCost = newParentCost + Aabb(in->right->bounds, bounds).surfaceArea();

                    queue.push({ in->left, leftCost, newParentCost, });
                    queue.push({ in->right, rightCost, newParentCost, });
                }
            }
        }

        return bestNode;
    }
};

template<typename T, typename Fn>
void visitBvhNode(typename BvhTree<T>::Node* node, const Aabb& request, Fn fn) {
    if (!node || !node->bounds.intersects(request)) return;

    if (auto inner = node->innerNode()) {
        visitBvhNode<T>(inner->left, request, fn);
        visitBvhNode<T>(inner->right, request, fn);
    } else {
        fn(node->leafNode()->data);
    }
}
