#include "common.h"
#include "spool.h"

struct Barrier {
    SYNCHRONIZATION_BARRIER barrier;

    void init(size_t amount) {
        InitializeSynchronizationBarrier(&barrier, LONG(amount), -1);
    }

    Barrier() {}
    Barrier(size_t amount) {
        InitializeSynchronizationBarrier(&barrier, LONG(amount), -1);
    }

    void arrive() {
        EnterSynchronizationBarrier(&barrier, 0);
    }
};

struct Task {
    void (*f)(void*);
    void* data;
    TaskBatch* batch;

    // For continuations, data is the fiber and f is null.
    bool continuation;
};

struct GlobalTaskData {
    size_t threadCount = 8;
    Maybe<Barrier> shutdownBarrier;

    std::vector<ThreadLocalTaskData*> threadLocalData;
    std::vector<std::thread> threads;

    ThreadsafePool<Fiber> fiberPool{ 8 };

    //
    // This is the primary communication point between the outside of the jobs system and the inside.
    // Jobs are added from the outside into here because they can't touch the job queues of the inside of the system.
    // This is also used as an overflow point for when we run out of space on the fixed buffers per-thread.
    //

    ThreadsafePool<Task> workPool{ 5 };

    Event wakeup;
    std::atomic<int> sleepingThreads;
};
GlobalTaskData g_globalTaskData;

struct ThreadLocalTaskData {
    ThreadLocalTaskData() : low(3), med(3), high(8) {}

    Deque<Task> low, med, high;
    size_t id = -1;

    Fiber threadFiber = nullptr;

    std::function<void()> handoverComplete;
};

thread_local ThreadLocalTaskData* g_currentThreadLocalData;

void expect(bool c, char* msg=nullptr) {
    if (!c) debugBreak();
}

template<typename Fn>
void switchToFiber(Fiber fiber, const Fn& f) {
    auto tls = accessTlsTask();
    assert(!tls->handoverComplete);

    tls->handoverComplete = f;
    if (GetCurrentFiber() != fiber) SwitchToFiber(fiber);

    tls = accessTlsTask();
    auto completion = tls->handoverComplete;
    tls->handoverComplete = nullptr;

    if (completion) completion();
}

constexpr size_t NUM_STEAL_ATTEMPTS = 10000;

static bool schedule() {
    auto state = accessTlsTask();

    if (state->handoverComplete) {
        state->handoverComplete();
        state->handoverComplete = nullptr;
    }

    // First try to find something local:
    Maybe<Task> task = state->high.pop();
    if (!task) task = state->med.pop();
    if (!task) task = state->low.pop();

    while (!task) {
        for (size_t i = 0; i < NUM_STEAL_ATTEMPTS && !task; ++i) {
            const size_t randomIdx = rand() % g_globalTaskData.threadCount;

            if (randomIdx == accessTlsTask()->id) {
                // Try to grab from the global queue:
                task = g_globalTaskData.workPool.pop();
            } else {
                while (true) {
                    bool raced;

                    task = g_globalTaskData.threadLocalData[randomIdx]->low.pilfer(&raced);
                    if (raced) continue;
                    if (task) break;
                    task = g_globalTaskData.threadLocalData[randomIdx]->med.pilfer(&raced);
                    if (raced) continue;
                    if (task) break;
                    task = g_globalTaskData.threadLocalData[randomIdx]->high.pilfer(&raced);
                    if (raced) continue;
                    if (task) break;

                    break;
                }
            }
        }

        if (!task) {
            ++g_globalTaskData.sleepingThreads;
            g_globalTaskData.wakeup.wait();
            --g_globalTaskData.sleepingThreads;
        } else {
            break;
        }
    }

    void* nextFiber = nullptr;

    if (task->continuation) {
        nextFiber = task->data;
    } else {
        if (task->f) task->f(task->data);

        if (task->batch) {
            std::shared_lock lock(task->batch->mutex);
            auto curr = --task->batch->counter;
            expect(curr >= 0);
            if (curr == 0) {
                // We can beat the main thread here:
                if (task->batch->suspendedFiber)
                    nextFiber = task->batch->suspendedFiber;

                if (task->batch->externalWaiting)
                    task->batch->externalWaiting->notify_all();
            }
        }

        // Sentinel to indicate we're done:
        if (!task->f)
            return true;
    }

    if (nextFiber) {
        auto currentFiber = GetCurrentFiber();
        switchToFiber(nextFiber, [&]() {
            g_globalTaskData.fiberPool.push(currentFiber);
        });
    }

    return false;
}

Barrier dummyFiberSwitched;

static void fiberFunc(void*) {
    while (!g_globalTaskData.shutdownBarrier) {
        schedule();
    }

#if 0
    auto r = g_globalTaskData.fiberPool.pop();
    switchToFiber(*r, [=]() {
        first.arrive();
        switchToFiber(accessTlsTask()->threadFiber, []() {
            ExitThread(0);
        });
    });
#endif

    //
    // The converted fiber from the spawning thread is special, and if we try to ExitThread
    // from that fiber in a different thread while the other thread is doing things we can
    // smash data when shutting down. So create a tiny fiber to do the shut down where we
    // just wait until we're all there first, then shut down on the dummy fiber.
    // Possibly a better approach is to treat the thread fibers specially, cache them and
    // never use them. We can make themlike 1kb and use them just to shut down later on.
    // That would mean not using std::thread, which is a bit annoying.
    //

    auto fn = [](void*) {
        dummyFiberSwitched.arrive();
        ExitThread(0);
    };
    auto dummyFiber = CreateFiber(1024, fn, nullptr);
    expect(dummyFiber);
    SwitchToFiber(dummyFiber);

    unreachable("This fiber should never be scheduled again");
}

void submitUntyped(void (*f)(void*), void* data, TaskPriority priority, TaskBatch* batch) {
    Task task = { f, data, batch, false };

    if (batch) ++batch->counter;

    // We're coming from outside the worker system:
    if (!accessTlsTask()) {
        g_globalTaskData.workPool.push(task);
    } else {
        while (true) {
            auto tls = accessTlsTask();
            expect(tls->handoverComplete == nullptr);

            bool succeeded = false;
            if (priority == TaskPriority::LOW) {
                succeeded = tls->low.push(task);
            } else if (priority == TaskPriority::MEDIUM) {
                succeeded = tls->med.push(task);
            } else if (priority == TaskPriority::HIGH) {
                succeeded = tls->high.push(task);
            }

            if (succeeded) break;

            // Nobody else can add things to our queue, so having space means we'll have space on
            // the other side:
            Task ourselves = { nullptr, GetCurrentFiber(), nullptr, true };
            if (tls->low.size() < tls->low.capacity()) {
                auto newFiber = g_globalTaskData.fiberPool.pop();
                expect(newFiber != none);
                switchToFiber(*newFiber, [=]() {
                    bool success = tls->low.push(ourselves);
                    expect(success);
                });
            } else {
                // We'd need to add to the global work pool here if we failed, and make sure the global
                // pool can resize on-demand.
                expect(false);
            }
        }
    }

    if (g_globalTaskData.sleepingThreads > 0) g_globalTaskData.wakeup.signal();
}

void wait(TaskBatch* batch) {
    // This pertains to callers from outside the jobs system:
    if (!accessTlsTask()) {
        std::unique_lock lock(batch->mutex);
        if (!batch->externalWaiting)
            batch->externalWaiting.emplace();

        batch->externalWaiting->wait(lock);
        return;
    }

    batch->mutex.lock();
    if (batch->counter == 0) {
        // We've already completed everything, so we're already done waiting.
        batch->mutex.unlock();
    } else {
        // Only release the RW lock (and let something resume us), once we've switched over to the next fiber:
        batch->suspendedFiber = GetCurrentFiber();

        auto newFiber = g_globalTaskData.fiberPool.pop();
        expect(newFiber != none);
        switchToFiber(*newFiber, [=]() {
            batch->mutex.unlock();
        });
    }
}

static std::atomic<int> threadIdCounter;
Barrier startupBarrier;

static void threadFunc(void*) {
    auto id = threadIdCounter++;

    SetThreadDescription(GetCurrentThread(), L"Ersatz worker");

    {
        g_currentThreadLocalData = new ThreadLocalTaskData;
        g_currentThreadLocalData->id = id;
        expect(g_globalTaskData.threadLocalData[id] == nullptr);
        g_globalTaskData.threadLocalData[id] = g_currentThreadLocalData;
    }

    startupBarrier.arrive();

    auto fiber = ConvertThreadToFiber(nullptr);
    g_currentThreadLocalData->threadFiber = fiber;
    fiberFunc(nullptr);
}

void startJobs() {
    g_globalTaskData.threadLocalData.resize(g_globalTaskData.threadCount);
    startupBarrier.init(g_globalTaskData.threadCount + 1);
    dummyFiberSwitched.init(g_globalTaskData.threadCount);

    // Create a bunch of fibers to use:
    for (size_t i = 0; i < 250; ++i)
        g_globalTaskData.fiberPool.push(CreateFiber(256 * 1024, fiberFunc, nullptr));

    for (size_t i = 0; i < g_globalTaskData.threadCount; ++i)
        g_globalTaskData.threads.push_back(std::thread(threadFunc, nullptr));

    startupBarrier.arrive();
}

void shutdownJobs() {
    g_globalTaskData.shutdownBarrier = Barrier(g_globalTaskData.threadCount);

    for (size_t i = 0; i < g_globalTaskData.threadCount; ++i)
        submit<void*>(nullptr, nullptr, TaskPriority::LOW, nullptr);

    for (auto& thread : g_globalTaskData.threads)
        thread.join();
}

#pragma optimize("", off)
ThreadLocalTaskData* accessTlsTask() {
    return g_currentThreadLocalData;
}
