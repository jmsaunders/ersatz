// A line rendering system

#include "common.h"
#include "d3d.h"

#include "immediate_mode.h"

using namespace linalg;
using namespace linalg::aliases;
extern int width, height;

using BufferElem = ImmediateManager::BufferElem;
static const D3D11_INPUT_ELEMENT_DESC g_lineVertexFormat[] = {
    { "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, offsetof(BufferElem, pos), D3D11_INPUT_PER_VERTEX_DATA, 0 },
};

ImmediateManager::ImmediateManager(ComPtr<ID3D11Device> device, ComPtr<ID3D11DeviceContext> dc) : m_device(device), m_dc(dc) {
    if (auto maybeShader = resourceManager->requestShader("lines.hlsl", {}, true)) {
        shader = maybeShader;
    } else {
        unreachable("no shader");
    }

    device->CreateInputLayout(g_lineVertexFormat, _countof(g_lineVertexFormat), shader->vsblob->GetBufferPointer(), shader->vsblob->GetBufferSize(), &m_inputLayout);

    D3D11_RASTERIZER_DESC rasterizerDesc = {};
    rasterizerDesc.CullMode = D3D11_CULL_NONE;
    rasterizerDesc.FillMode = D3D11_FILL_SOLID;
    rasterizerDesc.DepthBias = true;
    rasterizerDesc.DepthBias = -100;
    rasterizerDesc.MultisampleEnable = true;

    device->CreateRasterizerState(&rasterizerDesc, &lineRasterizerState);

    {
        D3D11_BUFFER_DESC desc = {};
        desc.ByteWidth = 16; // fucking direct3d
        desc.Usage = D3D11_USAGE_DYNAMIC;
        desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
        desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

        auto hr = m_device->CreateBuffer(&desc, nullptr, &_c2Buffer);
        if (FAILED(hr)) unreachable("failed to create the shader constant buffer");
    }
}

void ImmediateManager::addLine(float3 start, float3 end, float width, float4 c) {
#if 0
    m_buffer.push_back({ float4(start, width), c, float4(end, 1.f) });
    m_buffer.push_back({ float4(start, width), c, float4(end, 1.f) });
    m_buffer.push_back({ float4(end, width), c, float4(start, 1.f) });

    m_buffer.push_back({ float4(end, width), c, float4(start, 1.f) });
    m_buffer.push_back({ float4(end, width), c, float4(start, 1.f) });
    m_buffer.push_back({ float4(start, width), c, float4(end, 1.f) });
#endif

    m_buffer.push_back({ float4(start, 1.f) });
    m_buffer.push_back({ float4(end, 1.f) });

    // Dirty, reset:
    _vertexBuffer = nullptr;
}

void ImmediateManager::prepareRender() {
    D3D11_BUFFER_DESC desc = {};
    desc.Usage = D3D11_USAGE_DYNAMIC;
    desc.ByteWidth = UINT(m_buffer.size() * sizeof(m_buffer[0]));
    desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

    D3D11_SUBRESOURCE_DATA data = { (void*)&m_buffer[0] };

    if (FAILED(m_device->CreateBuffer(&desc, &data, &_vertexBuffer))) unreachable("couldn't create VB?");
}

void ImmediateManager::render(const float4x4& modelView, const float4x4& projection, int width, int height) {
    if (m_buffer.empty()) return;
    if (!_vertexBuffer) prepareRender();

    float4x4 mvp = mul(modelView, projection);

    ComPtr<ID3D11Buffer> mainConstantBuffer;
    shader->bind(m_dc);
    {
        struct ConstantBuffer {
            float4x4 mvp;
            float4 color;
            float4 misc;
        } constantBuffer = { mvp, float4(1,1,1,1), float4(float(width), float(height), 0.f, 0.f) };

        D3D11_SUBRESOURCE_DATA initData = { &constantBuffer };

        D3D11_BUFFER_DESC cbDesc = {};
        cbDesc.ByteWidth = sizeof(ConstantBuffer);
        cbDesc.Usage = D3D11_USAGE_DYNAMIC;
        cbDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
        cbDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

        if (FAILED(m_device->CreateBuffer(&cbDesc, &initData, &mainConstantBuffer))) unreachable("failed to create the shader constant buffer");
    }

    UINT strides[] = { sizeof(m_buffer[0]) }, offsets[] = { 0 };
    m_dc->IASetVertexBuffers(0, 1, _vertexBuffer.GetAddressOf(), strides, offsets);
    m_dc->IASetInputLayout(m_inputLayout.Get());

    m_dc->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_LINELIST);

    {
        D3D11_MAPPED_SUBRESOURCE res;
        if (FAILED(m_dc->Map(_c2Buffer.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &res))) unreachable("couldn't map constant buffer");
        *(float*)(res.pData) = 2.5f;
        m_dc->Unmap(_c2Buffer.Get(), 0);
    }

    ID3D11Buffer* constantBuffers[] = { mainConstantBuffer.Get(), _c2Buffer.Get() };
    m_dc->VSSetConstantBuffers(0, 2, constantBuffers);
    m_dc->GSSetConstantBuffers(0, 2, constantBuffers);
    m_dc->PSSetConstantBuffers(0, 2, constantBuffers);

    ComPtr<ID3D11RasterizerState> oldRasterState;
    m_dc->RSGetState(oldRasterState.GetAddressOf());
    m_dc->RSSetState(lineRasterizerState.Get());
    m_dc->Draw(UINT(m_buffer.size()), 0);
    m_dc->RSSetState(oldRasterState.Get());
}

void ImmediateManager::reset() {
    _vertexBuffer = nullptr;
    m_buffer.clear();
}
