#include "common.h"
#include "csg.h"
#include "editor_state.h"

#include <numeric>

constexpr float epsilon = 0.001f;

Ref<Brush> createBrush(const float3& min, const float3& max) {
    if (!equals(min.x, max.x) && !equals(min.y, max.y) && !equals(min.z, max.z)) {
        Vec<Plane> planes = {
            Plane(float3(1, 0, 0), +min.x), // West
            Plane(float3(-1, 0, 0),-max.x), // East
            Plane(float3(0, 1, 0), +min.y), // Floor
            Plane(float3(0, -1, 0),-max.y), // Ceiling
            Plane(float3(0, 0, 1), +min.z), // South
            Plane(float3(0, 0, -1),-max.z), // North
        };

        auto ret = mkref<Brush>();
        ret->type = BrushType::ADDITIVE;
        ret->planes = planes;

        float4x2 generateTextureAxes(float3 normal, float rot, float2 scale, float2 offs, float2 texdim);
        auto tex = resourceManager->defaultTexture();
        for (auto p : planes) {
            ret->textureInfo.push_back({ tex, generateTextureAxes(p.n, 0, float2(.25f, .25f), {}, float2(float(tex.width), float(tex.height))) });
        }

        return ret;
    }

    return nullptr;
}

void Brush::flip() {
    if (type == BrushType::ADDITIVE)
        type = BrushType::SUBTRACTIVE;
    else
        type = BrushType::ADDITIVE;

    for (auto& p : planes) {
        p.n = -p.n;
        p.d = -p.d;
    }
}

bool Brush::pointLiesOnBoundary(const float3& pt, float epsilon) const {
    bool hasBoundary = false;

    // For additive brushes, the normals face out so we have to be on the back side of all of them:
    auto outside = type == BrushType::SUBTRACTIVE ? Side::BACK : Side::FRONT;
    for (const auto& p : planes) {
        auto side = p.side(pt, epsilon);
        if (side == outside)
            return false;
        hasBoundary |= side == Side::ON;
    }

    return hasBoundary;
}

bool Brush::regenerateBase(bool assignOnFailure) {
    Vec<Face> faces;
    faces.resize(planes.size());

    for (size_t i = 0; i < planes.size(); ++i) {
        for (size_t j = i + 1; j < planes.size(); ++j) {
            auto maybeLine = planes[i].intersect(planes[j], 0.0000001f);
            if (!maybeLine) continue;
            const auto& line = *maybeLine;

            for (size_t k = j + 1; k < planes.size(); ++k) {
                if (auto v = planes[k].intersect(line, 0.00000001f)) {
                    if (pointLiesOnBoundary(*v, 0.01f)) {
                        const auto v4 = float4(*v, 0.f);
                        faces[i].verts.push_back(v4);
                        faces[j].verts.push_back(v4);
                        faces[k].verts.push_back(v4);
                    }
                }
            }
        }
    }

    for (auto [index, face] : enumerate(faces)) {
        auto& verts = face.verts;

        if (verts.empty()) {
            if (!assignOnFailure) return false;
        }

        const auto center = std::accumulate(begin(verts), end(verts), float4()) / verts.size();
        const auto startv = normalize(center - verts[0]);

        // We anchor the first vertex and then measure the angle between the vertex from the centre to this vertex from
        // the similar vector to all the other vertices. We use the 'w' coordinate for the angle:
        for (auto& v : verts) {
            float4 lv = normalize(center - v);
            v.w = fullAngle(startv, lv, planes[index].n);
        }

        std::sort(begin(verts)+1, end(verts), [](const float4& lhs, const float4& rhs) {
            return lhs.w < rhs.w;
        });

        constexpr float angleEpsilon = 0.001f;

        // Remove the redundant vertices that come in when multiple planes have the same intersection point.
        verts.erase(std::unique(begin(verts), end(verts), [&](const float4& lhs, const float4& rhs) {
            return abs(lhs.w - rhs.w) < angleEpsilon;
        }), end(verts));

        if (abs(verts.front().w - verts.back().w) < angleEpsilon) verts.pop_back();

        for (auto& v : verts) v.w = 1.f;

        if (verts.size() < 3) {
            if (!assignOnFailure) return false;
            OutputDebugStringA("Face became degenerate!\n");
            continue;
        }
    }

    baseMesh.faces = std::move(faces);
    bounds = baseMesh.computeBounds();

    return true;
}

bool brushInteractsWithFace(VecA<float4> face, RefA<Brush> brush) {
    const auto outside = brush->type == BrushType::ADDITIVE ? Side::FRONT : Side::BACK;
    const auto inside = brush->type == BrushType::ADDITIVE ? Side::BACK : Side::BACK;

    const float epsilon = 0.01f;

    bool layingOnPlane = false;

    // We don't detect the case where we're entirely inside here:

    bool entirelyInside = true;

    for (const auto& p : brush->planes) {
        bool allOutside = true;
        bool allOn = true;

        auto firstSide = p.side(face[0], epsilon);
        for (size_t i = 0; i < face.size(); ++i) {
            const auto first = face[i], second = face[(i + 1) % face.size()];
            const auto secondSide = p.side(second, epsilon);

            if (firstSide == inside) allOutside = false;
            else entirelyInside = false;
            if (firstSide != Side::ON) allOn = false;

            if (firstSide != secondSide && firstSide != Side::ON && secondSide != Side::ON) {
                Line edge = { first.xyz(), second.xyz() - first.xyz() };
                if (auto pt = p.intersectWithin(edge, epsilon)) {
                    if (brush->pointLiesOnBoundary(*pt, epsilon)) {
                        return true;
                    }
                }
            }

            firstSide = secondSide;
        }

        if (allOn)
            layingOnPlane = true;
        else if (allOutside)
            return false;
    }

    return !entirelyInside || layingOnPlane;
}

// Given a set of convex faces representing the face from the brush, figure out all the texture information and emit:
void emitFinalFaces(Ref<Brush>& brush, VecA<Vec<float4>> faces, size_t faceNo) {
    const auto& faceNormal = brush->planes[faceNo].n;
    const auto& texInfo = brush->textureInfo[faceNo];

    for (const auto& face : faces) {
        if (face.empty()) continue;

        CsgFace csgFace;
        csgFace.texture = texInfo.tex;
        csgFace.brush = brush;
        csgFace.planeNo = int(faceNo);

        csgFace.verts.reserve(faces.size());
        for (const auto& v : face) {
            float4 pv = float4(v.xyz(), 1.f);
            const float2 uv(fastdot(texInfo.uvspace[0], pv), fastdot(texInfo.uvspace[1], pv));
            csgFace.verts.push_back({ v, uv });
        }

        brush->finalFaces.push_back(csgFace);
    }
}

enum {
    EXTERIOR_VALID = 1 << 0,
    INTERIOR_VALID = 1 << 1,
};

float areaOfTriangle(float4 a, float4 b, float4 c) {
    return length(cross((b - a).xyz(), (c - a).xyz()));
}

bool isDegenerate(VecA<float4> face) {
    for (size_t i = 2; i < face.size(); ++i) {
        auto area = areaOfTriangle(face[0], face[i-1], face[i]);
        if (!isZero(area, 0.01f))
            return false;
    }
    return true;
}

// @TODO we need to handle the case where a split plane for a brush _does_ split us and the brush interacts with us, but where the split happens
// is actually completely outside of the other brush. So the split it induces in us is completely irrelevant. This happens a lot.

// Use this: http://paulbourke.net/geometry/pointlineplane/

// Split the face by the brush, and classify the 'interior' portion as either: kInternal, kFrontBack, kCoincident
unsigned splitAndClassifyFace(VecA<float4> face, RefA<Brush> brush, size_t planeIdx, Vec<float4>& exterior, Vec<float4>& interior) {
    exterior.clear();
    interior.clear();

    const auto exteriorSide = brush->type == BrushType::ADDITIVE ? Side::FRONT : Side::BACK;

    const Plane& p = brush->planes[planeIdx];
    bool spanning = false, newPointsCreated = false;

    const float epsilon = 0.005f;

    int exteriorVerts = 0, interiorVerts = 0;
    for (size_t i = 0; i < face.size(); ++i) {
        // This is a necessary but not sufficient condition to actually split here.
        // We only want to bridge when we cross a plane _and change brush sides_.

        const auto first = face[i], second = face[(i + 1) % face.size()];
        const auto firstDist = p.distanceToPoint(first), secondDist = p.distanceToPoint(second);
        const auto firstSide = p.side(first, epsilon), secondSide = p.side(second, epsilon);

        if (firstSide == exteriorSide) {
            exterior.push_back(first);
            ++exteriorVerts;
        } else if (firstSide == Side::ON) {
            exterior.push_back(first);
            interior.push_back(first);
            spanning = true;
        } else {
            interior.push_back(first);
            ++interiorVerts;
        }

        if (firstSide != secondSide && firstSide != Side::ON && secondSide != Side::ON) {
            // Creating this intersection in this way could be a huge problem:
            Line edge = { first.xyz(), second.xyz() - first.xyz() };
            if (auto pt = p.intersect(edge, epsilon)) {
                const float4 newp(*pt, 0);
                exterior.push_back(newp);
                interior.push_back(newp);
            } else {
                debugBreak();
            }
        }
    }

    const unsigned validFlags =
        ((exteriorVerts > 0 && !isDegenerate(exterior)) ? EXTERIOR_VALID : 0)
      | ((interiorVerts > 0 && !isDegenerate(interior)) ? INTERIOR_VALID : 0);
    return validFlags;
}

void removePartsInteriorToBrush(Vec<float4> current, Plane facePlane, RefA<Brush> brush, bool keepCoincident, Vec<float4>& interior, Vec<float4>& exterior, Vec<Vec<float4>>& out) {
    bool coplanar = false, frontback = false;
    size_t firstCoplanarPlane = -1;

    for (size_t i = 0; i < brush->planes.size(); ++i) {
        if (current.empty()) break;

        const auto faceFlag = splitAndClassifyFace(current, brush, i, exterior, interior);

        // We can sometimes get degenerate geometry after splitting (accumulated rounding error?)
        // Probably want to try keeping track of the vertices using the plane triple rather than the intersection points
        // for more stability..

        if ((faceFlag & INTERIOR_VALID) && (faceFlag & EXTERIOR_VALID)) {
            // Split, keep carving up the current interior, but we know the exterior portion is done:
            current = interior;
            assert(exterior.size());
            out.push_back(exterior);
        } else if (faceFlag & INTERIOR_VALID) {
            continue;
        } else if (faceFlag & EXTERIOR_VALID) {
            // Entirely external, add us:
            assert(current.size());
            out.push_back(current);
            return;
        } else {
            // It makes no sense to be completely on multiple planes of the same brush. This is indicative
            // of some absolutely greasy rounding problems.
            //assert(!coplanar);
            firstCoplanarPlane = i;

            const auto d = dot(facePlane.n, brush->planes[i].n);
            if (d < 0) frontback = true;

            coplanar = true;
        }
    }

    if (coplanar && !frontback && keepCoincident)
        out.push_back(current);
}

void partitionFaceByBrush(Vec<float4> current, RefA<Brush> brush, Vec<Vec<float4>>& interior, Vec<Vec<float4>>& exterior) {
    Vec<float4> exteriorPts, interiorPts;

    for (size_t i = 0; i < brush->planes.size(); ++i) {
        if (current.empty()) break;

        const auto faceFlag = splitAndClassifyFace(current, brush, i, exteriorPts, interiorPts);

        if (faceFlag & INTERIOR_VALID)
            current = interiorPts;
        else
            current.clear();

        if (faceFlag & EXTERIOR_VALID)
            exterior.push_back(exteriorPts);
    }

    if (current.size() > 0)
        interior.push_back(current);
}

// Given a face in the world, subdivide it by all the relevant brushes and keep only the relevant parts:
void processFace(Ref<Brush> brush, VecA<Ref<Brush>> brushes, size_t faceNo, BvhNode* tree, Vec<size_t>& conn, Vec<float4>& exterior, Vec<float4>& interior) {
    const auto& facePlane = brush->planes[faceNo];
    const auto& faceNormal = brush->planes[faceNo].n;

    Vec<float4> interiorTemp, exteriorTemp;

    Vec<Vec<float4>> currentFaces, nextFaces;
    Vec<Vec<float4>> acceptedFaces;

    {
        Vec<float4> vs;
        for (const auto& v : brush->baseMesh.faces[faceNo].verts)
            vs.push_back(v);
        currentFaces.emplace_back(std::move(vs));
    }

    // Figure out which brushes are relevant to our face:
    conn.clear();
    visitBvhNode<Ref<Brush>>(tree, Aabb(currentFaces[0]), [&](const Ref<Brush>& brush) {
        conn.push_back(brush->index);
    });
    std::sort(begin(conn), end(conn));

    for (auto connectedIt = rbegin(conn); connectedIt != rend(conn); ++connectedIt) {
        const auto otherBrushNo = *connectedIt;
        const auto& otherBrush = brushes[otherBrushNo];
        const bool faceBeforeBrush = brush->index < otherBrushNo;

        if (otherBrush == brush) continue;

        // "compress" the empty faces:
        eraseIf(currentFaces, [](const Vec<float4>& verts) {
            return verts.empty();
        });

        // If we don't intersect with the base face before subdivisions then we can't intersect with any subcomponents:
        if (!brushInteractsWithFace(brush->baseMesh.faces[faceNo].verts, otherBrush))
            continue;

        const auto faceCount = currentFaces.size();
        if (brush->type == otherBrush->type) {
            for (size_t i = 0; i < faceCount; ++i) {
                if (!brushInteractsWithFace(currentFaces[i], otherBrush)) {
                    nextFaces.push_back(currentFaces[i]);
                } else {
                    removePartsInteriorToBrush(currentFaces[i], facePlane, otherBrush, !faceBeforeBrush, interiorTemp, exteriorTemp, nextFaces);
                }
            }
        } else {
            if (!faceBeforeBrush) {
                // We're after this so we know anything contained in it will definitely be accepted since it
                // overrides anything before it. Anything outside we need to keep processing.
                for (size_t i = 0; i < faceCount; ++i)
                    partitionFaceByBrush(currentFaces[i], otherBrush, acceptedFaces, nextFaces);
            } else {
                // We're overriden:
                for (size_t i = 0; i < faceCount; ++i)
                    removePartsInteriorToBrush(currentFaces[i], facePlane, otherBrush, faceBeforeBrush, interiorTemp, exteriorTemp, nextFaces);
            }
        }

        swap(currentFaces, nextFaces);
        nextFaces.clear();
    }

    const auto worldType = brushes[0]->type;

    emitFinalFaces(brush, acceptedFaces, faceNo);
    if (worldType == brush->type)
        emitFinalFaces(brush, currentFaces, faceNo);
}

struct FaceConstructor {
    Ref<Brush> _brush;
    Plane _facePlane;

    struct CsgPlane {
        int brushId, planeId;
    };
    struct CsgEdge {
        CsgPlane edge;
        CsgPlane start, end;
    };

    struct CsgFace {
        Vec<CsgEdge> edges;
    };

    void splitEdgeByPlane(CsgEdge edge, CsgPlane splitPlane) {
    }
};

void generateFinalFaces(Ref<Brush> brush, VecA<Ref<Brush>> brushes, const BvhTree<Ref<Brush>>& bvhTree) {
    brush->finalFaces.clear();

    // Scratch space for processFaces so it can be re-used across calls
    Vec<size_t> conn;
    Vec<float4> exterior, interior;

    // Index=-1 means we're deleted:
    if (brush->index != -1) {
        auto relevantParent = bvhTree.root->findRelevanceAncestor(brush->bounds);
        for (size_t faceNo = 0; faceNo < brush->baseMesh.faces.size(); ++faceNo)
            processFace(brush, brushes, faceNo, relevantParent, conn, exterior, interior);
    }
}

void generateCsgGeometry(VecA<Ref<Brush>> worldBrushes, const BvhTree<Ref<Brush>>& worldBvhTree, const Maybe<Set<Ref<Brush>>>& dirtyBrushes) {
    Timer timer;

    if (dirtyBrushes) {
        for (auto brush : *dirtyBrushes) {
            auto& brushes = brush->parent ? brush->parent->brushes : worldBrushes;
            auto& tree = brush->parent ? brush->parent->bvhTree : worldBvhTree;
            generateFinalFaces(brush, brushes, tree);
        }
    } else {
        for (auto brush : worldBrushes) {
            auto& brushes = brush->parent ? brush->parent->brushes : worldBrushes;
            auto& tree = brush->parent ? brush->parent->bvhTree : worldBvhTree;
            generateFinalFaces(brush, brushes, tree);
        }
    }

    OutputDebugStringA(format("CSG took: %f ms\n", timer.elapsedMs()).c_str());
}
