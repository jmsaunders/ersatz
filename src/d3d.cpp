// Random d3d stuff:

#include "common.h"
#include <functional>

#define INITGUID
#include <d3dcompiler.h>
#include "d3d.h"
#include "math_common.h"

#include "buffer_allocator.h"

#include "external/miniz.h"
#include "external/stb_image.h"

static Ref<Shader> compileShader(StrA path, ComPtr<ID3D11Device> dev, bool geo, const Set<Str>& defines) {
    unsigned flags = 0;
#ifdef _DEBUG
    flags |= D3DCOMPILE_DEBUG;
#endif

    if (auto file = readFile(path)) {
        ID3DBlob* errors = nullptr;

        Vec<D3D_SHADER_MACRO> macros;
        for (const auto& d : defines)
            macros.push_back({ d.c_str(), "" });
        macros.push_back({ nullptr, nullptr });

        ComPtr<ID3D11VertexShader> vs;
        ComPtr<ID3D11GeometryShader> gs;
        ComPtr<ID3D11PixelShader> ps;

        ComPtr<ID3DBlob> vsblob, gsblob, psblob;

        HRESULT hr;

        {
            hr = D3DCompile(&file->at(0), file->size(), path.c_str(), macros.data(), nullptr, "vertex", "vs_4_0", flags, 0, &vsblob, &errors);
            if (errors && errors->GetBufferSize() != 0) {
                OutputDebugStringA((char*)errors->GetBufferPointer());
                unreachable("could not compile shader");
            }

            if (FAILED(hr)) unreachable("failed to compile shader");

            hr = dev->CreateVertexShader(vsblob->GetBufferPointer(), vsblob->GetBufferSize(), nullptr, &vs);
            if (FAILED(hr)) unreachable("could not create vertex shader");
        }

        {
            hr = D3DCompile(&file->at(0), file->size(), path.c_str(), macros.data(), nullptr, "pixel", "ps_4_0", flags, 0, &psblob, &errors);
            if (errors && errors->GetBufferSize() != 0) {
                OutputDebugStringA((char*)errors->GetBufferPointer());
                unreachable("errors compiling pixel shader");
            }

            if (hr != S_OK) unreachable("could not compile pixel shader");

            hr = dev->CreatePixelShader(psblob->GetBufferPointer(), psblob->GetBufferSize(), nullptr, &ps);
            if (FAILED(hr))
                unreachable("failed to create pixel shader");
        }

        if (geo) {
            hr = D3DCompile(&file->at(0), file->size(), path.c_str(), macros.data(), nullptr, "geometry", "gs_4_0", flags, 0, &gsblob, &errors);
            if (errors && errors->GetBufferSize() != 0) {
                OutputDebugStringA((char*)errors->GetBufferPointer());
                unreachable("errors compiling geometry shader");
            }

            if (hr != S_OK) unreachable("could not compile geometry shader");

            hr = dev->CreateGeometryShader(gsblob->GetBufferPointer(), gsblob->GetBufferSize(), nullptr, &gs);
            if (FAILED(hr))
                unreachable("failed to create geometry shader");
        }

        return mkref<Shader>(vs, vsblob, ps, psblob, gs, gsblob);
    }

    return nullptr;
}

void Shader::bind(const ComPtr<ID3D11DeviceContext>& dc) {
    dc->VSSetShader(vs.Get(), nullptr, 0);
    dc->PSSetShader(ps.Get(), nullptr, 0);
    dc->GSSetShader(gs.Get(), nullptr, 0);
}

Ref<ResourceManager> resourceManager;

Ref<Shader> ResourceManager::requestShader(StrA path, const Set<Str>& defines, bool geo) {
    auto it = shaders.find(path);
    if (it != shaders.end()) {
        auto it2 = it->second.find(defines);
        if (it2 != it->second.end()) return it2->second;
    }

    if (auto ret = compileShader(path, m_device, geo, defines)) {
        shaders[path][defines] = ret;
        return ret;
    } else {
        unreachable("could not compile shader");
    }
}

static TextureInfo loadTexture(StrA name, const ComPtr<ID3D11Device>& device,
        const ComPtr<ID3D11DeviceContext>& context, void* data, size_t size) {
    int width, height, channels;
    auto decodedImage = stbi_load_from_memory((stbi_uc*)data, int(size), &width, &height, &channels, 4);

    D3D11_TEXTURE2D_DESC desc = {};
    desc.Width = width;
    desc.Height = height;
    desc.Usage = D3D11_USAGE_DEFAULT;
    desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    desc.ArraySize = 1;
    desc.MipLevels = 0;
    desc.SampleDesc.Count = 1;
    desc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET;
    desc.MiscFlags = D3D11_RESOURCE_MISC_GENERATE_MIPS;

    // TODO this is hardcoded to 4 bytes per pixel?
    D3D11_SUBRESOURCE_DATA initial = { decodedImage, UINT(width * 4) };

    ComPtr<ID3D11Texture2D> tex;
    HRESULT hr = device->CreateTexture2D(&desc, nullptr, &tex);
    if (FAILED(hr)) unreachable("couldn't create texture");

    ComPtr<ID3D11ShaderResourceView> srv;
    if (FAILED(device->CreateShaderResourceView(tex.Get(), nullptr, &srv))) unreachable("couldn't create texture view");

    context->UpdateSubresource(tex.Get(), 0, nullptr, decodedImage, UINT(width * 4), 0);
    context->GenerateMips(srv.Get());

    stbi_image_free(decodedImage);
    return { name, srv, width, height };
}

TextureInfo ResourceManager::defaultTexture() {
    auto ret = requestTexture("res/default_diffuse.png");
    if (!ret) unreachable("can't find default texture");
    return *ret;
}

Maybe<TextureInfo> ResourceManager::requestTexture(StrA path) {
    auto it = _textures.find(path);
    if (it != end(_textures)) return it->second;

    TextureInfo ret;
    auto data = readFile(path);
    if (data) {
        _textures[path] = ret = loadTexture(path, m_device, m_context, &data->at(0), data->size());
    }
    return ret;
}

Maybe<TextureInfo> ResourceManager::requestTextureFromArchive(StrA archive, StrA path) {
    auto& textureInfo = _archiveTextures[archive][path];
    if (textureInfo) return textureInfo;

    char fullPath[2048];
    snprintf(fullPath, sizeof(fullPath), "res/QRP/%s", path.c_str());

    if (auto data = readEntireFile(fullPath)) {
        textureInfo = loadTexture(path, m_device, m_context, data->data(), data->size());
    }

    // @TODO loading stuff from zip files is wack slow:
#if 0
    mz_zip_archive arch = {};

    if (auto cached = lookup(_archives, archive)) {
        arch = *cached;
    } else {
        if (!mz_zip_reader_init_file(&arch, archive.c_str(), 0)) {
            OutputDebugStringA(SSS("Failed to create archive " << archive << " handle for file " << path << "\n").c_str());
            return none;
        }
        _archives[archive] = arch;
    }

    int goodFile = mz_zip_reader_locate_file(&arch, path.c_str(), "", 0);
    if (goodFile != -1) {
        size_t size;
        void* buffer = mz_zip_reader_extract_to_heap(&arch, goodFile, &size, 0);
        if (buffer) {
            textureInfo = loadTexture(device, buffer, size);
        } else {
            OutputDebugStringA(SSS("Failed to extract from archive " << archive << " file " << path << "\n").c_str());
        }

        mz_free(buffer);
    } else {
        //OutputDebugStringA(SSS("Failed to find from archive " << archive << " file " << path << "\n").c_str());
    }
#endif

    //mz_zip_reader_end(&arch);

    return textureInfo;
}
