#include "common.h"

#include <mutex>

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

using namespace std;

thread_local std::stringstream tempStringStream;

static int64_t _performanceFrequency;
static once_flag timerInit;

#include "external/fast_double_parser.h"

float parseFloat(StrView str) {
    double result;
    auto ret = fast_double_parser::parse_number(str.data(), &result);
    assert(ret);
    return float(result);
}

Str toUpper(StrA in) {
    auto ret = in;
    std::transform(ret.begin(), ret.end(), ret.begin(), ::toupper);
    return ret;
}

Maybe<Str> readFile(const Str& name) {
    FILE* f = fopen(name.c_str(), "rb");
    if (!f) return none;

    fseek(f, 0, SEEK_END);
    Str ret;
    ret.resize(ftell(f));
    fseek(f, 0, SEEK_SET);
    fread(&ret[0], 1, ret.size(), f);
    fclose(f);
    return ret;
}

Vec<Str> separateFilePath(StrA inputPathIn) {
    auto inputPath = inputPathIn;

    Vec<Str> ret;
    while (inputPath.size() > 0) {
        auto sepPos = inputPath.find(_pathSeparator);
        if (sepPos != Str::npos) {
            ret.push_back(inputPath.substr(0, sepPos));
            inputPath = inputPath.substr(sepPos + 1);
        } else {
            ret.push_back(inputPath);
            break;
        }
    }

    return ret;
}

Pair<Str, Str> separateFilePathFromName(StrA inputPath) {
    Str path, file = inputPath;

    auto cutoffIt = std::find_if(rbegin(inputPath), rend(inputPath),
                               [](char c) { return c == '\\' || c == '/'; });

    if (cutoffIt != inputPath.rend()) {
        auto cutoff = distance(cutoffIt, rend(inputPath));
        path = inputPath.substr(0, cutoff - 1);
        file = inputPath.substr(cutoff);
    }

    return make_pair(path, file);
}

bool isRelativePath(StrA path) {
    // Linux:
    if (path[0] == '/')
        return false;

    // Windows:
    if (path.size() > 1 && isalpha(path[0]) && path[1] == ':')
        return false;

    return true;
}

bool isPathSeparator(char c) {
    return c == '\\' || c == '/';
}

Str joinPath(StrA lhs, StrA rhs) {
    if (lhs.empty())
        return rhs;
    if (rhs.empty())
        return lhs;

    if (isPathSeparator(lhs.back()))
        return lhs + rhs;

    return lhs + _pathSeparator + rhs;
}

bool fileExists(StrA path) {
    auto f = fopen(path.c_str(), "r");
    if (f) {
        fclose(f);
        return true;
    }
    return false;
}

char* heapBackString(const Str& str) {
    char* text = new char[str.size() + 1];
    for (size_t i = 0; i < str.size(); ++i)
        text[i] = str.at(i);
    text[str.size()] = '\0';
    return text;
}

#ifdef _WIN32

Timer::Timer() {
    call_once(timerInit, []() {
        LARGE_INTEGER pf;
        QueryPerformanceFrequency(&pf);
        _performanceFrequency = pf.QuadPart;
    });

    reset();
}

void Timer::reset() {
    LARGE_INTEGER s;
    QueryPerformanceCounter(&s);
    _start = s.QuadPart;
}

double Timer::elapsedS() const {
    LARGE_INTEGER s;
    QueryPerformanceCounter(&s);
    return double(s.QuadPart - _start) / _performanceFrequency;
}

double Timer::elapsedMs() const {
    return elapsedS() * 1000;
}

Pair<Str, Str> catPath(const Vec<Str>& path, char separator) {
    stringstream ss;
    bool first = true;
    for (size_t i = 0; i < path.size() - 1; ++i) {
        if (!first)
            ss << separator;
        ss << path[i];
        first = false;
    }

    return make_pair(ss.str(), path.back());
}

Str workingDirectory() {
    // We allocate +1 bytes because GetCurrentDirectory writes the null term explicitly.
    // Just resize it away after:

    auto len = GetCurrentDirectoryA(0, nullptr);
    Str ret(len + 1, ' ');
    len = GetCurrentDirectoryA(len, ret.data());
    ret.resize(len);
    return ret;
}

Wstr workingDirectoryW() {
    // We allocate +1 bytes because GetCurrentDirectory writes the null term explicitly.
    // Just resize it away after:

    auto len = GetCurrentDirectoryW(0, nullptr);
    Wstr ret(len + 1, ' ');
    len = GetCurrentDirectoryW(len, ret.data());
    ret.resize(len);
    return ret;
}

void debugBreak() {
    DebugBreak();
}

MappedFile::MappedFile(StrA path) {
    fh = CreateFileA(path.c_str(), GENERIC_READ, 0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);
    assert(fh);

    size = GetFileSize(fh, nullptr);

    mapping = CreateFileMappingA(fh, nullptr, PAGE_READONLY, 0, 0, nullptr);
    assert(mapping);

    buffer = (const char*)MapViewOfFile(mapping, FILE_MAP_READ, 0, 0, size);
    assert(buffer);
}

Event::Event() {
    _evt = CreateEventA(nullptr, false, false, "");
}

void Event::signal() {
    SetEvent(_evt);
}

void Event::wait() {
    WaitForSingleObject(_evt, INFINITE);
}

#else

#error provide platform implementation

#endif

Maybe<Vec<unsigned char>> readEntireFile(const char* name) {
    auto f = fopen(name, "rb");
    if (!f) {
        return none;
    }

    fseek(f, 0, SEEK_END);
    auto sz = ftell(f);
    std::vector<unsigned char> ret;
    ret.resize(sz);
    fseek(f, 0, SEEK_SET);
    fread(&ret[0], 1, sz, f);
    fclose(f);
    return ret;
}
