#pragma once

#include <shared_mutex>
#include "threadsafe_deque.h"

using Fiber = void*;

// Multi-producer multi-consumer data pool
template<typename T>
struct ThreadsafePool {
    std::mutex mutex;
    Deque<T> data;

    ThreadsafePool(size_t lgsize) : data(lgsize) {}

    void push(const T& f) {
        std::unique_lock lock(mutex);
        if (!data.push(f)) unreachable("ran out of pool space");
    }

    Maybe<T> pop() {
        Maybe<T> ret;
        while (true) {
            bool raced = false;
            ret = data.pilfer(&raced);
            if (!raced) break;
        }
        return ret;
    }
};

struct TaskBatch {
    std::atomic<int> counter = 0;

    // This exists to synchronize hibernating the waiting thread:
    std::shared_mutex mutex;

    Maybe<std::condition_variable_any> externalWaiting;

    // The fiber we continue when our counter hits zero:
    Fiber suspendedFiber = 0;
};

enum class TaskPriority { LOW, MEDIUM, HIGH };
void submitUntyped(void (*f)(void*), void* data, TaskPriority priority, TaskBatch* batch);

template<typename T>
void submit(void (*f)(T* t), T* data, TaskPriority priority, TaskBatch* batch) {
    submitUntyped((void (*)(void*)) f, (void*)data, priority, batch);
}

void wait(TaskBatch* batch);

void startJobs();
void shutdownJobs();

struct ThreadLocalTaskData* accessTlsTask();
