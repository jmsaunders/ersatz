#include "common.h"
#include "commands.h"
#include "editor_state.h"

Set<Ref<Brush>> UndoManager::undo() {
    Set<Ref<Brush>> dirty;

    if (_undos.size() > 0) {
        auto elem = _undos.back();
        _undos.pop_back();

        _world.findBrushesInteract(dirty, elem->brushes());
        elem->undo(_world);
        _world.findBrushesInteract(dirty, elem->brushes());

        _redos.push_back(elem);
    }

    return dirty;
}

Set<Ref<Brush>> UndoManager::redo() {
    Set<Ref<Brush>> dirty;

    if (_redos.size() > 0) {
        auto elem = _redos.back();
        _redos.pop_back();

        _world.findBrushesInteract(dirty, elem->brushes());
        elem->redo(_world);
        _world.findBrushesInteract(dirty, elem->brushes());

        _undos.push_back(elem);
    }

    return dirty;
}

// Transform:

void TransformBrushCommand::undo(EditorWorld& world) {
    brush->planes = oldPlanes;
    brush->regenerateBase();

    auto& bvhTree = brush->parent ? brush->parent->bvhTree : world.bvhTree;
    bvhTree.update(brush, brush->bounds);
}

void TransformBrushCommand::redo(EditorWorld& world) {
    brush->planes = newPlanes;
    brush->regenerateBase();

    auto& bvhTree = brush->parent ? brush->parent->bvhTree : world.bvhTree;
    bvhTree.update(brush, brush->bounds);
}

// Invert:

void InvertBrushCommand::undo(EditorWorld&) {
    for (auto& brush : _brushes) {
        brush->flip();
        brush->regenerateBase();
    }
}

void InvertBrushCommand::redo(EditorWorld&) {
    for (auto& brush : _brushes) {
        brush->flip();
        brush->regenerateBase();
    }
}

// Append:

void AppendBrushCommand::undo(EditorWorld& world) {
    assert(world.brushes.back() == brush);
    brush->index = -1;
    world.brushes.erase(world.brushes.end() - 1);

    auto& bvhTree = brush->parent ? brush->parent->bvhTree : world.bvhTree;
    bvhTree.remove(brush);
}

void AppendBrushCommand::redo(EditorWorld& world) {
    brush->index = world.brushes.size();
    world.brushes.push_back(brush);

    auto& bvhTree = brush->parent ? brush->parent->bvhTree : world.bvhTree;
    bvhTree.insert(brush, brush->bounds);
}

// Delete:

void DeleteBrushesCommand::undo(EditorWorld& world) {
    Set<Vec<Ref<Brush>>*> dirtyLists;

    for (auto brushInfo : _brushes) {
        auto brush = brushInfo.brush;
        auto& brushes = brush->parent ? brush->parent->brushes : world.brushes;
        auto& bvhTree = brush->parent ? brush->parent->bvhTree : world.bvhTree;

        dirtyLists.insert(&brushes);

        brushes.insert(brushes.begin() + brushInfo.index, brush);
        bvhTree.insert(brush, brush->bounds);
    }

    for (auto dirtyList : dirtyLists) {
        for (auto [index, brush] : enumerate(*dirtyList))
            brush->index = index;
    }
}

void DeleteBrushesCommand::redo(EditorWorld& world) {
    Set<Vec<Ref<Brush>>*> dirtyLists;

    for (size_t j = 0; j < _brushes.size(); ++j) {
        size_t idx = _brushes.size() - 1 - j;
        auto brushInfo = _brushes[idx];
        auto brush = brushInfo.brush;

        auto& brushes = brush->parent ? brush->parent->brushes : world.brushes;
        auto& bvhTree = brush->parent ? brush->parent->bvhTree : world.bvhTree;

        dirtyLists.insert(&brushes);

        brushes.erase(brushes.begin() + brush->index);
        bvhTree.remove(brush);
        brush->index = -1;
    }

    for (auto dirtyList : dirtyLists) {
        for (auto [index, brush] : enumerate(*dirtyList))
            brush->index = index;
    }
}

// Change texture:

void ChangeTextureCommand::undo(EditorWorld& world) {
    for (auto c : changes)
        c.brush->textureInfo[c.face].tex = c.old;
    for (const auto& b : brushes())
        b->regenerateBase();
}

void ChangeTextureCommand::redo(EditorWorld& world) {
    for (auto c : changes)
        c.brush->textureInfo[c.face].tex = c.newer;
    for (const auto& b : brushes())
        b->regenerateBase();
}

Set<Ref<Brush>> ChangeTextureCommand::brushes() const {
    Set<Ref<Brush>> ret;
    for (auto c : changes) ret.insert(c.brush);
    return ret;
}
