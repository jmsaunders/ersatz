//
// common.h
// Common (PCH) header:
//

#pragma once

#define _CRT_SECURE_NO_WARNINGS

#include <algorithm>
#include <assert.h>
#include <cstdarg>
#include <cstdint>
#include <cstring>
#include <memory>
#include <optional>
#include <set>
#include <stack>
#include <string>
#include <unordered_map>
#include <vector>
#include <sstream>
#include <map>
#include <string_view>
#include <queue>
#include <functional>

extern bool g_keyDownInLastFrame[256];

//#define ignore(x) (void)x
template<typename T>
void ignore(const T& /*ignored*/) {}

using Str = std::string;
using StrA = const Str&;
using StrView = std::string_view;
using Wstr = std::wstring;
using WstrA = const std::wstring&;

template <typename T> using Own = std::unique_ptr<T>;
template <typename T> using Ref = std::shared_ptr<T>;

template <typename T> using Stack = std::stack<T>;
template <typename T> using Vec = std::vector<T>;
template <typename T> using Set = std::set<T>;
template <typename K, typename V, typename Hasher=std::hash<K>> using Map = std::unordered_map<K, V, Hasher>;
template <typename K, typename V> using CompMap = std::map<K, V>;
template <typename X, typename Y> using Pair = std::pair<X, Y>;

template <typename T> using RefA = const Ref<T>&;
template <typename T> using VecA = const std::vector<T>&;

template <typename T> using Maybe = std::optional<T>;
constexpr auto none = std::nullopt;

struct Timer {
    Timer();
    double elapsedS() const;
    double elapsedMs() const;
    void reset();

  private:
    int64_t _start;
};

template <typename T, typename U> Ref<T> dyn_cast(const Ref<U>& u) {
    return std::dynamic_pointer_cast<T>(u);
}

template <typename T, typename U> bool is(const Ref<U>& u) {
    return dyn_cast<T>(u) != nullptr;
}

#define NOMINMAX
#include <Windows.h>

[[noreturn]] inline void unreachable(const char* msg) {
    MessageBoxA(nullptr, msg, "oops", MB_OK);
    abort();
}

extern thread_local std::stringstream tempStringStream;
#define SSS(x) (tempStringStream.str(""), (tempStringStream << x), tempStringStream.str())
#define YESNO(x) (x ? "YES" : "NO")
#define TRUEFALSE(x) (x ? "TRUE" : "FALSE")

template <typename Key, typename Value, typename LookupKey>
Maybe<Value> lookup(const Map<Key, Value>& map, const LookupKey& key) {
    auto it = map.find(key);
    if (it == map.end())
        return none;
    return it->second;
}

template <typename Key> bool lookup(const Set<Key>& map, const Key& key) {
    return map.find(key) != map.end();
}

// http://www.reedbeta.com/blog/python-like-enumerate-in-cpp17/
template <typename T, typename TIter = decltype(std::begin(std::declval<T>())),
          typename = decltype(std::end(std::declval<T>()))>
constexpr auto enumerate(T&& iterable) {
    struct iterator {
        size_t i;
        TIter iter;
        bool operator!=(const iterator& other) const { return iter != other.iter; }
        void operator++() {
            ++i;
            ++iter;
        }
        auto operator*() const { return std::tie(i, *iter); }
    };
    struct iterable_wrapper {
        T iterable;
        auto begin() { return iterator{ 0, std::begin(iterable) }; }
        auto end() { return iterator{ 0, std::end(iterable) }; }
    };
    return iterable_wrapper{ std::forward<T>(iterable) };
}

// These exist so we can have static creators accessing private constructors.
// This is a problem because make_shared<> can't access, so gotta call new then wrap.

template <typename T> Ref<T> wrapShare(T* t) {
    return Ref<T>(t);
}

template <typename T> Ref<T> wrapUnique(T* t) {
    return Own<T>(t);
}

template <typename T, typename... Args> Ref<T> mkref(Args&&... args) {
    return std::shared_ptr<T>(new T(std::forward<Args>(args)...));
}

template <typename T, typename... Args> Own<T> mkown(Args&&... args) {
    return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}

template<typename T, size_t Size, size_t Align=8>
class Fwd {
    typename std::aligned_storage<Size, Align>::type _data;
    T* ptr() {
        return reinterpret_cast<T*>(&_data);
    }

public:
    template <typename... Args>
    Fwd(Args&&... args) {
        if constexpr (sizeof(T) > Size) {
            char (*__fail)[sizeof(T)] = 1;
            static_assert(false, "Inadequate static size provided");
        }

        static_assert(alignof(T) == Align);
        new (reinterpret_cast<void*>(&_data)) T(std::forward<Args>(args)...);
    }

    ~Fwd() {
        ptr()->~T();
    }

    operator T& () {
        return *ptr();
    }

    T* operator-> () {
        return ptr();
    }
};

// Variable flags:
enum class VFlags {
    none,
    // reference type:
    ref,
};

// This is a pretty ugly hack:
extern Map<Str, Ref<struct LexedFile>> lexedFiles;

//
// File and path helpers:
//

// Support helpers for dealing with file system stuff:
static const char _pathSeparator = '\\';

bool isPathSeparator(char c);
Str joinPath(StrA lhs, StrA rhs);
bool fileExists(StrA path);
Maybe<Str> readFile(const Str& name);
Str workingDirectory();
Wstr workingDirectoryW();
Vec<Str> separateFilePath(StrA inputPath);
Pair<Str, Str> separateFilePathFromName(StrA inputPath);
bool isRelativePath(StrA path);
Pair<Str, Str> catPath(const Vec<Str>& path, char separator = _pathSeparator);
Maybe<Vec<unsigned char>> readEntireFile(const char* name);


char* heapBackString(const Str& str);

#ifdef __clang__
#define FORMAT_ATTRIBUTE(x, y)    __attribute__((format (printf, x, y)))
#else
#define FORMAT_ATTRIBUTE(x, y)
#endif

inline Str format(const char* fmt, va_list va) {
    auto cnt = int64_t(_vscprintf(fmt, va)) + 1;

    Vec<char> buffer(cnt);
    _vsnprintf_s(&buffer[0], cnt, cnt - 1, fmt, va);
    va_end(va);

    return Str(buffer.begin(), buffer.end());
}

FORMAT_ATTRIBUTE(1, 2) inline Str format(const char* fmt, ...) {
    va_list va;
    va_start(va, fmt);
    return format(fmt, va);
}

void debugBreak();

// TODO move these random helpers somewhere else..

inline bool hasPrefix(const char* big, const char* prefix) {
    while (*big == *prefix) {
        ++big;
        ++prefix;
    }

    return *prefix == '\0';
}

inline bool hasPrefix(const std::string& big, const char* prefix) {
    return hasPrefix(big.c_str(), prefix);
}

inline bool hasSuffix(StrA big, StrA suffix) {
    if (big.size() < suffix.size()) return false;

    auto sit = suffix.rbegin(), bit = big.rbegin();
    for (; sit != suffix.rend() && bit != big.rend(); ++sit, ++bit) {
        if (*sit != *bit) return false;
    }

    return true;
}

template<typename T> inline Maybe<T> decode(const Str& s);

template<> inline Maybe<int> decode(const Str& s) {
    try {
        return std::stoi(s);
    } catch (...) {
        return none;
    }
}

template<> inline Maybe<float> decode(const Str& s) {
    try {
        return std::stof(s);
    } catch (...) {
        return none;
    }
}

template<class InputIt1, class InputIt2, class OutputIt>
OutputIt mergeIntersection(InputIt1 first1, InputIt1 last1, InputIt2 first2, InputIt2 last2, OutputIt d_first) {
    while (first1 != last1 && first2 != last2) {
        if (*first1 == *first2) {
            *d_first++ = *first1;
            ++first1;
            ++first2;
        } else if (*first1 < *first2) {
            ++first1;
        } else {
            ++first2;
        }
    }

    return d_first;
}

// :( God I hate WRL
#include <wrl/client.h>
using Microsoft::WRL::ComPtr;
template<typename T> using ComPtrA = const ComPtr<T>&;

template<typename Cont, typename Pred>
void eraseIf(Cont& c, Pred pred) {
    auto it = std::remove_if(c.begin(), c.end(), pred);
    c.erase(it, c.end());
}

struct MappedFile {
    HANDLE fh = nullptr, mapping = nullptr;
    DWORD size = 0;
    const char* buffer = nullptr;

    MappedFile(StrA path);

    MappedFile(const MappedFile& other) = delete;

    MappedFile(MappedFile&& other) noexcept {
        std::swap(fh, other.fh);
        std::swap(mapping, other.mapping);
        std::swap(size, other.size);
        std::swap(buffer, other.buffer);
    }

    ~MappedFile() {
        UnmapViewOfFile(buffer);
        CloseHandle(mapping);
        CloseHandle(fh);
    }
};

float parseFloat(StrView str);
Str toUpper(StrA in);

enum class SigType {
    Once,
    All,
};

class Event {
    void* _evt;
public:
    Event();

    void signal();
    void wait();
};

struct ThreadLocalTaskData* accessTlsTask();
