#pragma once

#include "index_allocators.h"
#include "d3d.h"
#include "csg.h"
#include "editor_state.h"

class VertexBufferAllocator;
class MapRenderer {
    ComPtr<ID3D11InputLayout> _inputLayout;
    ComPtr<ID3D11SamplerState> _samplerState;
    ComPtr<ID3D11RasterizerState> _lineRasterizerState;

    ComPtr<ID3D11Device> _device;
    ComPtr<ID3D11DeviceContext> _context;
    Ref<Shader> _filledShader, _lineShader;
    EditorWorld* _world;

    // One primary vertex buffer shared by all map rendering data:
    std::unique_ptr<VertexBufferAllocator> _vertexBufferAllocator;
    ComPtr<ID3D11Buffer> _vertexBuffer;

    IndexBufferSectionAllocator _indexSectionAllocator;
    SubIndexAllocator* _wireframeIndexAllocator;
    ComPtr<ID3D11Buffer> _indexBuffer;

    ComPtr<ID3D11Buffer> _primaryConstantBuffer, _linesConstantBuffer;

    struct ConstantBuffer {
        float4x4 mvp;
        float4 colour;
        float4 viewport;
    };

    using TexId = ID3D11ShaderResourceView*;

    // Per-pass state information associated with a single draw call:
    Map<TexId, SubIndexAllocator*> _passState, _selectedState;

    // Per-brush information:
    struct BrushState {
        Vec<SubIndexAllocation> indexBlocks;
        Vec<BufferAllocator<CsgVertex>::Block*> vertexBlocks;
        Maybe<SubIndexAllocation> lineBlock;
    };

    Map<Ref<Brush>, BrushState> _brushState;

    void updateBuffersForBrush(RefA<Brush> brush);
    void clearPriorBrushState(BrushState& bs);

public:
    MapRenderer(const ComPtr<ID3D11Device>& dev, const ComPtr<ID3D11DeviceContext>& dc, EditorWorld* map);
    ~MapRenderer();

    void updateVertexBuffer(Maybe<Set<Ref<Brush>>> dirtyBrushes = none);
    void draw(const ComPtr<ID3D11DeviceContext>& dc, const float4x4& mvp, float2 viewport, bool lines, bool filled);
};
