#include "common.h"
#include "orthographic_viewport.h"

const float3 xAxis(1, 0, 0), yAxis(0, 1, 0), zAxis(0, 0, 1);

OrthographicViewport::OrthographicViewport(EditorWorld* world, ComPtrA<ID3D11Device> device, ComPtrA<ID3D11DeviceContext> context, Type type)
        : EditorViewport(device, context), _world(world), _im(device, context), _worldMeshes(device, context), _type(type) {
    _basis = (_type == TOP_VIEW) ? float3x3(xAxis, zAxis, -yAxis)
           : (_type == SIDE_VIEW) ? float3x3(xAxis, yAxis, -zAxis)
           : (_type == FRONT_VIEW) ? float3x3(zAxis, yAxis, -xAxis)
           : identity;

    for (float i = -_maxGrid; i <= _maxGrid; i += _smallStep) {
        auto x1 = mul(_basis, float3(i, -_maxGrid, 0.f));
        auto x2 = mul(_basis, float3(i,  _maxGrid, 0.f));
        auto y1 = mul(_basis, float3(-_maxGrid, i, 0.f));
        auto y2 = mul(_basis, float3( _maxGrid, i, 0.f));

        if (isZero(fmod(i, _bigStep))) {
            const float4 boldColour(0.45f, 0.45f, 0.45f, 1.f);
            _im.addLine(x1, x2, 2.f, boldColour);
            _im.addLine(y1, y2, 2.f, boldColour);
        } else {
            const float4 lightColour(0.6f, 0.6f, 0.6f, 1.f);
            // Push out the light parts into the screen a bit:
            auto offs = mul(_basis, float3(0, 0, 1.f));
            _im.addLine(x1 + offs, x2 + offs, 1.f, lightColour);
            _im.addLine(y1 + offs, y2 + offs, 1.f, lightColour);
        }
    }

    for (const auto& mesh : _world->meshes) {
        for (const auto& face : mesh.faces) {
            for (size_t i = 0; i < face.verts.size() - 1; ++i)
                _worldMeshes.addLine(face.verts[i].xyz(), face.verts[i + 1].xyz(), 2.f, { 0.f, 0.1f, 0.5f, 1.f });
            _worldMeshes.addLine(face.verts.back().xyz(), face.verts[0].xyz(), 2.f, { 0.f, 0.1f, 0.5f, 1.f });
        }
    }
}

float4x4 orthographicProjection(float x1, float x2, float y1, float y2, float znear, float zfar) {
    return {
        { 2 / (x2 - x1), 0, 0, -(x1 + x2) / (x2 - x1) },
        { 0, 2 / (y2 - y1), 0, -(y1 + y2) / (y2 - y1) },
        { 0, 0, -2 / (zfar - znear), -(zfar + znear) / (zfar - znear) },
        { 0, 0, 0, 1 },
    };
}

float4x4 extend(const float3x3& m) {
    return {
        { m.x, 0 },
        { m.y, 0 },
        { m.z, 0 },
        { 0, 0, 0, 1 },
    };
}

void OrthographicViewport::drawInternal() {
    float bg[] = { 0.8f, 0.8f, 0.8f, 1.0f };
    _context->ClearRenderTargetView(_renderTargetView.Get(), bg);
    _context->ClearDepthStencilView(_depthStencilView.Get(), D3D11_CLEAR_DEPTH, 1.f, 0);

    auto sz = size();

    float4x4 modelView = transpose(viewMatrix());
    float4x4 projection = transpose(orthographicProjection(-sz.x / 2, sz.x / 2, -sz.y / 2, sz.y / 2, -8192, 8192));

    _im.render(modelView, projection, int(sz.x), int(sz.y));

    // Reset because we want the world to show up over top the grid:
    _context->ClearDepthStencilView(_depthStencilView.Get(), D3D11_CLEAR_DEPTH, 1.f, 0);
    _worldMeshes.render(modelView, projection, int(sz.x), int(sz.y));
}

float4x4 OrthographicViewport::viewMatrix() const {
    return mul(
        extend(_basis),
        scaling_matrix(float3(_scale, _scale, _scale)),
        translation_matrix(_center));
}

void OrthographicViewport::mouseDrag(int dx, int dy) {
    float4 local(float(dx), float(-dy), 0, 0);
    auto world = mul(inverse(viewMatrix()), local);

    _center += world.xyz();
}

void OrthographicViewport::mouseWheel(bool up, int x, int y) {
    auto start = screenToWorld(x, y);

    if (up)
        _scale *= 1.2f;
    else
        _scale /= 1.2f;

    auto diff = screenToWorld(x, y) - start;
    _center += diff.xyz();
}

float4 OrthographicViewport::screenToWorld(int x, int y) {
    auto sz = size();
    auto viewInverse = inverse(viewMatrix());
    return mul(viewInverse, float4(float(x - sz.x/2), float(sz.y/2 - y), 0, 1));
}