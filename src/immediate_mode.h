#pragma once

#include "d3d.h"

using namespace linalg;
using namespace linalg::aliases;

struct ImmediateManager {
    ImmediateManager(ComPtr<ID3D11Device> device, ComPtr<ID3D11DeviceContext> dc);
    void addLine(float3 start, float3 end, float width = 2.f, float4 c = { 1.f, 1.f, 1.f, 1.f });

    // Prepare the object buffers to render:
    void prepareRender();

    void render(const float4x4& modelView, const float4x4& projection, int width, int height);
    void reset();

    struct BufferElem {
        float4 pos;
    };

private:
    Vec<BufferElem> m_buffer;
    ComPtr<ID3D11InputLayout> m_inputLayout;
    ComPtr<ID3D11Device> m_device;
    ComPtr<ID3D11DeviceContext> m_dc;
    ComPtr<ID3D11Buffer> _vertexBuffer;
    ComPtr<ID3D11RasterizerState> lineRasterizerState;
    Ref<Shader> shader;
    ComPtr<ID3D11Buffer> _c2Buffer;
};
