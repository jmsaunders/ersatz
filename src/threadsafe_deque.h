#pragma once

template<typename T>
class Deque {
    struct CircularBuffer {
        T* _data;
        size_t _lgSize;

        CircularBuffer(size_t lgSize) : _lgSize(lgSize) {
            _data = new T[size()];
        }
        ~CircularBuffer() {
            delete _data;
        }

        T& get(size_t idx) {
            return _data[idx & (size() - 1)];
        }

        size_t size() const {
            return size_t(1) << _lgSize;
        }
    };

    std::atomic<int64_t> top = 0, bottom = 0;
    CircularBuffer buffer;
public:

    Deque(size_t size) : buffer(size) {
    }

    // This is only used to get an upper-bound on the size. It's only safe to use within
    // the thread to make sure it cannot be larger than a given amount. We use this so
    // we can be sure we're able to push ourselves onto a work queue.
    int64_t size() const {
        return bottom - top;
    }

    int64_t capacity() const {
        return buffer.size();
    }

    bool push(const T& val) {
        int64_t b = bottom;
        int64_t t = top.load(std::memory_order::memory_order_acquire);

        if (b - t > int64_t(buffer.size() - 1)) {
            return false;
        }

        buffer.get(b) = val;
        bottom.store(b + 1, std::memory_order::memory_order_release);
        return true;
    }

    Maybe<T> pop() {
        int64_t b = bottom - 1;
        // If we resize buffers we need to grab the buffer we're working with here
        bottom = b;
        int64_t t = top;

        Maybe<T> ret;
        if (t <= b) {
            ret = buffer.get(b);
            if (t == b) {
                if (!top.compare_exchange_strong(t, t + 1, std::memory_order::memory_order_seq_cst))
                    ret = none;
                bottom = b + 1;
            }
        } else {
            bottom = b + 1;
        }

        return ret;
    }

    Maybe<T> pilfer(bool* raced) {
        *raced = false;
        int64_t t = top.load(std::memory_order::memory_order_acquire);
        int64_t b = bottom.load(std::memory_order::memory_order_acquire);

        Maybe<T> ret;
        if (t < b) {
            ret = buffer.get(t);
            if (!top.compare_exchange_strong(t, t + 1, std::memory_order::memory_order_seq_cst)) {
                *raced = true;
                ret = none;
            }
        }
        return ret;
    }
};
