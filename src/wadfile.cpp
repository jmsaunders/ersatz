#include "common.h"
#include "wadfile.h"

// https://www.gamers.org/dEngine/quake/spec/quake-spec34/qkspec_4.htm

struct wadhead_t {
    u_char magic[4];             // "WAD2", Name of the new WAD format
    long numentries;             // Number of entries
    long diroffset;              // Position of WAD directory in file
};

struct wadentry_t {
    long offset;                 // Position of the entry in WAD
    long dsize;                  // Size of the entry in WAD file
    long size;                   // Size of the entry in memory
    char type;                   // type of entry
    char cmprs;                  // Compression. 0 if none.
    short dummy;                 // Not used
    char name[16];               // 1 to 16 characters, '\0'-padded
};

static constexpr size_t MAXTEXTURENAME = 16;
static constexpr size_t MIPLEVELS = 4;

struct wadmiptex_t {
    char name[MAXTEXTURENAME];  // Name of texture
    uint32_t width, height;     // Extends of the texture
    uint32_t offsets[MIPLEVELS]; // Offsets to texture mipmaps BSPMIPTEX;
};

//
// @TODO
// This is RGB but we uplaod to the GPU as BGRA. Could make the code nicer by
// expanding to 4 actual bytes per elem and swapping the first and last.
//

constexpr unsigned int palette[] = {
    0x000000,0x0f0f0f,0x1f1f1f,0x2f2f2f,0x3f3f3f,0x4b4b4b,0x5b5b5b,0x6b6b6b,
    0x7b7b7b,0x8b8b8b,0x9b9b9b,0xababab,0xbbbbbb,0xcbcbcb,0xdbdbdb,0xebebeb,
    0x0f0b07,0x170f0b,0x1f170b,0x271b0f,0x2f2313,0x372b17,0x3f2f17,0x4b371b,
    0x533b1b,0x5b431f,0x634b1f,0x6b531f,0x73571f,0x7b5f23,0x836723,0x8f6f23,
    0x0b0b0f,0x13131b,0x1b1b27,0x272733,0x2f2f3f,0x37374b,0x3f3f57,0x474767,
    0x4f4f73,0x5b5b7f,0x63638b,0x6b6b97,0x7373a3,0x7b7baf,0x8383bb,0x8b8bcb,
    0x000000,0x070700,0x0b0b00,0x131300,0x1b1b00,0x232300,0x2b2b07,0x2f2f07,
    0x373707,0x3f3f07,0x474707,0x4b4b0b,0x53530b,0x5b5b0b,0x63630b,0x6b6b0f,
    0x070000,0x0f0000,0x170000,0x1f0000,0x270000,0x2f0000,0x370000,0x3f0000,
    0x470000,0x4f0000,0x570000,0x5f0000,0x670000,0x6f0000,0x770000,0x7f0000,
    0x131300,0x1b1b00,0x232300,0x2f2b00,0x372f00,0x433700,0x4b3b07,0x574307,
    0x5f4707,0x6b4b0b,0x77530f,0x835713,0x8b5b13,0x975f1b,0xa3631f,0xaf6723,
    0x231307,0x2f170b,0x3b1f0f,0x4b2313,0x572b17,0x632f1f,0x733723,0x7f3b2b,
    0x8f4333,0x9f4f33,0xaf632f,0xbf772f,0xcf8f2b,0xdfab27,0xefcb1f,0xfff31b,
    0x0b0700,0x1b1300,0x2b230f,0x372b13,0x47331b,0x533723,0x633f2b,0x6f4733,
    0x7f533f,0x8b5f47,0x9b6b53,0xa77b5f,0xb7876b,0xc3937b,0xd3a38b,0xe3b397,
    0xab8ba3,0x9f7f97,0x937387,0x8b677b,0x7f5b6f,0x775363,0x6b4b57,0x5f3f4b,
    0x573743,0x4b2f37,0x43272f,0x371f23,0x2b171b,0x231313,0x170b0b,0x0f0707,
    0xbb739f,0xaf6b8f,0xa35f83,0x975777,0x8b4f6b,0x7f4b5f,0x734353,0x6b3b4b,
    0x5f333f,0x532b37,0x47232b,0x3b1f23,0x2f171b,0x231313,0x170b0b,0x0f0707,
    0xdbc3bb,0xcbb3a7,0xbfa39b,0xaf978b,0xa3877b,0x977b6f,0x876f5f,0x7b6353,
    0x6b5747,0x5f4b3b,0x533f33,0x433327,0x372b1f,0x271f17,0x1b130f,0x0f0b07,
    0x6f837b,0x677b6f,0x5f7367,0x576b5f,0x4f6357,0x475b4f,0x3f5347,0x374b3f,
    0x2f4337,0x2b3b2f,0x233327,0x1f2b1f,0x172317,0x0f1b13,0x0b130b,0x070b07,
    0xfff31b,0xefdf17,0xdbcb13,0xcbb70f,0xbba70f,0xab970b,0x9b8307,0x8b7307,
    0x7b6307,0x6b5300,0x5b4700,0x4b3700,0x3b2b00,0x2b1f00,0x1b0f00,0x0b0700,
    0x0000ff,0x0b0bef,0x1313df,0x1b1bcf,0x2323bf,0x2b2baf,0x2f2f9f,0x2f2f8f,
    0x2f2f7f,0x2f2f6f,0x2f2f5f,0x2b2b4f,0x23233f,0x1b1b2f,0x13131f,0x0b0b0f,
    0x2b0000,0x3b0000,0x4b0700,0x5f0700,0x6f0f00,0x7f1707,0x931f07,0xa3270b,
    0xb7330f,0xc34b1b,0xcf632b,0xdb7f3b,0xe3974f,0xe7ab5f,0xefbf77,0xf7d38b,
    0xa77b3b,0xb79b37,0xc7c337,0xe7e357,0x7fbfff,0xabe7ff,0xd7ffff,0x670000,
    0x8b0000,0xb30000,0xd70000,0xff0000,0xfff393,0xfff7c7,0xffffff,0x9f5b53
};

void Wad::loadWad(StrA path) {
    _files.emplace_back(MappedFile(path));
    auto& file = _files.back();
    auto buffer = file.buffer;
    auto head = (wadhead_t*)buffer;

    auto directories = (wadentry_t*)(buffer + head->diroffset);
    for (size_t i = 0; i < head->numentries; ++i) {
        auto dir = directories[i];
        if (dir.type == 'D') {
            assert(dir.cmprs == 0);
            const char* fileOffset = buffer + dir.offset;
            const auto pic = (wadmiptex_t*)(fileOffset);

            const auto pixels = (unsigned char*)(fileOffset + pic->offsets[0]);

            _pics[toUpper(dir.name)] = { pic->width, pic->height, pixels };
        }
    }
}

typedef struct                 // A Directory entry
{ long  offset;                // Offset to entry, in bytes, from start of file
  long  size;                  // Size of entry in file, in bytes
} dentry_t;

typedef struct                 // The BSP file header
{ long  version;               // Model version, must be 0x17 (23).
  dentry_t entities;           // List of Entities.
  dentry_t planes;             // Map Planes.
                               // numplanes = size/sizeof(plane_t)
  dentry_t miptex;             // Wall Textures.
  dentry_t vertices;           // Map Vertices.
                               // numvertices = size/sizeof(vertex_t)
  dentry_t visilist;           // Leaves Visibility lists.
  dentry_t nodes;              // BSP Nodes.
                               // numnodes = size/sizeof(node_t)
  dentry_t texinfo;            // Texture Info for faces.
                               // numtexinfo = size/sizeof(texinfo_t)
  dentry_t faces;              // Faces of each surface.
                               // numfaces = size/sizeof(face_t)
  dentry_t lightmaps;          // Wall Light Maps.
  dentry_t clipnodes;          // clip nodes, for Models.
                               // numclips = size/sizeof(clipnode_t)
  dentry_t leaves;             // BSP Leaves.
                               // numlaves = size/sizeof(leaf_t)
  dentry_t lface;              // List of Faces.
  dentry_t edges;              // Edges of faces.
                               // numedges = Size/sizeof(edge_t)
  dentry_t ledges;             // List of Edges.
  dentry_t models;             // List of Models.
                               // nummodels = Size/sizeof(model_t)
} dheader_t;

typedef struct                 // Mip texture list header
{ long numtex;                 // Number of textures in Mip Texture list
  long offset[1];              // Offset to each of the individual texture
} mipheader_t;                 //  from the beginning of mipheader_t

void Wad::loadBsp(StrA path) {
    _files.emplace_back(MappedFile(path));
    auto& file = _files.back();
    auto buffer = file.buffer;
    auto head = (dheader_t*)buffer;

    auto miphead = (mipheader_t*)(buffer + head->miptex.offset);
    for (size_t i = 0; i < miphead->numtex; ++i) {
        const char* fileOffset = buffer + head->miptex.offset + miphead->offset[i];
        auto pic = (wadmiptex_t*)fileOffset;
        const auto pixels = (unsigned char*)(fileOffset + pic->offsets[0]);
        _pics[toUpper(pic->name)] = { pic->width, pic->height, pixels };
    }
}

TextureInfo Wad::request(StrA name) {
    auto lit = _loaded.find(name);
    if (lit != _loaded.end()) return lit->second;

    auto it = _pics.find(name);
    if (it != _pics.end()) {
        auto& ret = _loaded[name];

        const auto& pic = it->second;
        ret.name = it->first;
        ret.width = pic.w;
        ret.height = pic.h;

        Vec<unsigned int> data;
        data.resize(ret.width * ret.height);

        for (size_t h = 0; h < ret.height; ++h) {
            for (size_t w = 0; w < ret.width; ++w) {
                const auto id = pic.loc[w + h * ret.width];
                const auto c = palette[id];

                const unsigned char r = c & 0xFF, g = (c & 0xFF00) >> 8, b = (c & 0xFF0000) >> 16;

                data[w + h * ret.width] = (b << 0) | (g << 8) | (r << 16) | (0xFF << 24);
            }
        }

        D3D11_TEXTURE2D_DESC desc = {};
        desc.Width = ret.width;
        desc.Height = ret.height;
        desc.Usage = D3D11_USAGE_DEFAULT;
        desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
        desc.ArraySize = 1;
        desc.MipLevels = 0;
        desc.SampleDesc.Count = 1;
        desc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET;
        desc.MiscFlags = D3D11_RESOURCE_MISC_GENERATE_MIPS;

        D3D11_SUBRESOURCE_DATA initial = { data.data(), UINT(ret.width * 4) };

        auto device = resourceManager->m_device;
        auto context = resourceManager->m_context;

        ComPtr<ID3D11Texture2D> tex;
        HRESULT hr = device->CreateTexture2D(&desc, nullptr, &tex);
        if (FAILED(hr)) unreachable("couldn't create texture");

        if (FAILED(device->CreateShaderResourceView(tex.Get(), nullptr, &ret.srv))) unreachable("couldn't create texture view");

        context->UpdateSubresource(tex.Get(), 0, nullptr, data.data(), UINT(ret.width * 4), 0);
        context->GenerateMips(ret.srv.Get());

        TextureInfo textureInfo = { name, ret.srv, ret.width, ret.height };
        resourceManager->_textures[name] = textureInfo;

        return ret;
    }

    return resourceManager->defaultTexture();
}
