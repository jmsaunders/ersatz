#pragma once

#include "viewport.h"
#include "d3d.h"
#include "immediate_mode.h"
#include "editor_state.h"

struct OrthographicViewport : EditorViewport {
    // The type of view this is
    enum Type {
        TOP_VIEW,
        SIDE_VIEW,
        FRONT_VIEW
    } _type;

    OrthographicViewport(EditorWorld* world, ComPtrA<ID3D11Device> device, ComPtrA<ID3D11DeviceContext> context, Type = TOP_VIEW);

    Str name() {
        switch (_type) {
        case TOP_VIEW:
            return "Top view";
        case SIDE_VIEW:
            return "Side view";
        case FRONT_VIEW:
            return "Front view";
        }

        unreachable("Bad viewport type");
    }

protected:

    void mouseUp(int button, int x, int y) override {}
    void mouseDown(int button, int x, int y) override {}
    void mouseWheel(bool up, int x, int y) override;
    void mouseDrag(int dx, int dy) override;

    void drawInternal() override;

private:
    float3x3 _basis;
    EditorWorld* _world;

    float4x4 viewMatrix() const;

    float3 _center;
    float _scale = 2.f;
    float _maxGrid = 4096; // ??
    // Small step gets light lines, big step gets big lines
    float _smallStep = 8, _bigStep = 64;

    ImmediateManager _im, _worldMeshes;

    float4 screenToWorld(const float2& screen);
    float4 screenToWorld(int x, int y);
};
