#include "common.h"
#include "map_render.h"
#include "editor_state.h"

constexpr size_t k_vertexBufferSize = 2 * 1024 * 1024;
constexpr size_t k_indexBufferSize = 2 * 1024 * 1024;

using VertexBlock = BufferAllocator<CsgVertex>::Block*;
class VertexBufferAllocator {
    Vec<CsgVertex> m_buffer;
    BufferAllocator<CsgVertex> m_allocator;

public:
    VertexBufferAllocator(size_t capacity) : m_allocator(capacity) {
        m_buffer.resize(capacity);
    }

    VertexBlock alloc(VecA<CsgVertex> verts) {
        auto block = m_allocator.alloc(verts.size());
        assert(block);

        memcpy(m_buffer.data() + block->offset, verts.data(), sizeof(CsgVertex) * verts.size());

        return block;
    }

    void free(VertexBlock block) {
        m_allocator.free(block);
    }

    void upload(const ComPtr<ID3D11DeviceContext>& context, const ComPtr<ID3D11Buffer>& buffer) {
        D3D11_MAPPED_SUBRESOURCE res;
        if (FAILED(context->Map(buffer.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &res))) unreachable("map failed?");
        memcpy(res.pData, m_buffer.data(), highwater() * sizeof(CsgVertex));
        context->Unmap(buffer.Get(), 0);
    }

    size_t highwater() const {
        return m_allocator.highwater();
    }

    bool empty() const {
        return m_allocator.empty();
    }
};

constexpr D3D11_INPUT_ELEMENT_DESC k_vertexLayoutDesc[] = {
    { "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, offsetof(CsgVertex, pos), D3D11_INPUT_PER_VERTEX_DATA, 0 },
    { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, offsetof(CsgVertex, uv), D3D11_INPUT_PER_VERTEX_DATA, 0 },
};

static constexpr size_t k_defaultWireframeIndexCount = 1024 * 1024;

MapRenderer::MapRenderer(const ComPtr<ID3D11Device>& dev, const ComPtr<ID3D11DeviceContext>& dc, EditorWorld* map)
    : _device(dev), _context(dc), _world(map),
    _vertexBufferAllocator(new VertexBufferAllocator(k_vertexBufferSize)),
    _indexSectionAllocator(k_indexBufferSize),
    _wireframeIndexAllocator(_indexSectionAllocator.alloc(k_defaultWireframeIndexCount)) {

    _lineShader = resourceManager->requestShader("lines.hlsl", {}, true);
    _filledShader = resourceManager->requestShader("base.hlsl", { "HAS_TEXCOORD0" });
    if (FAILED(dev->CreateInputLayout(k_vertexLayoutDesc, _countof(k_vertexLayoutDesc), _filledShader->vsblob->GetBufferPointer(), _filledShader->vsblob->GetBufferSize(), &_inputLayout)))
        unreachable("failed to create input layout");

    D3D11_SAMPLER_DESC samplerDesc = {};
    samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT; //D3D11_FILTER_ANISOTROPIC;
    samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
    samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
    samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
    samplerDesc.MinLOD = 0.f;
    samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;
    samplerDesc.MaxAnisotropy = 8;
    samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;

    if (FAILED(dev->CreateSamplerState(&samplerDesc, &_samplerState)))
        unreachable("failed to create sampler state");

    {
        D3D11_RASTERIZER_DESC rasterizerDesc = {};
        rasterizerDesc.CullMode = D3D11_CULL_NONE;
        rasterizerDesc.FillMode = D3D11_FILL_SOLID;
        rasterizerDesc.DepthBias = true;
        rasterizerDesc.DepthBias = -100;
        rasterizerDesc.MultisampleEnable = true;

        dev->CreateRasterizerState(&rasterizerDesc, &_lineRasterizerState);
    }

    {
        D3D11_BUFFER_DESC desc = {};
        desc.Usage = D3D11_USAGE_DYNAMIC;
        desc.ByteWidth = UINT(k_vertexBufferSize * sizeof(CsgVertex));
        desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
        desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

        if (FAILED(dev->CreateBuffer(&desc, nullptr, &_vertexBuffer))) unreachable("could not create vertex buffer");
    }

    {
        D3D11_BUFFER_DESC desc = {};
        desc.Usage = D3D11_USAGE_DEFAULT;
        desc.ByteWidth = UINT(k_indexBufferSize * sizeof(int));
        desc.BindFlags = D3D11_BIND_INDEX_BUFFER;

        if (FAILED(dev->CreateBuffer(&desc, nullptr, &_indexBuffer))) unreachable("could not create vertex buffer");
    }

    {
        D3D11_BUFFER_DESC desc = {};
        desc.ByteWidth = sizeof(ConstantBuffer);
        desc.Usage = D3D11_USAGE_DYNAMIC;
        desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
        desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

        if (FAILED(_device->CreateBuffer(&desc, nullptr, &_primaryConstantBuffer))) unreachable("failed to create the shader constant buffer");
    }

    {
        D3D11_BUFFER_DESC desc = {};
        desc.ByteWidth = 16; // Can't be sizeof(float) because that's too small.
        desc.Usage = D3D11_USAGE_DYNAMIC;
        desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
        desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

        if (FAILED(_device->CreateBuffer(&desc, nullptr, &_linesConstantBuffer))) unreachable("failed to create the shader constant buffer");
    }

    updateVertexBuffer();
}

MapRenderer::~MapRenderer() {
}

void MapRenderer::updateBuffersForBrush(RefA<Brush> brush) {
    const auto& brushFaces = brush->finalFaces;

    if (brushFaces.empty()) return;

    const bool brushSelected = lookup(_world->selectedBrushes, brush);

    auto& brushState = _brushState[brush];

    // @TODO
    // Much of the update time is re-sizing these buffers. We may need better ways of figuring out
    // how to fixed-size them, or to use LLVM's SmallVector?
    Vec<CsgVertex> verts;
    Map<TexId, Vec<int>> unselectedIndices, selectedIndices;
    Vec<int> wireframeIndices;

    for (const auto& face : brushFaces) {
        const auto tex = face.texture;

        const auto startVerts = int(verts.size());
        verts.insert(end(verts), begin(face.verts), end(face.verts));

        const bool faceSelected = _world->selectedFaces.find({ brush, face.planeNo }) != _world->selectedFaces.end();

        auto& ourIndices = brushSelected || faceSelected ? selectedIndices[tex.srv.Get()] : unselectedIndices[tex.srv.Get()];
        for (int i = 0; i < int(face.verts.size()); ++i) {
            if (i & 1) {
                ourIndices.push_back(startVerts + i / 2);
            } else {
                ourIndices.push_back(startVerts + int(face.verts.size() - i / 2 - 1));
            }
        }
        ourIndices.push_back(-1);

        for (int i = 0; i < int(face.verts.size()); ++i)
            wireframeIndices.push_back(startVerts + i);
        wireframeIndices.push_back(startVerts);
        wireframeIndices.push_back(-1);
    }

    if (verts.empty()) return;

    // This can be made better by only allocating the index information at the very end when we have a good idea of how large we need them to be.
    // This is sufficient for now though.

    auto vertexBlock = _vertexBufferAllocator->alloc(verts);
    brushState.vertexBlocks.push_back(vertexBlock);

    {
        for (int& index : wireframeIndices) {
            if (index != -1) index += int(vertexBlock->offset);
        }

        brushState.lineBlock = SubIndexAllocation(_wireframeIndexAllocator->alloc(wireframeIndices));
    }

    auto f = [&](auto tex, auto& indicesForFace, bool selected) {
        for (auto& index : indicesForFace) {
            if (index != -1)
                index += int(vertexBlock->offset);
        }

        auto& indexSectionAllocator = selected ? _selectedState[tex] : _passState[tex];
        if (!indexSectionAllocator)
            indexSectionAllocator = _indexSectionAllocator.alloc(std::max(indicesForFace.size(), size_t(500)));

        auto indexBlock = indexSectionAllocator->alloc(indicesForFace);
        brushState.indexBlocks.emplace_back(indexBlock);
    };

    for (auto& [tex, indicesForFace] : selectedIndices)
        f(tex, indicesForFace, true);
    for (auto& [tex, indicesForFace] : unselectedIndices)
        f(tex, indicesForFace, false);
}

void MapRenderer::clearPriorBrushState(BrushState& bs) {
    bs.indexBlocks.clear();
    bs.lineBlock = none;

    for (auto& vb : bs.vertexBlocks)
        _vertexBufferAllocator->free(vb);
    bs.vertexBlocks.clear();
}

void MapRenderer::updateVertexBuffer(Maybe<Set<Ref<Brush>>> dirtyBrushes) {
    Timer timer;

    if (dirtyBrushes) {
        Timer timer;
        for (auto brush : *dirtyBrushes) {
            clearPriorBrushState(_brushState[brush]);
        }

        for (auto dirty : *dirtyBrushes)
            updateBuffersForBrush(dirty);

        OutputDebugStringA(format("Brush update took %fms\n", timer.elapsedMs()).c_str());
    } else {
        Timer timer;
        for (auto& [brush, data] : _brushState)
            clearPriorBrushState(data);

        for (auto brush : _world->brushes)
            updateBuffersForBrush(brush);

        for (auto e : _world->entities) {
            // @TODO
            // We need a better way of visualizing triggers with transparency this sucks
            if (dyn_cast<TriggerEntity>(e)) continue;

            if (auto be = dyn_cast<BrushEntity>(e)) {
                for (auto brush : be->brushes)
                    updateBuffersForBrush(brush);
            }
        }
        OutputDebugStringA(format("Brush update took %fms\n", timer.elapsedMs()).c_str());
    }

    {
        Timer timer;
        _vertexBufferAllocator->upload(_context, _vertexBuffer);
        OutputDebugStringA(format("VB update took %fms\n", timer.elapsedMs()).c_str());
    }

    {
        Timer timer;

        for (auto& [tex, subIndices] : _passState)
            subIndices->upload(_context, _indexBuffer);
        for (auto& [tex, subIndices] : _selectedState)
            subIndices->upload(_context, _indexBuffer);

        _wireframeIndexAllocator->upload(_context, _indexBuffer);

        OutputDebugStringA(format("IB update took %fms\n", timer.elapsedMs()).c_str());
    }

    OutputDebugStringA(format("Total book-keeping: %fms\n", timer.elapsedMs()).c_str());
    OutputDebugStringA(format("Highwater on VB: %llu\n", _vertexBufferAllocator->highwater()).c_str());
    OutputDebugStringA(format("Highwater on IB: %llu\n", _indexSectionAllocator.highwater()).c_str());
}

void MapRenderer::draw(const ComPtr<ID3D11DeviceContext>& dc, const float4x4& mvp, float2 viewport, bool lines, bool filled) {
    const UINT stride = sizeof(CsgVertex), offset = 0;
    dc->IASetVertexBuffers(0, 1, _vertexBuffer.GetAddressOf(), &stride, &offset);
    dc->IASetIndexBuffer(_indexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);
    dc->IASetInputLayout(_inputLayout.Get());

    {
        D3D11_MAPPED_SUBRESOURCE res;
        if (FAILED(_context->Map(_primaryConstantBuffer.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &res))) unreachable("couldn't map constant buffer");
        *(ConstantBuffer*)(res.pData) = { mvp, float4(1.f, 1.f, 1.f, 1.f), float4(viewport, 0, 1) };
        _context->Unmap(_primaryConstantBuffer.Get(), 0);
    }

    if (lines) {
        _lineShader->bind(_context);
        _context->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_LINESTRIP);

        {
            D3D11_MAPPED_SUBRESOURCE res;
            if (FAILED(_context->Map(_linesConstantBuffer.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &res))) unreachable("couldn't map constant buffer");
            *(float*)(res.pData) = 2.5f;
            _context->Unmap(_linesConstantBuffer.Get(), 0);
        }

        ID3D11Buffer* constantBuffers[] = { _primaryConstantBuffer.Get(), _linesConstantBuffer.Get() };
        _context->VSSetConstantBuffers(0, 2, constantBuffers);
        _context->GSSetConstantBuffers(0, 2, constantBuffers);
        _context->PSSetConstantBuffers(0, 2, constantBuffers);

        ComPtr<ID3D11RasterizerState> oldRasterState;
        _context->RSGetState(oldRasterState.GetAddressOf());
        _context->RSSetState(_lineRasterizerState.Get());
        _context->DrawIndexed(UINT(_wireframeIndexAllocator->highwater()), UINT(_wireframeIndexAllocator->offset()), 0);
        _context->RSSetState(oldRasterState.Get());
    }

    if (filled) {
        _filledShader->bind(dc);
        dc->VSSetConstantBuffers(0, 1, _primaryConstantBuffer.GetAddressOf());
        dc->PSSetConstantBuffers(0, 1, _primaryConstantBuffer.GetAddressOf());
        dc->GSSetConstantBuffers(0, 1, _primaryConstantBuffer.GetAddressOf());
        dc->PSSetSamplers(0, 1, _samplerState.GetAddressOf());
        dc->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

        for (auto ps : _passState) {
            dc->PSSetShaderResources(0, 1, &ps.first);
            dc->DrawIndexed(UINT(ps.second->highwater()), UINT(ps.second->offset()), 0);
        }

        // Draw selected stuff:
        const float4 selectedColour(0.85f, 0.2f, 0.2f, 1.f);
        {
            D3D11_MAPPED_SUBRESOURCE res;
            if (FAILED(_context->Map(_primaryConstantBuffer.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &res))) unreachable("couldn't map constant buffer");
            *(ConstantBuffer*)(res.pData) = { mvp, selectedColour, float4(viewport, 0, 1) };
            _context->Unmap(_primaryConstantBuffer.Get(), 0);
        }

        for (auto ps : _selectedState) {
            auto count = UINT(ps.second->highwater());
            if (count > 0) {
                dc->PSSetShaderResources(0, 1, &ps.first);
                dc->DrawIndexed(count, UINT(ps.second->offset()), 0);
            }
        }
    }
}
