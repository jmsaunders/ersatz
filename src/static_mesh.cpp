#include "common.h"
#include "d3d.h"

#define TINYGLTF_NO_STB_IMAGE_WRITE
#include "external/tiny_gltf.h"

static DXGI_FORMAT decodeFormat(const tinygltf::Accessor& accessor) {
    switch (accessor.componentType) {
    case TINYGLTF_COMPONENT_TYPE_FLOAT: {
        switch (accessor.type) {
        case TINYGLTF_TYPE_VEC2: return DXGI_FORMAT_R32G32_FLOAT;
        case TINYGLTF_TYPE_VEC3: return DXGI_FORMAT_R32G32B32_FLOAT;
        case TINYGLTF_TYPE_VEC4: return DXGI_FORMAT_R32G32B32A32_FLOAT;
        default: unreachable("Bad accessor type");
        }
    }

    case TINYGLTF_COMPONENT_TYPE_UNSIGNED_BYTE:
    case TINYGLTF_COMPONENT_TYPE_BYTE:
        assert(accessor.type == TINYGLTF_TYPE_SCALAR);
        return DXGI_FORMAT_R8_SINT;

    case TINYGLTF_COMPONENT_TYPE_UNSIGNED_SHORT: {
        switch (accessor.type) {
        case TINYGLTF_TYPE_SCALAR: return DXGI_FORMAT_R16_UINT;
        case TINYGLTF_TYPE_VEC4:   return DXGI_FORMAT_R16G16B16A16_UINT;
        default: unreachable("Bad accessor type");
        }
    }

    case TINYGLTF_COMPONENT_TYPE_UNSIGNED_INT:
        assert(accessor.type == TINYGLTF_TYPE_SCALAR);
        return DXGI_FORMAT_R32_UINT;

    default: unreachable("Bad component type");
    }
}

Maybe<StaticMesh> loadMesh(StrA path, ID3D11Device* dev, ID3D11DeviceContext* context) {
    auto bytes = readFile(path);
    if (!bytes) return none;

    auto tinygltf = tinygltf::TinyGLTF{};

    tinygltf::Model model;
    Str err, warn;

    if (hasSuffix(path, ".gltf")) {
        if (!tinygltf.LoadASCIIFromString(&model, &err, &warn, &bytes->front(), unsigned(bytes->size()), "res"))
            return none;
    } else if (hasSuffix(path, ".glb")) {
        if (!tinygltf.LoadBinaryFromMemory(&model, &err, &warn, (const unsigned char*) &bytes->front(), unsigned(bytes->size()), "res"))
            return none;
    }

    Vec<ComPtr<ID3D11Buffer>> modelBuffers;
    for (const auto& buffer : model.buffers) {
        D3D11_BUFFER_DESC desc = {};
        desc.ByteWidth = UINT(buffer.data.size());
        desc.BindFlags = D3D11_BIND_VERTEX_BUFFER | D3D11_BIND_INDEX_BUFFER;

        D3D11_SUBRESOURCE_DATA initial = { (void*) &buffer.data.front() };
        ComPtr<ID3D11Buffer> vb;
        if (FAILED(dev->CreateBuffer(&desc, &initial, &vb))) unreachable("could not create vertex buffer");
        modelBuffers.push_back(vb);
    }

    // Create textures for the boys:
    Vec<ID3D11Texture2D*> images;
    for (const auto& img : model.images) {
        D3D11_TEXTURE2D_DESC desc = {};
        desc.Width = img.width;
        desc.Height = img.height;
        desc.Usage = D3D11_USAGE_DYNAMIC;
        desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
        desc.MipLevels = desc.ArraySize = 1;
        desc.SampleDesc.Count = 1;
        desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
        desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

        // TODO this is hardcoded to 4 bytes per pixel?
        D3D11_SUBRESOURCE_DATA initial = { &img.image[0], UINT(img.width * 4) };

        ID3D11Texture2D* tex = nullptr;
        HRESULT hr = dev->CreateTexture2D(&desc, &initial, &tex);
        if (FAILED(hr)) unreachable("couldn't create texture");

        images.push_back(tex);
    }

    StaticMesh ret;
    for (const auto& mesh : model.meshes) {
        for (const auto& prim : mesh.primitives) {
            Vec<D3D11_INPUT_ELEMENT_DESC> descs;

            // This is the set of attributes we can receive in the world shader:
            Set<Str> shaderDefines;

            if (prim.mode != TINYGLTF_MODE_TRIANGLES) unreachable("please only give us triangles");

            Vec<ComPtr<ID3D11Buffer>> vbs;
            Vec<UINT> strides, offsets;
            UINT attrNum = 0;
            for (const auto& [attrName, accessorId] : prim.attributes) {
                const auto& accessor = model.accessors[accessorId];
                const auto& bufferView = model.bufferViews[accessor.bufferView];

                D3D11_INPUT_ELEMENT_DESC desc = {};

                // If we have an underscore, we treat this as a numbered thing:
                auto underscoreIt = attrName.rfind('_');
                if (underscoreIt != Str::npos && underscoreIt != 0) {
                    static const Set<Str> allowed = { "TEXCOORD", "WEIGHTS", "JOINTS" };

                    auto semanticName = attrName.substr(0, underscoreIt);
                    auto it = allowed.find(semanticName);
                    if (it != allowed.end())
                        desc.SemanticName = it->c_str();
                    else
                        unreachable(SSS("Invalid numbered accessor type " << attrName).c_str());

                    if (auto i = decode<int>(attrName.c_str() + underscoreIt + 1))
                        desc.SemanticIndex = *i;
                    else
                        unreachable(SSS("Cannot identify the number part of " << attrName).c_str());
                } else {
                    desc.SemanticName = attrName.c_str();
                }

                if (attrName == "NORMAL")
                    shaderDefines.insert("HAS_NORMALS");
                if (attrName == "TEXCOORD_0")
                    shaderDefines.insert("HAS_TEXCOORD0");

                desc.InputSlot = attrNum++;
                desc.Format = decodeFormat(accessor);
                desc.AlignedByteOffset = UINT(accessor.byteOffset);
                desc.InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;

                vbs.push_back(modelBuffers[bufferView.buffer]);
                strides.push_back(UINT(accessor.ByteStride(bufferView)));
                offsets.push_back(UINT(bufferView.byteOffset));

                descs.push_back(desc);
            }

            const auto& material = model.materials[prim.material];

            ComPtr<ID3D11ShaderResourceView> texture;

            // Do we have a texture?
            if (material.pbrMetallicRoughness.baseColorTexture.index != -1) {
                // Create texture view
                D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
                srvDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
                srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
                srvDesc.Texture2D.MipLevels = 1;
                srvDesc.Texture2D.MostDetailedMip = 0;

                if (FAILED(dev->CreateShaderResourceView(images[material.pbrMetallicRoughness.baseColorTexture.index], &srvDesc, &texture))) unreachable("couldn't create texture view");
            }

            // Create the index buffer now:
            const auto& indexAccessor = model.accessors[prim.indices];
            const auto& indexBufferView = model.bufferViews[indexAccessor.bufferView];
            auto indexBufferOffset = UINT(indexAccessor.byteOffset + indexBufferView.byteOffset);
            ComPtr<ID3D11Buffer> ib;
            {
                const auto& buffer = model.buffers[indexBufferView.buffer];
                ib = modelBuffers[indexBufferView.buffer];
            }

            auto shader = resourceManager->requestShader("base.hlsl", shaderDefines);

            ComPtr<ID3D11InputLayout> inputLayout;
            auto hr = dev->CreateInputLayout(&descs.front(), UINT(descs.size()), shader->vsblob->GetBufferPointer(), shader->vsblob->GetBufferSize(), &inputLayout);
            if (FAILED(hr)) unreachable("failed to create layout");

            DrawBatch batch = { ib, decodeFormat(indexAccessor), indexBufferOffset, UINT(indexAccessor.count), inputLayout, shader, texture, vbs, strides, offsets };
            ret.batches.push_back(batch);
        }
    }

    return ret;
}

void StaticMesh::issueDraws(ID3D11DeviceContext* ctx) {
    ctx->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

    for (const auto& batch : batches) {
        if (batch.texture) ctx->PSSetShaderResources(0, 1, batch.texture.GetAddressOf());

        batch.shader->bind(ctx);
        ctx->IASetInputLayout(batch.inputLayout.Get());
        ctx->IASetVertexBuffers(0, UINT(batch.vbs.size()), batch.vbs[0].GetAddressOf(), &batch.strides[0], &batch.offsets[0]);
        ctx->IASetIndexBuffer(batch.indexBuffer.Get(), batch.indexFormat, batch.indexBufferOffset);

        ctx->DrawIndexed(batch.indexCount, 0, 0);
    }
}
