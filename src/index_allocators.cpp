#include "common.h"
#include "index_allocators.h"

#include <d3d11.h>

SubIndexAllocator* IndexBufferSectionAllocator::alloc(size_t size) {
    auto block = m_allocator.alloc(size);
    assert(block);
    return new SubIndexAllocator(size, this, block);
}

bool IndexBufferSectionAllocator::realloc(SubIndexAllocator* block, size_t size) {
    bool moved;
    auto newBlock = m_allocator.realloc(block->m_block, size, &moved);
    assert(newBlock);

    block->m_block = newBlock;
    block->m_allocator.expand(size);

    return moved;
}

void IndexBufferSectionAllocator::free(SubIndexAllocator* block) {
    m_allocator.free(block->m_block);
}

//
// Sub index:
//

SubIndexAllocator::SubIndexAllocator(size_t size, IndexBufferSectionAllocator* parentAllocator, BufferAllocator<int>::Block* block)
    : m_allocator(size), m_parentAllocator(parentAllocator), m_block(block) {
    m_buffer.resize(size);
}

SubIndexAllocationBlock* SubIndexAllocator::alloc(VecA<int> indices) {
    const auto length = indices.size();
    auto block = m_allocator.alloc(length);

    bool relocated = false;

    // If we can't service this request, we need to expand our buffer inside the parent and re-copy everything:
    if (!block) {
        // TODO: Do we want to use the standard exponential growth strategy?
        const auto newSize = (m_block->size + length) * 2;

        relocated = m_parentAllocator->realloc(this, newSize);
        m_buffer.resize(newSize);

        block = m_allocator.alloc(length);
        assert(block);
    }

    memcpy(m_buffer.data() + block->offset, indices.data(), length * sizeof(int));

    m_dirty = true;
    return new SubIndexAllocationBlock{ this, block };
}

void SubIndexAllocator::upload(ComPtr<ID3D11DeviceContext> context, ComPtr<ID3D11Buffer> indexBuffer) {
    if (!m_dirty || empty()) return;
    m_dirty = false;

    auto offset = this->offset();
    D3D11_BOX box = {
        UINT(offset * sizeof(int)), 0, 0,
        UINT((offset + highwater()) * sizeof(int)), 1, 1
    };
    context->UpdateSubresource(indexBuffer.Get(), 0, &box, m_buffer.data(), box.right - box.left, 0);
}

void SubIndexAllocator::free(SubIndexAllocationBlock* s) {
    // Clear the range it was in, then re-upload it to the GPU:
    memset(m_buffer.data() + s->block->offset, 0, s->block->size * sizeof(int));
    m_buffer[s->block->offset + s->block->size - 1] = -1;

    m_allocator.free(s->block);
    delete s;

    m_dirty = true;
}

SubIndexAllocation::~SubIndexAllocation() {
    if (block) block->parentAllocator->free(block);
}
