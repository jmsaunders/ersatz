#pragma once

// Allocates blocks within a range

template<typename T>
struct BufferAllocator {
    static_assert(std::is_standard_layout<T>::value, "Buffer contents must be standard layout");

    struct Block;
    using FreeBlockMap = std::map<Pair<size_t, size_t>, Block*>;
    struct Block {
        Block(size_t offset, size_t size) : offset(offset), size(size) {}

        const size_t offset;
        size_t size;

        // Internal book-keeping:
    private:
        friend BufferAllocator;
        Maybe<typename FreeBlockMap::iterator> freeBlockIt = none;
        typename std::map<size_t, Block*>::iterator allBlockIt;
    };

    BufferAllocator(size_t initialSize) : m_size(initialSize) {
        allocateFreeBlock(0, initialSize);
    }

    Block* alloc(size_t elemCount) {
        assert(elemCount > 0);

        auto it = m_freeBlocks.lower_bound(std::make_pair(elemCount, 0));
        if (it == end(m_freeBlocks)) return nullptr;

        Block* ret = it->second;
        assert(ret->freeBlockIt && it == ret->freeBlockIt);
        m_freeBlocks.erase(it);

        auto remainder = ret->size - elemCount;
        if (remainder) {
            allocateFreeBlock(ret->offset + elemCount, remainder);
        }

        ret->size = elemCount;
        ret->freeBlockIt = none;

        validate();
        return ret;
    }

    Block* realloc(Block* block, size_t elemCount, bool* moved = nullptr) {
        assert(elemCount > 0);
        assert(!block->freeBlockIt);

        auto ourIt = block->allBlockIt;
        assert(ourIt != m_allBlocks.end());

        auto next = std::next(ourIt);

        // Can we merge with the next one to expand to the required count?
        if (next != m_allBlocks.end() && next->second->size + block->size >= elemCount && next->second->freeBlockIt) {
            if (moved) *moved = false;

            // We need to re-anchor the position of this block since part of it was consumed. To do this, we need
            // to completely re-create the block.
            auto consumedBlock = next->second;
            const auto leftOver = consumedBlock->size + block->size - elemCount;

            m_freeBlocks.erase(*consumedBlock->freeBlockIt);
            m_allBlocks.erase(consumedBlock->allBlockIt);
            delete consumedBlock;

            if (leftOver > 0) {
                // Re-create it if we have any space left:
                allocateFreeBlock(block->offset + elemCount, leftOver);
            }

            block->size = elemCount;
            validate();
            return block;
        }

        if (moved) *moved = true;
        free(block);
        return alloc(elemCount);
    }

    void free(Block* block) {
        assert(!block->freeBlockIt);

        auto ourIt = block->allBlockIt;
        assert(ourIt != m_allBlocks.end());

        auto next = std::next(ourIt);

        Block* remainingBlock = block;

        if (ourIt != m_allBlocks.begin()) {
            auto prevBlock = std::prev(ourIt)->second;
            if (prevBlock->freeBlockIt) {
                remainingBlock = mergeFreeBlocks(prevBlock, ourIt->second);
            }
        }

        if (next != m_allBlocks.end() && next->second->freeBlockIt) {
            remainingBlock = mergeFreeBlocks(remainingBlock, next->second);
        }

        if (remainingBlock->freeBlockIt)
            m_freeBlocks.erase(*remainingBlock->freeBlockIt);

        addToFreeList(remainingBlock);

        validate();
    }

    bool empty() const {
        return m_allBlocks.size() == 1 && m_freeBlocks.size() == 1;
    }

    size_t size() const {
        return m_size;
    }

    // This is the size of the allocated subsection of the buffer.
    size_t highwater() const {
        auto last = std::prev(m_allBlocks.end());
        if (last->second->freeBlockIt) {
            if (last == m_allBlocks.begin()) return 0;
            last = std::prev(last);
        }
        assert(!last->second->freeBlockIt);
        return last->second->offset + last->second->size;
    }

    void expand(size_t newSize) {
        assert(newSize > m_size);
        auto lastBlock = m_allBlocks.rbegin()->second;
        if (lastBlock->freeBlockIt) {
            lastBlock->size += newSize - m_size;
            reseatFreeBlock(lastBlock);
        } else {
            allocateFreeBlock(m_size, newSize - m_size);
        }
        m_size = newSize;
        validate();
    }

private:

//#define DO_VALIDATION

    void validate() {
#ifdef DO_VALIDATION
        for (auto it = begin(m_allBlocks); it != end(m_allBlocks); ++it) {
            auto ourBlock = it->second;
            if (next(it) != end(m_allBlocks)) {
                auto nextBlock = next(it)->second;
                assert(nextBlock->offset == ourBlock->offset + ourBlock->size);
            }

            auto freeIt = m_freeBlocks.find({ ourBlock->size, ourBlock->offset });
            bool inFree = freeIt != m_freeBlocks.end();
            assert(inFree == bool(ourBlock->freeBlockIt));
        }

        for (auto it = begin(m_freeBlocks); it != end(m_freeBlocks); ++it) {
            auto block = it->second;
            auto mainIt = m_allBlocks.find(block->offset);
            assert(mainIt != m_allBlocks.end());
            assert(mainIt->second == block);
        }
#endif
    }

    void reseatFreeBlock(Block* block) {
        assert(block->freeBlockIt);
        m_freeBlocks.erase(*block->freeBlockIt);
        addToFreeList(block);
    }

    // Merge rhs into lhs and validate:
    Block* mergeFreeBlocks(Block* lhs, Block* rhs) {
        assert(lhs->offset + lhs->size == rhs->offset);

        lhs->size += rhs->size;

        if (rhs->freeBlockIt) m_freeBlocks.erase(*rhs->freeBlockIt);
        m_allBlocks.erase(rhs->allBlockIt);

        delete rhs;
        return lhs;
    }

    void addToFreeList(Block* block) {
        bool added;
        std::tie(block->freeBlockIt, added) = m_freeBlocks.insert({ { block->size, block->offset }, block });
        assert(added);
    }

    void allocateFreeBlock(size_t offset, size_t length) {
        auto block = new Block(offset, length);
        addToFreeList(block);

        bool added;
        std::tie(block->allBlockIt, added) = m_allBlocks.insert({ offset, block });
        assert(added);
    }

    // Total size of the pool:
    size_t m_size;

    // Ordered by size, then offset:
    FreeBlockMap m_freeBlocks;

    // Ordered by offset:
    std::map<size_t, Block*> m_allBlocks;
};

