#pragma once

#include "math_common.h"
#include "bvh_tree.h"
#include "d3d.h"

struct Brush;
struct BrushEntity;

struct CsgVertex {
    float4 pos;
    float2 uv;
};

struct CsgFace {
    TextureInfo texture;
    Vec<CsgVertex> verts;

    Ref<Brush> brush;
    int planeNo;
};

struct Face {
    Vec<float4> verts;

    Aabb computeBounds() const {
        if (verts.empty()) return Aabb();
        Aabb ret(verts[0]);
        for (size_t i = 1; i < verts.size(); ++i) ret.expand(verts[i]);
        return ret;
    }
};

struct Mesh {
    Vec<Face> faces;

    Aabb computeBounds() const {
        if (faces.empty()) return Aabb();

        Aabb ret = faces[0].computeBounds();
        for (size_t i = 1; i < faces.size(); ++i) ret.expand(faces[i].computeBounds());
        return ret;
    }
};

enum class BrushType { SUBTRACTIVE, ADDITIVE };

struct Brush {
    struct FaceTextureInfo {
        TextureInfo tex;

        // World space -> uv space
        float4x2 uvspace;
    };

    // Properties of the brush:
    Vec<Plane> planes;
    Vec<FaceTextureInfo> textureInfo;
    BrushEntity* parent = nullptr;
    BrushType type = BrushType::ADDITIVE;

    // Cache to make stuff faster:
    Aabb bounds;
    size_t index = -1;
    Mesh baseMesh;
    Vec<CsgFace> finalFaces;

    bool regenerateBase(bool assignOnFailure = true);
    bool pointLiesOnBoundary(const float3& pt, float epsilon = defaultEpsilon) const;
    void flip();
};

float4x2 generateTextureAxes(float3 normal, float rot, float2 scale, float2 offs, float2 texdim);

using BvhNode = BvhTree<Ref<Brush>>::Node;
void processFace(Ref<Brush> brush, VecA<Ref<Brush>> brushes, size_t faceNo, BvhNode* tree);

Ref<Brush> createBrush(const float3& min, const float3& max);
void generateFinalFaces(Ref<Brush> brush, VecA<Ref<Brush>> brushes, const BvhTree<Ref<Brush>>& bvhTree);
void generateCsgGeometry(VecA<Ref<Brush>> brushes, const BvhTree<Ref<Brush>>& bvhTree, const Maybe<Set<Ref<Brush>>>& dirtyBrushes = none);
