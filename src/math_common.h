#pragma once

#include <algorithm>    // min/max
#include <cmath>        // fabs
#include "external/linalg.h"

using namespace linalg;
using namespace linalg::aliases;

using std::min;
using std::max;

// Constants

static const float defaultEpsilon = 0.001f;
static const float pi = 3.14159265358979323846f;
static const float twoPi = 6.28318530717958647692f;

// Math support inlined functions

template<typename T>
inline T sign(T a) {
    if(a >= 0) return 1;
    return -1;
}

template<typename T>
T toRad(T a) {
    return a * (pi / 180);
}

template<typename T>
T toDegrees(T a) {
    return a / (pi / 180);
}

inline bool isZero(float num, float e=defaultEpsilon) {
    return fabs(num) < e;
}

inline bool equals(float a, float b, float e=defaultEpsilon) {
    return isZero(a - b, e);
}

template<typename T>
void clamp(T val, T low, T high) {
    assert(low <= high);
    if(val < low) val = low;
    if(val > high) val = high;
    return val;
}

inline float4x4 xRotation(float angle) {
    auto c = cos(angle), s = sin(angle);
    return {
        { 1, 0, 0, 0 },
        { 0, c,-s, 0 },
        { 0, s, c, 0 },
        { 0, 0, 0, 1 } };
}

inline float4x4 yRotation(float angle) {
    auto c = cos(angle), s = sin(angle);
    return {
        { c, 0, s, 0 },
        { 0, 1, 0, 0 },
        {-s, 0, c, 0 },
        { 0, 0, 0, 1 } };
}

inline float4x4 zRotation(float angle) {
    auto c = cos(angle), s = sin(angle);
    return {
        { c,-s, 0, 0 },
        { s, c, 0, 0 },
        { 0, 0, 1, 0 },
        { 0, 0, 0, 1 } };
}

#include <xmmintrin.h>
#include <intrin.h>

// I think I've made the other code fast enough that we don't need this anymore :/
// Nope this is way way way faster in release mode. The inliner doesn't like the linalg code so I should remove _all_ uses of apply.
__forceinline float fastdot(const float3& a, const float3& b) {
    __m128 fst = { a.x, a.y, a.z, 0.f };
    __m128 snd = { b.x, b.y, b.z, 0.f };
    return _mm_dp_ps(fst, snd, 0xff).m128_f32[0];
}

__forceinline float fastdot(const float4& a, const float4& b) {
    __m128 fst = { a.x, a.y, a.z, a.w };
    __m128 snd = { b.x, b.y, b.z, b.w };
    return _mm_dp_ps(fst, snd, 0xff).m128_f32[0];
}

inline float fullAngle(const float4& a, const float4& b, const float3& n, float epsilon = defaultEpsilon) {
    // I don't remember why I added this but this is an extremely harmful test for faces whose dimensions are significantly
    // different because it will vary wildly on an axis that basically doesn't matter.
    //if (length2(a - b) < 0.0000001f) return 0;

    auto angle = acos(clamp(fastdot(a, b), -1, 1));

    // @Note: This code determines the winding order!
    // I chose to make it backwards from what you might ordinarily expect (i.e. reverse the angle of the induced normal
    // is the same as the passed-in normal) because X x Y = Z and Y x X = -Z. Specifically, this means that if we choose
    // Y as our anchor, then X's angle should ideally be pi/2 (90 degrees). To wind clockwise, we need to reverse this.
    if (fastdot(cross(a.xyz(), b.xyz()), n) > 0)
        return 2 * float(pi) - angle;
    return angle;
}

inline std::ostream& operator<<(std::ostream& s, const float4& v) {
    s << "{ " << v.x << " " << v.y << " " << v.z << " " << v.w << " }";
    return s;
}

struct Line {
    float3 origin;
    float3 dir;

    float3 atTime(float t) const {
        return origin + dir * t;
    }
};

template<typename T> T sq(T t) { return t * t; }

enum class Side { FRONT, ON, BACK };

// @TODO this needs more robust epsilon tests:
struct Plane {
    union {
        float4 p;
        struct {
            float3 n;
            float d;
        };
    };

    Plane(float3 n, float d) : p(n.x, n.y, n.z, d) {}

    Side side(const float3& p, float epsilon = defaultEpsilon) const {
        float dist = distanceToPoint(p);
        if (dist < -epsilon) return Side::BACK;
        if (dist > epsilon) return Side::FRONT;
        return Side::ON;
    }

    Side side(const float4& p, float epsilon=defaultEpsilon) const {
        return side(p.xyz(), epsilon);
    }

    float distanceToPoint(const float3& pt) const {
        return fastdot(float4(pt, 1.f), p);
    }

    float distanceToPoint(const float4& pt) const {
        return distanceToPoint(pt.xyz());
    }

    Maybe<Line> intersect(const Plane& other, float epsilon=defaultEpsilon) const {
        auto v = cross(n, other.n);
        if (length2(v) < epsilon) return none;

        auto pt = cross(other.d * n - d * other.n, v) / fastdot(v, v);
        return Line{ pt, v };
    }

    Maybe<float3> intersect(const Line& line, float epsilon=defaultEpsilon) const {
        auto dist = fastdot(n, -line.dir);
        if (isZero(dist, epsilon)) return none;

        auto time = distanceToPoint(line.origin) / dist;
        return line.origin + line.dir * time;
    }

    Maybe<float3> intersectWithin(const Line& line, float epsilon=defaultEpsilon) const {
        auto dist = fastdot(n, -line.dir);
        if (isZero(dist, epsilon)) return none;

        auto time = distanceToPoint(line.origin) / dist;
        if (time < 0 || time > 1) return none;

        return line.origin + line.dir * time;
    }

    Maybe<float> intersectTime(const Line& line) const {
        auto dist = fastdot(n, -line.dir);
        if (isZero(dist, 0.000001f)) return none;

        return distanceToPoint(line.origin) / dist;
    }
};

template<typename T>
inline bool intersects(T amin, T amax, T bmin, T bmax) {
    return bmin - defaultEpsilon <= amax && amin - defaultEpsilon <= bmax;
}

static constexpr float k_bigNum = 999999999999.f;

struct Aabb {
    float3 min, max;

    Aabb() : min(k_bigNum, k_bigNum, k_bigNum), max(-k_bigNum, -k_bigNum, -k_bigNum) {}
    explicit Aabb(float3 pt) : min(pt), max(pt) {}
    explicit Aabb(float4 pt) : min(pt.xyz()), max(pt.xyz()) {}
    Aabb(float3 min, float3 max) : min(min), max(max) {}
    Aabb(const Aabb& a) = default;
    Aabb(const Aabb& a, const Aabb& b)  {
        *this = a;
        expand(b);
    }
    Aabb(VecA<float3> pts) : min(pts.front()), max(pts.front()) {
        for (size_t i = 1; i < pts.size(); ++i)
            expand(pts[i]);
    }
    Aabb(VecA<float4> pts) : min(pts.front().xyz()), max(pts.front().xyz()) {
        for (size_t i = 1; i < pts.size(); ++i)
            expand(pts[i]);
    }

    Aabb inflated(float amt) {
        float3 diag = max - min;
        return { min - diag * amt, max + diag * amt };
    }

    void expand(float3 pt) {
        for (int i = 0; i < 3; ++i) {
            if (pt[i] > max[i]) max[i] = pt[i];
            if (pt[i] < min[i]) min[i] = pt[i];
        }
    }

    void expand(float4 pt) {
        expand(pt.xyz());
    }

    void expand(const Aabb& other) {
        expand(other.min);
        expand(other.max);
    }

    [[nodiscard]] bool intersects(const Aabb& other) const {
        return ::intersects(min.x, max.x, other.min.x, other.max.x) &&
            ::intersects(min.y, max.y, other.min.y, other.max.y) &&
            ::intersects(min.z, max.z, other.min.z, other.max.z);
    }

    [[nodiscard]] float3 diagonal() const {
        return { max.x - min.x, max.y - min.y, max.z - min.z };
    }

    [[nodiscard]] float surfaceArea() const {
        auto d = diagonal();
        return 2 * (d.x * d.y + d.x * d.z + d.y * d.z);
    }

    [[nodiscard]] bool equals(const Aabb& bounds) const {
        return
            ::equals(min.x, bounds.min.x) && ::equals(max.x, bounds.max.x) &&
            ::equals(min.y, bounds.min.y) && ::equals(max.y, bounds.max.y) &&
            ::equals(min.z, bounds.min.z) && ::equals(max.z, bounds.max.z);
    }

    [[nodiscard]] Maybe<float> intersection(const Line& line) const {
        auto tmin = 0.000000001f;
        auto tmax = k_bigNum;

        for (int i = 0; i < 3; ++i) {
            const float t0 = std::min((min[i] - line.origin[i]) / line.dir[i],
                         (max[i] - line.origin[i]) / line.dir[i]);
            const float t1 = std::max((min[i] - line.origin[i]) / line.dir[i],
                         (max[i] - line.origin[i]) / line.dir[i]);

            tmin = std::max(t0, tmin);
            tmax = std::min(t1, tmax);

            if (tmax < tmin) {
                return none;
            }
        }

        return tmin;
    }

    float3 center() const {
        return (min + max) / 2;
    }
};

inline Vec<Plane> transformPlanes(float3 delta, VecA<Plane> planes) {
    Vec<Plane> ret;

    for (auto p : planes)
        ret.push_back({ p.n, p.d - dot(p.n, delta) });

    return ret;
}

inline void snap(float3& in, float to) {
    in = linalg::floor(in / to) * to;
}
