#pragma once

#include "csg.h"

struct EditorWorld;

//
// Commands and undo management:
//

struct UndoableCommand {
    virtual void undo(EditorWorld& world) = 0;
    virtual void redo(EditorWorld& world) = 0;

    virtual Str name() const = 0;

    virtual Set<Ref<Brush>> brushes() const = 0;
};

struct AppendBrushCommand : UndoableCommand {
    AppendBrushCommand(RefA<Brush> brush)
        : brush(brush) {
    }

    void undo(EditorWorld& world) override;
    void redo(EditorWorld& world) override;

    Set<Ref<Brush>> brushes() const override {
        return { brush };
    }

    Str name() const override {
        return "Add brush";
    }

private:
    Ref<Brush> brush;
};

struct DeleteBrushesCommand : UndoableCommand {
    DeleteBrushesCommand(Set<Ref<Brush>> brushes) {
        for (auto b : brushes)
            _brushes.push_back({ b, b->index });

        sort(begin(_brushes), end(_brushes), [](auto lhs, auto rhs) {
            return lhs.index < rhs.index;
        });
    }

    void undo(EditorWorld& world) override;
    void redo(EditorWorld& world) override;

    Set<Ref<Brush>> brushes() const override {
        Set<Ref<Brush>> ret;
        for (auto& b : _brushes) ret.insert(b.brush);
        return ret;
        //return Set<Ref<Brush>>(_brushes.begin(), _brushes.end());
    }

    Str name() const override {
        return "Add brush";
    }

private:
    struct BrushAndIndex {
        Ref<Brush> brush;
        size_t index;
    };
    Vec<BrushAndIndex> _brushes;
};

struct InvertBrushCommand : UndoableCommand {
    InvertBrushCommand(const Set<Ref<Brush>>& brushes) : _brushes(brushes) {
    }

    void undo(EditorWorld& world) override;
    void redo(EditorWorld& world) override;

    Set<Ref<Brush>> brushes() const override {
        return _brushes;
    }

    Str name() const override {
        return "Invert brush";
    }

private:
    Set<Ref<Brush>> _brushes;
};

struct TransformBrushCommand : UndoableCommand {
    TransformBrushCommand(RefA<Brush> brush, VecA<Plane> oldPlanes)
        : brush(brush), oldPlanes(oldPlanes), newPlanes(brush->planes) {
    }

    void undo(EditorWorld& world) override;
    void redo(EditorWorld& world) override;

    Set<Ref<Brush>> brushes() const override {
        return { brush };
    }

    Str name() const override {
        return "Move brush";
    }

private:
    Ref<Brush> brush;
    Vec<Plane> oldPlanes, newPlanes;
};

struct FaceTextureChange {
    Ref<Brush> brush;
    size_t face;
    TextureInfo old, newer;
};

struct ChangeTextureCommand : UndoableCommand {
    ChangeTextureCommand(VecA<FaceTextureChange> changes) : changes(changes) {
    }

    void undo(EditorWorld& world) override;
    void redo(EditorWorld& world) override;
    Set<Ref<Brush>> brushes() const override;

    Str name() const override {
        return "Change texture";
    }

    Vec<FaceTextureChange> changes;
};

class UndoManager {
    EditorWorld& _world;
    Vec<Ref<UndoableCommand>> _undos, _redos;

public:
    UndoManager(EditorWorld& world) : _world(world) {}

    void addCommand(RefA<UndoableCommand> cmd) {
        _redos.clear();
        _undos.push_back(cmd);
    }

    // Returns the set of dirty brushes:
    Set<Ref<Brush>> undo();
    Set<Ref<Brush>> redo();
};
